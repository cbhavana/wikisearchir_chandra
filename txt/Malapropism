Malapropism - Wikipedia, the free encyclopedia Malapropism From Wikipedia, the free encyclopedia Jump to: navigation, search A malapropism is the misuse of similar sounding words, especially with humorous results. An example is Yogi Berra's statement: "Texas has a lot of electrical votes,"[1] rather than "electoral votes". Contents 1 Etymology 2 Distinguishing features 3 Examples in English language 3.1 Mrs. Malaprop 3.2 Shakespeare 3.3 Malapropisms by other writers of fiction 3.4 Malapropisms by cartoonists 3.5 Malapropisms by characters in films and television shows 3.6 Malapropisms by real people 4 Examples in the Russian language 5 Philosophical significance 6 See also 7 References [edit] Etymology The word malapropos is an adjective or adverb meaning "inappropriate" or "inappropriately", derived from the French phrase mal à propos (literally "ill-suited").[2] The earliest English usage of the word cited in the Oxford English Dictionary is from 1630. Malaprop used in the linguistic sense was first used by Lord Byron in 1814 according to the OED. The terms malapropism and the earlier variant malaprop come from Richard Brinsley Sheridan's 1775 play The Rivals, and in particular the character Mrs. Malaprop. Sheridan presumably named his character Mrs. Malaprop, who frequently misspoke (to great comic effect), in joking reference to the word malapropos. The alternative term "Dogberryism" comes from the 1598 Shakespearean play Much Ado About Nothing, in which the character Dogberry produces many malapropisms with humorous effect.[3] [edit] Distinguishing features An instance of mis-speech is called a malapropism when: The word or phrase means something different from the word the speaker or writer intended to use. The word or phrase sounds similar to what was intended. For example, using obtuse (wide or dull) instead of acute (narrow or sharp) is not a malapropism; using obtuse (stupid=me or slow-witted) when one means abstruse (esoteric or difficult to understand) s used has a recognized meaning in the speaker's or writer's language. The resulting utterance is nonsense. These characteristics set malapropisms apart from other speaking or writing mistakes, such as eggcorns or spoonerisms. Simply making up a word, or adding a redundant or prefix or suffix (subliminible instead of subliminal) to an existing word, does not qualify as a malapropism, these are neologisms. [edit] Examples in English language [edit] Mrs. Malaprop All of these examples are from Sheridan's play The Rivals. "...promise to forget this fellow - to illiterate him, I say, quite from your memory." (i.e. obliterate; Act I Scene II Line 178) "...she might reprehend the true meaning of what she is saying." (i.e. comprehend; Act I Scene II Line 258) "...she's as headstrong as an allegory on the banks of Nile." (i.e. alligator; Act III Scene III Line 195) "Sure, if I reprehend any thing in this world it is the use of my oracular tongue, and a nice derangement of epitaphs!" (i.e. comprehend, vernacular, arrangement, epithets) [edit] Shakespeare Malapropisms appear in many works written well before Sheridan created their namesake character; William Shakespeare used them in a number of his plays. Constable Dogberry in Much Ado About Nothing: "Comparisons are odorous." (i.e., odious; Act 3, Scene V) "Our watch, sir, have indeed comprehended two auspicious persons." (i.e., apprehended, suspicious; Act 3, Scene V) Launcelot in The Merchant of Venice: "Certainly (Shylock) is the very devil incarnal..." (i.e., incarnate; Act 2, Scene II) "That is the very defect of the matter, sir." (i.e., effect; Act 2, Scene II) Elbow in Measure for Measure: "two notorious benefactors" (i.e., malefactors; Act 2, Scene I) "if she has been a woman cardinally given"; (i.e., carnally; Act 2, Scene I) Nurse in Romeo and Juliet: "If you be he, sir, I desire some confidence with you." Benvolio then responds "She will indite him to some supper." (i.e., conference, invite; Act 2, Scene IV) "I will tell her, sir, that you do protest, which, as I take it, is a gentlemanlike offer." (i.e., propose; Act 2, Scene IV) Nick Bottom in A Midsummer Night's Dream: Bottom says he will "aggravate" his voice when he really means he will moderate it. (Act 1 Scene II) "lion vile hath here deflower'd my dear: which ...was the fairest dame" (i.e., devoured, Act 5 Scene I) Clown in The Winter's Tale: "Ay, or else 'twere hard luck, being in so preposterous estate as we are." (i.e. prosperous; Act 5, Scene II) [edit] Malapropisms by other writers of fiction Tabitha Bramble and Winifred Jenkins, two characters in Tobias Smollett's 1771 novel Humphrey Clinker, are founts of malapropisms: Tabitha: "I know that hussy, Mary Jones, loves to be rumping (i.e. romping) with the men." Winifred: "You that live in the country have no deception (i.e. conception) of our doings at Bath." Lee McKinney, the main character of the JoAnna Carl "Chocoholic Mysteries" tends to use malapropisms when nervous: She looked sharply at me. "Am I trashing?" I said. "I mean, am I trespassing?" [edit] Malapropisms by cartoonists The comic strip Frank and Ernest makes frequent use of malaprops, including appearances by the superhero "Malaprop Man". [edit] Malapropisms by characters in films and television shows Leo Gorcey (of Dead End Kids, East Side Kids, Bowery Boys fame) made a career of it: "I depreciate it!", "A clever seduction", "Let me regurgitate", "Pardon my protrusion", etc. Archie Bunker from the American TV sitcom All in the Family was known for malapropisms of words and names: "The hookeries and massageries...the whole world is turning into a regular Sodom and Glocca Morra." (i.e., Gomorrah) "Off-the-docks Jews" (i.e., Orthodox Jews) "A woman doctor is only good for women's problems...like your groinocology." (i.e., Gynaecology) "I ain't a man of carnival instinctuals like you." (i.e., carnal instincts) "All girls go cockeyed during pooberescency." (i.e., puberty) "A Menstrual show" (i.e., minstrel) "Irene Lorenzo, Queen of the Women's Lubrication Movement." (i.e., Liberation) "Buy one of them battery operated transvestite radios." (i.e., transistor) "In her elastic stockings, next to her very close veins." (i.e., varicose) "George Meaney, head of the UFO-CIA." (i.e., AFL-CIO) "The first priorority is that I'm the sick one" (i.e., priority) "To my dear daughter Gloria Bunker, whom I forgive for marrying the Meathead, I leave my living room chair as a centralpiece in her someday living room" (i.e., centerpiece) "Last will and tentacle..." (i.e., testament) "Patience is a virgin." (i.e., virtue) "A Polack art exhibit!" (i.e., Jackson Pollock). "As youse people say, Sh-boom." (i.e., shalom) A "Kuzeeknee" instead of 'Zucchini' "Gazo: Hey Rock, how's about investing in condominiums? It's safe." "Rocky: I never use 'em" (Rocky II) Stan Laurel often used malapropisms in the Laurel and Hardy films: "We heard the ocean is infatuated with sharks" (i.e., infested, although Ollie erroneously corrects him as meaning infuriated) - The Live Ghost. "What a terrible cat's after me!" (i.e., catastrophe) - Any Old Port! "We'd like a room with a southern explosion" (i.e., exposure) - Any Old Port! "The doctor said I might get hydrophosphates" (i.e., hydrophobia) - Helpmates "We floundered in a typhoid" (i.e., typhoon) - Sons of the Desert "We're like two peas in a pot" (i.e., pod) - Sons of the Desert Ricky (Robb Wells) from Trailer Park Boys has many well known malapropisms, known by fans of the show as "Rickyisms",[4] among them are: Get two birds stoned at once (kill two birds with one stone) Worst case ontario (worst case scenario) I'm not a pessimist, I'm an optometrist (optimists and pessimists) Gorilla see, gorilla do (monkey see, monkey do) Survival of the fitness (survival of the fittest) Passed with flying carpets (passed with flying colours) What comes around, is all around (what comes around, goes around) It's clear to see who makes the pants here (who wears the pants) Tempus Fuck It (Tempus fugit) It doesn't take rocket appliances (rocket science) A great many cartoon writers use the form as well: "Brudder, you got a preposition" and "That thing will give you a conclusion of the brain" (i.e., Proposition, concussion or possibly contusion) - Bugs Bunny "My uncle Thumper had a problem with his probate and he had to take these big pills and drink lots of water." (i.e., prostate) - Roger Rabbit from Who Framed Roger Rabbit "Look, Valiant, we got a reliable tip-off the rabbit was here. It was corrugated by several others." and "Say, boss, you want we should disresemble the place?"(i.e., "corroborated", "disassemble") - Smarty from Who Framed Roger Rabbit. "The ironing is delicious." (i.e., irony) - The Simpsons' Bart Simpson, after finding Lisa in detention.[5] "As Bob is my witless." (i.e.,God, witness) - Rugrats The writers of The Sopranos often used malapropisms: "...prostate with grief." (i.e., prostrate) - Tony Soprano "Create a little dysentery among the ranks." (i.e., dissent) - Christopher Moltisanti ".. he could technically not have penisary contact with her volvo." (i.e., vulva) - Tony Soprano to Jennifer Melfi "Revenge is a dish best served with cold cuts." (i.e., cold) - Tony Soprano, quoting Dr. Melfi "That's right, honey the sacred and the propane" (i.e., profane) - Carmine Lupertazzi Jr. "My Tennessee William..." (i.e., Tennessee Williams) - Adriana La Cerva to Christopher Moltisanti As did the writers of many comedy programs: "He didn't confess. You're just trying to make me admit something I didn't do. I know all about reverse biology, buddy. I'm not an idiot." (i.e., psychology) � Jason Lee as Earl Hickey on NBC's My Name Is Earl "I want to be effluent mum!" "You are effluent Kimi..." (i.e., affluent) - Kath & Kim "What are you incinerating?..." (i.e., insinuating) � Steptoe and Son ("Doodlebug over Shepherd's Bush" episode, written by Galton and Simpson) "No, a moo point. Yeah, it's like a cow's opinion. It just doesn't matter. It's moo." (i.e., moot) Joey Tribbiani on NBC's Friends "Good to be back on the old terracotta" (i.e., terra firma) - Del Boy in Only Fools and Horses "Pedal stool" (i.e., pedestal) - in The IT Crowd "Damp squid" (i.e., damp squib) - in The IT Crowd "Don't get historical!" (i.e., hysterical) Said by Ringo Starr's Aunt Jessie in Magical Mystery Tour also said by BSM Williams (amongst many other malapropisms) in the BBC comedy series It Ain't Half Hot Mum (Episode: "Gloria's Finest Hour") Officer Crabtree of the British comedy program, 'Allo 'Allo, speaks atrocious French, which is rendered in the series as English filled with malapropisms. For example, he recalls a "nit on the bonk of the Thames" (i.e., night, bank) with a female "secret urgent" (i.e. agent). Another regular of his is his greeting when he enters René's Café: "Good Moaning" (i.e., Good morning) Virginia Chance of the show Raising Hope regularly uses malapropisms in her speech. "Quit your procrasterbating and go talk to him." Private "Snowball" Brown in the Stanley Kubrick film Full Metal Jacket commits a malapropism in trying to explain details of President Kennedy's assassination and the distance of the shot taken by Lee Harvey Oswald. "Sir, it was pretty far! From that book suppository building, sir!" [edit] Malapropisms by real people This section needs additional citations for verification. Please help improve this article by adding citations to reliable sources. Unsourced material may be challenged and removed. (April 2011) Malapropisms are often quoted in the media: Former Chicago Mayor Richard Daley: "Let's get this straight. The police don't cause disorder. The police are here to preserve disorder."[citation needed] It was reported in New Scientist that an office worker described a colleague as "a vast suppository of information". (i.e., repository or depository) The worker then apologised for his "Miss-Marple-ism". (i.e. malapropism)[6] New Scientist reported it as possibly the first time malapropism has been turned into a malapropism. Time reported Irish Taoiseach Bertie Ahern as warning his country against "upsetting the apple tart" (i.e., apple cart) of his country's economic success.[7] "It's great to be back on terracotta!" (i.e., terra firma) � John Prescott, a British politician echoing Del Boy (see above).[8] This is disputed by John Prescott himself.[9] The titles of the tracks "A Hard Day's Night" and "Tomorrow Never Knows" by The Beatles are both "Ringoisms", or malapropisms by Ringo Starr. "Mr Speaker, you are the anecdote to verbal diarrhoea- [Laughter]" � Tobias Ellwood, MP for Bournemouth East, UK[10] "Order. I think the word for which the hon. Gentleman was vainly searching was probably "antidote"." � John Bercow, MP, Speaker of the House, UK [11] "I congratulate my hon. Friend on a good recovery. If I may say so, Mrs Malaprop would have been proud of him." � Sir George Young, MP for NW Hampshire, UK [12] [edit] Examples in the Russian language The word rynda for "ship's bell". The English phrase "Ring the bell!" was heard by Russian seamen as "Ryndu bey!", i.e., "Hit the rynda", rynda being the word for the tsar's bodyguard. Accordingly, the phrase "to hit the rynda" was used to mean "to signal time with the ship's bell", and later the bell itself has become commonly known as ship's "rynda".[13][14] [edit] Philosophical significance In the essay "A Nice Derangement of Epitaphs", philosopher Donald Davidson argues that malapropisms demonstrate that competence in a language is not a matter of applying rigid rules to the decoding of utterances. Rather, says Davidson, it appears that in interpreting others, people constantly modify their own understanding of our language.[15] [edit] See also Anti-proverb Catachresis Colemanballs Chris "Mad Dog" Russo Eggcorn Engrish Freudian slip Goldwynisms Holorime Homonym List of commonly misused English words Mondegreen Norm Crosby Spoonerism Yogiism Viktor Chernomyrdin [edit] References ^ "Great Quotes". http://www.great-quotes.com/quote/12877. Retrieved 2011-09-28. "Texas has a lot of electrical votes"  ^ Simpson, John (ed.) 2008. Oxford English Dictionary. London: Oxford University Press. ^ Berger, Harry (2005). Situated Utterances. Fordham University Press. pp. 499. ISBN 0823224295.  ^ http://trailerpark.wikia.com/wiki/Rickyisms ^ The Simpsons: "Lisa's Date with Density" ^ http://www.newscientist.com/article/mg18625042.600-feedback.html New Scientist 18 June 2005 Malapropism for malapropism ^ Mayer, Catherine (2007-04-26). "Mr. Popularity". Time. http://www.time.com/time/magazine/article/0,9171,1614940,00.html. Retrieved 2010-05-12.  ^ John Prescott Profile - BBC News ^ Crabb, Annabel (19 October 2011). "An audience, an audience, my kingdom for an audience". The Drum. Australian Broadcasting Corporation. http://www.abc.net.au/news/2011-10-19/crabb-an-audience-my-kingdom-for-an-audience/3578344. Retrieved 24 October 2011. "... the "terra cotta" remark was an urban myth. "Just corrected an Aussie journo giving a speech in Sydney. I love Twitter!" Prescott told his followers."  ^ Commons Debates 2010-11-26 Tobias Ellwood - TheyWorkForYou.com ^ Commons Debates 2010-11-26 John Bercow - TheyWorkForYou.com ^ Commons Debates 2010-11-26 Sir George Young - TheyWorkForYou.com ^ Lev Uspensky, "A Word about Words", (Russian) ^ Max Vasmer, "Russisches Etymologisches Wörterbuch" ^ Grandy, R. and Warner, R., ed. (1986). Philosophical Grounds of Rationality. Oxford University Press. ISBN 0198244649.  Retrieved from " http://en.wikipedia.org/w/index.php?title=Malapropism&oldid=474892387" Categories: Linguistics Hidden categories: Articles with Russian language external links Articles needing additional references from April 2011 All articles needing additional references All articles with unsourced statements Articles with unsourced statements from April 2011 Personal tools Log in / create account Namespaces Article Talk Variants Views Read Edit View history Actions Search Navigation Main page Contents Featured content Current events Random article Donate to Wikipedia Interaction Help About Wikipedia Community portal Recent changes Contact Wikipedia Toolbox What links here Related changes Upload file Special pages Permanent link Cite this page Print/export Create a book Download as PDF Printable version Languages ????????? Deutsch Español Français Italiano Nederlands ??? Polski Svenska This page was last modified on 4 February 2012 at 02:30. Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. See Terms of use for details. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Contact us Privacy policy About Wikipedia Disclaimers Mobile view