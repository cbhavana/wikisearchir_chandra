title{{multiple issues}}
In object-oriented programming, a class is a construct that is used as a blueprint to create instances of itself – referred to as class instances, class objects, instance objects or simply objects. A class defines constituent members which enable these class instances to have state and behavior. Data field members (member variables or instance variables) enable a class object to maintain state. Other kinds of members, especially methods, enable a class object's behavior. Class instances are of the type of the associated class. A class usually represents a noun, such as a person, place or (possibly quite abstract) thing, or something nominalized. For example, "Banana" is an instance of the class "Fruit" (a "Fruit" object). It is of the type "Fruit".
Design and implementationClasses consist of and are composed from structural and also behavioral constituents, though there may not be a requirement to include any structural or behavioral constituents at all. Programming languages that include classes as a programming construct offer support for various class-related features. These features, and the syntaxes with which they are provided, vary greatly. Various control features such as member access specifiers may be provided by a language.
StructureA class contains data field descriptions (or properties, fields, data members, attributes), i.e., field types and names, that will be associated with either per-instance or per-class state variables at program run time. In most languages, the structure as defined by the class determines how the memory used by its instances will be laid out. Note: Other implementations are possible such as wherein objects are structured as associative key-value containers (Python).Some programming languages support specification of invariants as part of the definition of the class, and enforce them through the type system. Encapsulation of state is necessary for being able to enforce the invariants of the class.
Behavior{{Main}}The behavior to be exhibited by class instances at program run time is defined in methods. A method is a subroutine (function) with a special property that it has access to data stored in an object (instance) and is thereby able to control the state of the object. (Note: Some languages allow direct access to instance variables (C++)). The various kinds of methods and language support for them differs. Kinds of methods include regular methods that are created and called by programmer code and special methods (constructors, destructors, conversion operators, static methods, e.g.) that are created and called by compiler-generated code. A language may allow the programmer to define, and even call, special methods.
The concepts of class interface{{Main}}
Every class implements (or realizes) an interface by providing structure (i.e. data and state) and method implementations (i.e. providing code that specifies how methods work). There is a distinction between the definition of an interface and the implementation of that interface. In most languages, this line is usually blurred, because a class declaration both defines and implements an interface. Some languages, however, provide features that help separate interface and implementation. For example, an abstract class can define an interface without providing implementation.Languages that support class inheritance also allow classes to inherit interfaces from the classes that they are derived from. In languages that support access specifiers, the interface of a class is considered to be the set of public members of the class, including both methods and attributes (via implicit getter and setter methods); any private members or internal data structures are not intended to be depended on by client code and thus are not part of the interface.The object-oriented programming methodology is designed in such a way that the operations of any interface of a class are usually chosen to be independent of each other. It results in a layered design where clients of an interface use the methods declared in the interface. An interface places no requirements for clients to invoke the operations of one interface in any particular order. This approach has the benefit that client code can assume that the operations of an interface are available for use whenever the client holds a valid reference to the object.
ExampleThe buttons on the front of your television set, for example, are the interface between you and the electrical wiring on the other side of its plastic casing. You press the "power" button to toggle the television on and off. In this example, the television is the instance, each method is represented by a button, and all the buttons together comprise the interface. In its most common form, an interface is a specification of a group of related methods without any associated implementation of the methods.attributes and interface example: A television set has a myriad of attributes, such as size and whether it supports color, together comprise its structure. A class represents the full description of a television, including its attributes (structure) and buttons (interface).static method example: Getting the total number of televisions in existence could be a static method of the television class. This method is clearly associated with the class, yet is outside the domain of each individual instance of the class. Another example is a static method that finds a particular instance out of the set of all television objects.
Member accessibility{{See}}Many languages support the concept of information hiding and encapsulation, typically with access specifiers for class members. Access specifiers control access to class members. Some access specifiers may also control how classes inherit such constraints. Their primary purpose is to separate the interface of a class from its implementation.A common set of access specifiers that many object-oriented languages support is:{{cite web}}

private (or class-private) restricts the access to the class itself. Only methods that are part of the same class can access private members.
protected (or class-protected) allows the class itself and all its subclasses to access the member.
public means that any code can access the member by its name.

Note that although many languages support the above access specifiers, the semantics of them may subtly differ in each.Object-oriented design uses the access specifiers in conjunction with careful design of public method implementations to enforce class invariants. A common usage of access specifiers is to separate the internal data (structure)s of a class from its interface: the internal data structures are made private, while public accessor methods can be used to inspect or alter such private data.Access specifiers do not necessarily control visibility, in that even private members may be visible to client code. In some languages, an inaccessible, but visible member may be referred to at run-time (e.g. pointer to it can be returned from member functions), but all attempts to use it by referring to the name of the member from client code will be prevented by the type checker.The various object-oriented programming languages enforce member accessibility and visibility to various degrees, and depending on the language's type system and compilation policies, enforced at either compile-time or run-time. For example, the Java language does not allow client code to access the private data of a class to compile at all, whereas in languages like Objective-C or Perl client code can do whatever it wants. In the C++ language, private methods are visible, but not accessible in the interface; however, they are commonly made invisible by explicitly declaring fully abstract classes that represent the interfaces of the class.Other prominent accessibility schemes (with example languages):

Instance vs. class accessibility: Ruby supports instance-private and instance-protected access specifiers in lieu of class-private and class-protected, respectively. They differ in that they restrict access based on the instance itself, rather than the instance&#39;s class.
Friend: C++ supports a mechanism where a function explicitly declared as a friend function of the class may access the members designated as private or protected.
Path-based: Java supports restricting access to a member within a Java package which is roughly the path of a file.


Inter-class relationshipIn addition to the design of standalone classes that do not have a relationship to other classes, programming languages may support more advanced class design based upon inter-class relationship. The inter-class relationship design capabilities commonly provided are compositional and hierarchical.
CompositionalClasses can be composed of other classes, thereby establishing a compositional relationship between the enclosing class and its embedded classes. Compositional relationship between classes is also commonly known as a has-a relationship. For example, a class 'Car' could be composed of (i.e., contain) a class 'Engine'. Therefore, a Car has an Engine. Composition is often implemented in programming languages by locating the data storage of embedded class instances within the storage area of the enclosing class instance. The instances of embedded classes have shorter or same lifetime than the lifetime of the instances of the enclosing class.
HierarchicalClasses can be derived from one or more existing classes (not all languages support multiple inheritance), thereby establishing a hierarchical relationship between the derived-from classes (base classes or parent classes or superclasses) and the derived class (or child class or subclass) . The relationship of the derived class to the derived-from classes is commonly known as an is-a relationship. For example, a class 'Button' could be derived from a class 'Control'. Therefore, a Button is a Control. Structural and behavioral members of the parent classes are inherited by the child class. Derived classes can define additional structural members (data fields) and/or behavioral members (methods) in addition to those that they inherit and are therefore specializations of their superclasses. Also, derived classes can override inherited methods if the language provides for such.If multiple inheritance is allowed, the hierarchy is a directed acyclic graph (or DAG for short), otherwise it is a tree. The hierarchy has classes as nodes and inheritance relationships as links. Classes in the same level are more likely to be associated than classes in different levels. The levels of this hierarchy are called layers or levels of abstraction.
Definitions of subclass{{Main}}Conceptually, a superclass should be considered as a common part of its subclasses. This factoring of commonality is one mechanism for providing reuse. Thus, extending a superclass by modifying the existing class is also likely to narrow its applicability in various situations. In object-oriented design, careful balance between applicability and functionality of superclasses should be considered. Subclassing is different from subtyping in that subtyping deals with common behaviour whereas subclassing is concerned with common structure.There are two slightly different points of view as to whether subclasses of the same class are required to be disjoint. Sometimes, subclasses of a particular class are considered to be completely disjoint. That is, every instance of a class has exactly one most-derived class, which is a subclass of every class that the instance has. This view does not allow dynamic change of object's class, as objects are assumed to be created with a fixed most-derived class. The basis for not allowing changes to object's class is that the class is a compile-time type, which does not usually change at runtime, and polymorphism is utilized for any dynamic change to the object's behavior, so this ability is not necessary. And design that does not need to perform changes to object's type will be more robust and easy-to-use from the point of view of the users of the class.From another point of view, subclasses are not required to be disjoint. Then there is no concept of a most-derived class, and all types in the inheritance hierarchy that are types of the instance are considered to be equally types of the instance. This view is based on a dynamic classification of objects, such that an object may change its class at runtime. Then object's class is considered to be its current structure, but changes to it are allowed. The basis for allowing changes to object's class is a perceived inconvenience caused by replacing an instance with another instance of a different type, since this would require change of all references to the original instance to be changed to refer to the new instance. When changing the object's class, references to the existing instances do not need to be replaced with references to new instances when the class of the object changes. However, this ability is not readily available in all programming languages. This analysis depends on the proposition that dynamic changes to object structure are common. This may or may not be the case in practice.
Orthogonality of the class concept and inheritanceAlthough class-based languages are commonly assumed to support inheritance, inheritance is not an intrinsic aspect of the concept of classes. There are languages that support classes yet do not support inheritance. Examples are earlier versions of Visual Basic. These languages, sometimes called "object-based languages", do not provide the structural benefits of statically type-checked interfaces for objects. This is because in object-based languages, it is possible to use and extend data structures and attach methods to them at run-time. This precludes the compiler or interpreter being able to check the type information specified in the source code as the type is built dynamically and not defined statically. Most of these languages allow for instance behaviour and complex operational polymorphism (see dynamic dispatch and polymorphism).
Within object-oriented analysis{{Main}}In object-oriented analysis and in UML, an association between two classes is a type of a link between the corresponding objects. A (two-way) association between classes A and B describes a relationship between each object of class A and some objects of class B, and vice versa. Associations are often named with a verb, such as "subscribes-to".An association role type describes the role type of an instance of a class when the instance participates in an association. An association role type is related to each end of the association. A role describes an instance of a class from the point of view of a situation in which the instance participates in the association. Role types are collections of role (instance)s grouped by their similar properties. For example, a "subscriber" role type describes the property common to instances of the class "Person" when they participate in a "subscribes-to" relationship with the class "Magazine". Also, a "Magazine" has the "subscribed magazine" role type when the subscribers subscribe-to it.Association role multiplicity describes how many instances correspond to each instance of the other class(es) of the association. Common multiplicities are "0..1", "1..1", "1..*" and "0..*", where the "*" specifies any number of instances.There are some special kinds of associations between classes.
Taxonomy of classesThere are many categories of classes depending on modifiers. Note that these categories do not necessarily categorize classes into distinct partitions.
Abstract  {{anchor}}{{Main}}In a language that supports inheritance, an abstract class, or abstract base class (ABC), is a class that cannot be instantiated for it is either labelled as abstract or it simply specifies abstract methods (or virtual methods). Abstract classes specify virtual methods via signatures, that are to be implemented by direct or indirect descendents of the abstract class. Before a class derived, directly or indirectly, from an abstract class can be instantiated, all abstract methods of its parent classes must be implemented by some class in the derivation chain. Most object oriented programming languages allow the programmer to specify, though not necessarily by a keyword, which classes are considered abstract and will not allow these to be instantiated. For example, in Java and PHP, the keyword abstract is used. In C++, an abstract class is a class having at least one abstract method given by the appropriate syntax in that language (a pure virtual function in C++ parlance). A class consisting of only virtual methods is called a Pure Abstract Base Class (or Pure ABC) in C++ and is also known as an interface by users of the language. Other languages, notably Java and C#, support a variant of abstract classes called an interface via a keyword in the language. Such a class can only contain abstract publicly accessible methods. Abstract classes defined as interfaces are a much more specific usage of the more general meaning of the term interface, even as used in computer science, and the concept of interfaces has seen much usage and popularity within the realm of languages that support object-orientation.

)


ConcreteOne common (if not modern) definition of concrete class is a class that can be instantiated. (In "Advanced C++ Programming Styles and Idioms", James Coplien defines "concrete class" as a class that has implicitly generated or explicitly defined special member functions that enable a class object to behave as do built-in types. These "concrete classes", then, adhere to the "Orthodox Canonical Form". Many developers who first learned the term as such, way back then, still primarily use it that way). This contrasts with abstract classes as described above.
Local and innerIn some languages, classes can be declared in scopes other than the global scope. There are various types of such classes.One common type is an inner class, which is a class defined within another class. Since it involves two classes, this can also be treated as another type of class association. The methods of an inner class can access static methods of the enclosing class(es). An inner class is typically not associated with instances of the enclosing class, i.e. an inner class is not instantiated along with its enclosing class. Depending on language, it may or may not be possible to refer to the class from outside the enclosing class. A related concept is inner types (a.k.a. inner data type, nested type), which is a generalization of the concept of inner classes. C++ is an example of a language that supports both inner classes and inner types (via typedef declarations).Another type is a local class, which is a class defined within a procedure or function. This limits references to the class name to within the scope where the class is declared. Depending on the semantic rules of the language, there may be additional restrictions on local classes compared non-local ones. One common restriction is to disallow local class methods to access local variables of the enclosing function. For example, in C++, a local class may refer to static variables declared within its enclosing function, but may not access the function's automatic variables.
Metaclasses{{Main}}Metaclasses are classes whose instances are classes. A metaclass describes a common structure of a collection of classes. A metaclass can implement a design pattern or describe a shorthand for particular kinds of classes. Metaclasses are often used to describe frameworks.In some languages such as Python, Ruby or Smalltalk, a class is also an object; thus each class is an instance of the unique metaclass, which is built in the language. For example, in Objective-C, each object and class is an instance of NSObject. The Common Lisp Object System (CLOS) provides metaobject protocols (MOPs) to implement those classes and metaclasses.
Non-subclassableNon-subclassable classes allow programmers to design classes and hierarchies of classes which at some level in the hierarchy, further derivation is prohibited. (A stand-alone class may be also designated as non-subclassable, preventing the formation of any hierarchy). Contrast this to abstract classes, which imply, encourage, and require derivation in order to be used at all. A non-subclassable class is implicitly concrete. A non-subclassable class is created by declaring the class as sealed in C# or as final in Java and PHP. For example, Java's {{Java}} class is designated as final. Non-subclassable classes may allow a compiler (in compiled languages) to perform optimizations that are not available for subclassable classes.
PartialIn languages supporting the feature, a partial class is a class whose definition may be split into multiple pieces, within a single source-code file or across multiple files. The pieces are merged at compile-time, making compiler output the same as for a non-partial class.The primary motivation for introduction of partial classes is to facilitate the implementation of code generators, such as visual designers. It is otherwise a challenge or compromise to develop code generators that can manage the generated code when it is interleaved within developer-written code. Using partial classes, a code generator can process a separate file or course-grained partial class within a file, and is thus alleviated from intricately interjecting generated code via extensive parsing, increasing compiler efficiency and eliminating the potential risk of corrupting developer code. In a simple implementation of partial classes, the compiler can perform a phase of precompilation where it "unifies" all the parts of a partial class. Then, compilation can proceed as usual.Other benefits and effects of the partial class feature include:

Enables separation of a class&#39;s interface and implementation code in a unique way.
Eases navigation through large classes within a editor.
Enables separation of concerns, in a way similar to aspect-oriented programming but without using any extra tools.
Enables multiple developers to work on a single class concurrently without the need to merge individual code into one file at a later time. 

Partial classes have existed in Smalltalk under the name of Class Extensions for considerable time. With the arrival of the .NET framework 2, Microsoft introduced partial classes, supported in both C# 2.0 and Visual Basic 2005. WinRT also supports partial classes.
Example in VB.NETThis simple example, written in Visual Basic .NET, shows how parts of the same class are defined in two different files.

file1.vb

:
Partial Class MyClass
    Private _name As String
End Class


file2.vb

:
Partial Class MyClass
    Public Readonly Property Name() As String
         Get
             Return _name
         End Get
    End Property
End Class
When compiled, the result is the same as if the two files were written as one, like this:

Class MyClass
    Private _name As String
    Public Readonly Property Name() As String
         Get
             Return _name
         End Get
    End Property
End Class


UninstantiableUninstantiable classes allow programmers to group together per-class fields and methods (and perhaps other kinds of members depending on the given programming language) that are accessible at runtime without an instance of the class. Indeed, instantiation is prohibited for this kind of class.For example, in C#, a class marked "static" can not be instantiated, can only have static members (fields, methods, other), may not have instance constructors, and is sealed.
UnnamedAn unnamed class or anonymous class, in languages that support such a concept, is a class which is not bound to a name or identifier upon definition. (Analogous to named vs. unnamed functions)).
BenefitsComputer programs usually model aspects of some real or abstract world (the Domain). Because each class models a concept, classes provide a more natural way to create such models. Each class in the model represents a noun in the domain, and the methods of the class represent verbs that may apply to that noun (Verbs can also be modeled as classes, see Command Pattern). For example in a typical business system, various aspects of the business are modeled, using such classes as Customer, Product, Worker, Invoice, Job, etc. An Invoice may have methods like Create, Print or Send, a Job may be Performed or Canceled, etc. Once the system can model aspects of the business accurately, it can provide users of the system with useful information about those aspects. Classes allow a clear correspondence (mapping) between the model and the domain, making it easier to design, build, modify and understand these models. Classes provide some control over the often challenging complexity of such models.Classes can accelerate development by reducing redundant program code, testing and bug fixing. If a class has been thoroughly tested and is known to be a 'solid work', it is usually true that using or extending the well-tested class will reduce the number of bugs - as compared to the use of freshly developed or ad hoc code - in the final output. In addition, efficient class reuse means that many bugs need to be fixed in only one place when problems are discovered.Another reason for using classes is to simplify the relationships of interrelated data. Rather than writing code to repeatedly call a graphical user interface (GUI) window drawing subroutine on the terminal screen (as would be typical for structured programming), it is more intuitive. With classes, GUI items that are similar to windows (such as dialog boxes) can simply inherit most of their functionality and data structures from the window class. The programmer then need only add code to the dialog class that is unique to its operation. Indeed, GUIs are a very common and useful application of classes, and GUI programming is generally much easier with a good class framework.
Run-time representationAs a data type, a class is usually considered as a compile-time construct. A language may also support prototype or factory metaobjects that represent run-time information about classes, or even represent metadata that provides access to reflection facilities and ability to manipulate data structure formats at run-time. Many languages distinguish this kind of run-time type information about classes from a class on the basis that the information is not needed at run-time. Some dynamic languages do not make strict distinctions between run-time and compile-time constructs, and therefore may not distinguish between metaobjects and classes.For example: if Human is a metaobject representing the class Person, then instances of class Person can be created by using the facilities of the Human metaobject.
See also

Hierarchy
Class diagram (UML)
Class-based programming
Prototype-based programming


References{{reflist}}
Further reading

Abadi; Cardelli: A Theory of Objects
ISO/IEC 14882:2003 Programming Language C++, International standard
Class Warfare: Classes vs. Prototypes, by Brian Foote
Meyer, B.: &#34;Object-oriented software construction&#34;, 2nd edition, Prentice Hall, 1997, ISBN 0-13-629155-4
Rumbaugh et al.: &#34;Object-oriented modeling and design&#34;, Prentice Hall, 1991, ISBN 0-13-630054-5


External links

{{cite web}}

{{Programming language}}
{{Data types}}{{DEFAULTSORT:Class (Computer Programming)}}



af:Klasse
ar:صنف (علوم الحاسب)
be-x-old:Кляса (праграмаваньне)
bs:Računarska klasa
ca:Classe (informàtica)
cs:Třída (programování)
da:Klasse (datalogi)
de:Klasse (Programmierung)
et:Klass (programmeerimine)
es:Clase (informática)
eo:Klaso (objektema programado)
fa:کلاس (برنامه‌نویسی)
fr:Classe (informatique)
gl:Clase sw
ko:클래스 (컴퓨터 과학)
is:Klasi (forritun)
it:Classe (informatica)
he:מחלקה (תכנות)
ltg:Klase
lt:Klasė (programavimas)
my:Class(ကွန်ပျူတာ)
nl:Klasse (informatica)
ja:クラス (コンピュータ)
pl:Klasa (programowanie obiektowe)
pt:Classe (programação)
ro:Clasă (programare)
ru:Класс (программирование)
simple:Class (programming)
sk:Trieda (programovanie)
su:Class (élmu komputer)
fi:Kapselointi
sv:Klass (programmering)
ta:வகுப்பு (கணினியியல்)
th:คลาส (คอมพิวเตอร์)
tr:Sınıf (programlama)
uk:Клас (програмування)
vi:Lớp (khoa học máy tính)
zh:类 (计算机科学)