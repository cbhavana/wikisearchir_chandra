titleAutonomous robots are robots that can perform desired tasks in unstructured environments without continuous human guidance. Many kinds of robots have some degree of autonomy. Different robots can be autonomous in different ways. A high degree of autonomy is particularly desirable in fields such as space exploration, cleaning floors, mowing lawns, and waste water treatment.Some modern factory robots are "autonomous" within the strict confines of their direct environment. It may not be that every degree of freedom exists in their surrounding environment, but the factory robot's workplace is challenging and can often contain  chaotic, unpredicted variables. The exact orientation and position of the next object of work and (in the more advanced factories) even the type of object and the required task must be determined. This can vary unpredictably (at least from the robot's point of view).One important area of robotics research is to enable the robot to cope with its environment whether this be on land, underwater, in the air, underground, or in space.A fully autonomous robot has the ability to

Gain information about the environment (Rule #1)
Work for an extended period without human intervention (Rule #2)
Move either all or part of itself throughout its operating environment without human assistance (Rule #3)
Avoid situations that are harmful to people, property, or itself unless those are part of its design specifications (Rule #4)
Maintain its own survival at the expense of the previous rules (the Sentient Robot Mandate{{Citation needed}})

An autonomous robot may also learn or gain new capabilities like adjusting strategies for accomplishing its task(s) or adapting to changing surroundings.Autonomous robots still require regular maintenance, as do other machines.
Examples of progress towards commercial autonomous robots
Self-maintenanceThe first requirement for complete physical autonomy is the ability for a robot to take care of itself. Many of the battery powered robots on the market today can find and connect to a charging station, and some toys like Sony's Aibo are capable of self-docking to charge their batteries.Self maintenance is based on "proprioception", or sensing one's own internal status. In the battery charging example, the robot can tell proprioceptively that its batteries are low and it then seeks the charger. Another common proprioceptive sensor is for heat monitoring. Increased proprioception will be required for robots to work autonomously near people and in harsh environments.


Common proprioceptive sensors are
Thermal
Hall Effect
Optical
Contact


Sensing the environment
Exteroception is sensing things about the environment. Autonomous robots must have a range of environmental sensors to perform their task and stay out of trouble.

Common exteroceptive sensors are
Electromagnetic spectrum
Sound
Touch
Chemical sensors (smell, odor)
Temperature
Range to things in the environment
Attitude (Inclination)

Some robotic lawn mowers will adapt their programming by detecting the speed in which grass grows as needed to maintain a perfect cut lawn, and some vacuum cleaning robots have dirt detectors that sense how much dirt is being picked up and use this information to tell them to stay in one area longer.
Task performance
The next step in autonomous behavior is to actually perform a physical task. A new area showing commercial promise is domestic robots, with a flood of small vacuuming robots beginning with iRobot and Electrolux in 2002. While the level of intelligence is not high in these systems, they navigate over wide areas and pilot in tight situations around homes using contact and non-contact sensors. Both of these robots use proprietary algorithms to increase coverage over simple random bounce.The next level of autonomous task performance requires a robot to perform conditional tasks. For instance, security robots can be programmed to detect intruders and respond in a particular way depending upon where the intruder is.
Indoor position sensing and navigation
For a robot to associate behaviors with a place (localization) requires it to know where it is and to be able to navigate point-to-point. Such navigation began with wire-guidance in the 1970s and progressed in the early 2000s to beacon-based triangulation. Current commercial robots autonomously navigate based on sensing natural features. The first commercial robots to achieve this were Pyxus' HelpMate hospital robot and the CyberMotion guard robot, both designed by robotics pioneers in the 1980s. These robots originally used manually created CAD floor plans, sonar sensing and wall-following variations to navigate buildings. The next generation, such as MobileRobots' PatrolBot and autonomous wheelchairPrincipal Investigator: W. Kennedy, National Institutes of Health, NIH SBIR 2 R44 HD041781-02 both introduced in 2004, have the ability to create their own laser-based maps of a building and to navigate open areas as well as corridors. Their control system changes its path on-the-fly if something blocks the way.At first, autonomous navigation was based on planar sensors, such as laser range-finders, that can only sense at one level. The most advanced systems now fuse information from various sensors for both localization (position) and navigation. Systems such as Motivity can rely on different sensors in different areas, depending upon which provides the most reliable data at the time, and can re-map a building autonomously.Rather than climb stairs, which requires highly specialized hardware, most indoor robots navigate handicapped-accessible areas, controlling elevators and electronic doors.Speci-Minder; see elevator and door access With such electronic access-control interfaces, robots can now freely navigate indoors. Autonomously climbing stairs and opening doors manually are topics of research at the current time.As these indoor techniques continue to develop, vacuuming robots will gain the ability to clean a specific user specified room or a whole floor. Security robots will be able to cooperatively surround intruders and cut off exits. These advances also bring concommitant protections: robots' internal maps typically permit "forbidden areas" to be defined to prevent robots from autonomously entering certain regions.
Outdoor autonomous position-sensing and navigation
Outdoor autonomy is most easily achieved in the air, since obstacles are rare. Cruise missiles are rather dangerous highly autonomous robots. Pilotless drone aircraft are increasingly used for reconnaissance. Some of these unmanned aerial vehicles (UAVs) are capable of flying their entire mission without any human interaction at all except possibly for the landing where a person intervenes using radio remote control. But some drone aircraft are capable of a safe, automatic landing also.Outdoor autonomy is the most difficult for ground vehicles, due to: a) 3-dimensional terrain; b) great disparities in surface density; c) weather exigencies and d) instability of the sensed environment.In the US, the MDARS project, which defined and built a prototype outdoor surveillance robot in the 1990s, is now moving into production and will be implemented in 2006. The General Dynamics MDARS robot can navigate semi-autonomously and detect intruders, using the MRHA software architecture planned for all unmanned military vehicles. The Seekur robot was the first commercially available robot to demonstrate MDARS-like capabilities for general use by airports, utility plants, corrections facilities and Homeland Security.FOXNews.com - Weapons Makers Unveil New Era of Counter-Terror Equipment - Local News | News Articles | National News | US NewsThe Mars rovers MER-A and MER-B (now known as Spirit rover and Opportunity rover) can find the position of the sun and navigate their own routes to destinations on the fly by:

mapping the surface with 3-D vision
computing safe and unsafe areas on the surface within that field of vision
computing optimal paths across the safe area towards the desired destination
driving along the calculated route;
repeating this cycle until either the destination is reached, or there is no known path to the destination

The planned ESA Rover, ExoMars Rover, is capable of vision based relative localisation and absolute localisation to autonomously navigate safe and efficient trajectorys to targets by:

reconstructing 3D models of the terrain surrounding the Rover using a pair of stereo cameras
determining safe and unsafe areas of the terrain and the general &#39;difficulty&#39; for the Rover to navigate the terrain
computing efficient paths across the safe area towards the desired destination
driving the Rover along the planned path
building up a &#39;Navigation Map&#39; of all past navigation data

The DARPA Grand Challenge and DARPA Urban Challenge have encouraged development of even more autonomous capabilities for ground vehicles, while this has been the demonstrated goal for aerial robots since 1990 as part of the AUVSI International Aerial Robotics Competition.
Open problems in autonomous robotics{{Expand section}}
There are several open problems in autonomous robotics which are special to the field rather than being a part of the general pursuit of AI.
Energy autonomy & foraging
Researchers concerned with creating true artificial life are concerned not only with intelligent control, but further with the capacity of the robot to find its own resources through foraging (looking for food, which includes both energy and spare parts).This is related to autonomous foraging, a concern within the sciences of behavioral ecology, social anthropology, and human behavioral ecology; as well as robotics, artificial intelligence, and artificial life.
AS(Autonomous System) that allow moving through a real world environment to perform its tasks including locomotion, localization, motion planning and sensing. 
Within the Internet, an Autonomous System (AS) is a collection of connected Internet Protocol (IP) routing prefixes under the control of one or more network operators that presents a common, clearly defined routing policy to the Internet.[1]
Originally the definition required control by a single entity, typically an Internet service provider or a very large organization with independent connections to multiple networks, that adhere to a single and clearly defined routing policy, as originally defined in RFC 1771.[2] The newer definition in RFC 1930 came into use because multiple organizations can run BGP using private AS numbers to an ISP that connects all those organizations to the Internet. Even though there may be multiple Autonomous Systems supported by the ISP, the Internet only sees the routing policy of the ISP. That ISP must have an officially registered Autonomous System Number (ASN).
A unique ASN is allocated to each AS for use in BGP routing. AS numbers are important because the ASN uniquely identifies each network on the Internet.
Until 2007, AS numbers were defined as 16-bit integers, which allowed for a maximum of 65536 assignments. The Internet Assigned Numbers Authority (IANA) has designated AS numbers 64512 through 65534 to be used for private purposes. The ASNs 0, 59392–64511, and 65535 are reserved by the IANA and should not be used in any routing environment. ASN 0 may be used to label non-routed networks. All other ASNs (1–54271) are subject to assignment by IANA, and, as of September 9, 2008, only 49152–54271 remained unassigned. RFC 4893 introduced 32-bit AS numbers, which IANA has begun to allocate. These numbers are written either as simple integers, or in the form x.y, where x and y are 16-bit numbers. Numbers of the form 0.y are exactly the old 16-bit AS numbers, 1.y numbers and 65535.65535 are reserved, and the remainder of the space is available for allocation.[3] The accepted textual representation of Autonomous System Numbers is defined in RFC 5396.[4]
See also{{Portal}}

AIBO
Artificial intelligence
Cognitive robotics
domestic robot
Driverless car
Epigenetic robotics
Evolutionary robotics
Friendly Robotics
Humanoid robot
Intelligent system
Robotic mower
Microbot
PatrolBot
Penguin Robot
Simultaneous localization and mapping
von Neumann machine
Wake-up robot problem
William Grey Walter


References


External links{{Commonscat-inline}}{{Robotics}}

bs:Adaptivni robot
fa:ربات خودمختار
he:רובוט אוטונומי
sk:Autonómny robot
th:หุ่นยนต์อัตโนมัติ
ur:خودتحکمی روبالہ