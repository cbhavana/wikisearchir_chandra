title{{Infobox person}}
Anna Louise Friel (born 12 July 1976) is an English actress. She rose to fame in the UK as Beth Jordache on the Channel 4 soap Brookside.
Early lifeFriel was born in Rochdale, Greater Manchester, the daughter of Julie, a special needs teacher, and Des Friel, a former teacher of French and owner of a web design company.{{cite web}}"Anna Friel Interview" Bryan Appleyard, The Sunday Times, 18th February 2001 Friel's Irish Catholic father, a former folk guitarist, was born in Belfast, Northern Ireland, and grew up in Donegal, Ireland.{{cite web}}{{cite news}}{{cite web}} Her brother Michael starred in Hovis television adverts.{{cite news}}She attended Crompton House Church of England High School, in High Crompton, Shaw and Crompton.{{Citation needed}} She then attended Holy Cross College in Bury.{{cite news}}She started off her Training at Oldham Theatre Workshop where she met Coronation Street actor Antony Cotton.
Acting careerAt the age of 13, she was hired for her first professional acting job in the Channel 4 drama serial G.B.H., as the daughter of Michael Palin's character. Her performance led to a series of appearances on various British television shows, including Emmerdale. In 1992, she was added to the cast on the Channel 4 soap opera Brookside, where she gained greater exposure in the role of Beth Jordache. She stayed on the programme for two years.{{Citation needed}} Friel's on-air lesbian kiss was the first in British soap history.{{cite web}} In 1995, Friel won the National Television Award for Most Popular Actress for her work on Brookside.{{cite news}}In 1996, Friel courted further controversy when she appeared in the television film The Tribe by Stephen Poliakoff, which included nudity and a much discussed ménage à trois sex scene between characters played by Friel, Jonathan Rhys Meyers and Jeremy Northam.{{cite web}}In 2001, she made her West End stage début in London in a fringe production of Lulu, which also transferred to Broadway.{{cite news}} Friel's film credits include playing Nick Leeson's wife Lisa in Rogue Trader opposite Ewan McGregor; David Leland's The Land Girls, which also starred Rachel Weisz and Catherine McCormack;{{Citation needed}} Adam Collis Sunset Strip as Tammy Franklin, opposite Jared Leto, Simon Baker and Nick Stahl; All for Love with Richard E. Grant; A Midsummer Night's Dream as Hermia, opposite Christian Bale, Dominic West and Calista Flockhart; Timeline as Gerard Butler's object of affection; Me Without You opposite Michelle Williams; and Goal! and Goal! 2: Living the Dream....Her television work includes the short lived The Jury for the Fox and ITV1's Watermelon which was based on the novel by Marian Keyes. In 2007, she started playing Charlotte "Chuck" Charles in Pushing Daisies, a new television series from the creator of Dead Like Me and Wonderfalls, on the American network ABC.{{cite web}} Although popular with critics, in November 2008, creator Bryan Fuller stated that it had been cancelled due to poor ratings.{{cite web}} The show ran for two seasons.In the pilot season following the cancellation of Pushing Daisies, Friel was offered six pilots but declined all six roles in favour of focusing on her movie career.{{cite web}}In November 2006, she was awarded an Honorary Doctorate by the University of Bolton for contributions to the performing arts.{{cite news}}She has also been seen as the face of Pantene Pro-V Ice shine in the United Kingdom, appearing in both television advertisements and print advertising.{{Citation needed}} She has also appeared in adverts on television for 3 Mobile and Virgin Atlantic Airways.{{Citation needed}}In September 2009, Friel starred in a West End adaptation of Breakfast at Tiffany's at the Theatre Royal Haymarket.{{cite news}}In April 2010, she was cast in the psychological thriller film The Dark Fields{{cite web}} (now entitled Limitless and released in March 2011). In November 2010, Friel starred in London Boulevard alongside Ray Winstone, Colin Farrell, Keira Knightley and her then partner David Thewlis.{{cite web}}Friel appears in the video for the Manic Street Preachers' September 2010 single "(It's Not War) Just the End of Love" opposite Welsh actor Michael Sheen, with whom she co-starred in the 2003 film Timeline.{{cite web}}In January 2011, Friel appeared in a episode of the  BBC series Come Fly with Me as a fictionalised version of herself.{{Citation needed}} In the show she is bald and has lost her wig.{{fact}}In December 2011, she played the star role (Ellie Manning) in ITV's series drama Without You.Without You at ITV web site That same month, she starred as Captain Elizabeth Bonny in the SyFy network's production of Neverland, a prequel to J.M. Barrie's classic story Peter Pan.{{fact}}
Personal lifeIn 2001, Friel began a relationship with actor David Thewlis, whom she met on a flight to Cannes. Their daughter, Gracie Ellen Mary Friel,{{cite news}} was born on 9 July 2005 at Portland Hospital in London. In December 2010, Friel and Thewlis ended their relationship.{{cite web}} She began dating fellow actor Rhys Ifans, with whom she acted in the SyFy mini series, Neverland, in 2011."Here comes the bride! Oh no, it's just Geri Halliwell in a HUGE white dress at Gorbachev charity gala" 23 September 2011, Mail OnlineIn 2008, Friel began supporting the Fashion Targets Breast Cancer campaign in support of Breakthrough Breast Cancer.{{cite web}}
Awards and nominations{{BLP unsourced section}}

2009 Royal Television Society North West Award – Best Performance in a Single Drama or Drama Series for The Street
2008 Golden Globe – Best Performance by an Actress in a Television Series, Comedy or Musical Nomination for Pushing Daisies (nominee)
1999 Drama Desk Awards – Outstanding Featured Actress in a Play for &#34;Closer&#34;
1995 TV Times Awards – Best Actress
1994 TV Quick Magazine – Best Newcomer
Honorary degree, awarded by University of Bolton.


Filmography

Year  Title  Role  Other notes 
1991  G.B.H.  Susan Nelson  TV mini-series 
1992  Emmerdale  Poppy Bruce  Episodes 
1993  Medics  Holly Jarrett  Episode (#3.3) 
1993–1995  Brookside  Beth Jordache  Episodes 
1995  The Imaginatively Titled Punt & Dennis Show   Episode (#2.1) 
1996  Tales from the Crypt  Angelica  Episode (&#34;About Face&#34;) 
Cadfael  Sioned  Episode (&#34;A Morbid Taste for Bones&#34;) 
1998  The Land Girls  Prue (Prudence)   
Our Mutual Friend  Bella Wilfer  Episodes (#1.1, #1.2) 
The Stringer  Helen  
 
The Tribe  Lizzie   
St. Ives  Flora Gilchrist   
1999  A Midsummer Night's Dream  Hermia   
Rogue Trader  Lisa Leeson   
Mad Cows  Maddy   
2000  Sunset Strip  Tammy Franklin   
An Everlasting Piece  Bronagh   
2001  The Fear  Storyteller  Recurring 
The War Bride  Lily   
Me Without You  Marina   
2002  Fields of Gold  Lucia Merritt  TV movie 
2003  Watermelon  Claire Ryan  TV movie 
Last Rumba in Rochdale  Bodney  Voice actor 
Timeline  Lady Claire   
2004  Perfect Strangers  Susie Wilding   
2005  Goal!  Roz Harmison   
2006  Irish Jam  Maureen   
Niagara Motel  Denise   
2007  Goal! 2: Living the Dream...  Roz Harmison   
Rubbish  Isobel   
2007–2008  Pushing Daisies  Charlotte &#34;Chuck&#34; Charles  Main cast 
2008  Bathory  Countess Erzsébet Báthory   
2009  Land of the Lost  Holly Cantrell   
The Street  Dee Purnell  Episodes (#3.2 and #3.3) 
2010  London Boulevard  Briony   
Angel Makers  Lizzie  upcoming film 
You Will Meet a Tall Dark Stranger  Iris   
2011  Neverland  Elizabeth Bonny  TV movie 
Treasure guards  Victoria Eckhart  TV movie 
Come Fly with Me  Herself  Comedy series 
Without You Ellie TV Series 
Limitless Melissa   
2012  Public Enemies  Paula Radnor  Drama series 

References{{Reflist}}
External links

{{IMDb name}}
Anna Friel's Sentimental Journey Horkins, Tony Page Six Magazine Oct. 19, 2008

{{DramaDesk PlayOutstandingFeaturedActress 1975-1999}}{{Use dmy dates}}
{{Persondata}}
{{DEFAULTSORT:Friel, Anna}}










cs:Anna Friel
de:Anna Friel
es:Anna Friel
fr:Anna Friel
ga:Anna Friel
it:Anna Friel
nl:Anna Friel
ja:アンナ・フリエル
pl:Anna Friel
pt:Anna Friel
ru:Фрил, Анна
sk:Anna Frielová
sv:Anna Friel