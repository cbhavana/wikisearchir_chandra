title{{Use dmy dates}}
{{Infobox film}}
The Fall of the Roman Empire is a 1964 English-language epic film produced by Samuel Bronston Productions and the Rank Organisation, and released by Paramount Pictures. It was directed by Anthony Mann and produced by Samuel Bronston with Jaime Prades and Michal Waszynski as associate producers. The screenplay was by Ben Barzman, Basilio Franchina and Philip Yordan. It was photographed in 70mm Ultra Panavision by Robert Krasker, with an original music score by Dimitri Tiomkin. The historian Will Durant was engaged to advise on period detail and plot. The 2000 film Gladiator was not a formal remake but has many points of contact with this movie.The film starred Sophia Loren (Lucilla), Stephen Boyd (Livius), Alec Guinness (Marcus Aurelius), James Mason (Timonides), Christopher Plummer (Commodus), Mel Ferrer (Cleander) and Omar Sharif (Sohaemus, King of Armenia) with Finlay Currie (Caecina), Anthony Quayle (Verulus), John Ireland (Ballomar), Eric Porter (Julianus), Andrew Keir (Polybius), Douglas Wilmer (Niger) and George Murcell (Victorinus).The film was a financial failure at the box-office. However, it is considered unusually intelligent and thoughtful for a film of the contemporary sword and sandal genre and also enjoys a 100% "Fresh" rating at Rotten Tomatoes.
PlotThe timeframe of the film is AD 180–192, from the last days of the Roman Emperor Marcus Aurelius to the death of his son and successor Commodus. The film opens with Marcus Aurelius conducting his war to pacify the Germanic tribes along the Danube frontier. He has just summoned the governors of all the Roman provinces to his camp in order to present to them Gaius Metellus Livius (a fictional character) as his heir and successor. This is conceivable because Livius indicates that he had been brought into the imperial family by the emperor, presumably by adoption, and the four previous emperors (Nerva, Trajan, Hadrian and Antoninus Pius) had made their adopted sons their heirs.What is foremost in Livius' mind is to marry his beloved Lucilla, the emperor's daughter, but Marcus Aurelius feels he must marry her to King Sohaemus of Armenia to cement a peace treaty between him and Rome as a buffer on Rome's eastern border to the hostile Persians (Parthians). Livius appeals to her to go away with him, but her duty overrules her heart.Commodus is devastated when he learns of his father's intention to make Livius his successor. This makes Commodus reckless and hostile toward Livius, who had once been a close friend. Later Commodus would explain that this was his way of letting the gods decide who would be successor. Conspirators, acting independently of Commodus, poison Marcus Aurelius, leaving Livius with only one choice: to proclaim Commodus as "undoubted Caesar." Commodus gratefully makes Livius commander of all Roman armies and his second-in-command.Commodus begins his reign by opposing Marcus Aurelius' policy of peace and freedom, which he characterizes as weakness, and demanding more taxes and tribute from the eastern provinces of Syria and Egypt, driving them to rebellion. Meanwhile Livius pacifies the northern frontier by following Marcus Aurelius' policy of making "human frontiers" for the Roman Empire. He and Timonides, a Greek Christian freedman and friend of Marcus Aurelius, return to Rome along with the conquered German leaders (Ballomar, et al.) with the proposal to Romanize and settle them on abandoned farm land. This proposal is accepted by the Roman Senate, but it sets Livius at odds with Commodus, who all but banishes him to continued duty on the northern frontier. When Commodus is faced with the defection of the eastern provinces and Armenia, he has no one but Livius to turn to. Livius proves to be successful against the eastern rebellion, but he declines following Commodus' demand for brutal retribution. Livius answers Commodus with the demand for either a new Rome or a new Caesar.Commodus reacts by bribing the senators, plebians, and the army to side with him against Livius. Commodus's success is apparent when the traditional head for the colossal statue of Jupiter in the Capitoline temple is replaced with a likeness of the head of Commodus, and in the senate it is proposed to change Rome's name to "the city of Commodus" and the empire's name to "the empire of Commodus." Livius is arrested and is set to be executed with Lucilla, who had tried to assassinate her brother. Commodus returns Livius' favor to him in proclaiming him undoubted Caesar by challenging him to a gladiatorial combat with the imperial throne as the prize. Livius prevails by killing Commodus and rescuing Lucilla from the flames of execution. Victorinus, who had been bribed by Commodus to deliver Livius's army to him, proclaims Livius Caesar, but Livius declines with the warning that if he were Caesar he would crucify them all (Victorinus, Julianus, Niger, et al.). The film culminates in an auction for the imperial throne. The narrator sums it up: "This was the beginning of the fall of the Roman Empire. A great civilization is not conquered from without until it has destroyed itself from within."Allmovie Guide plot description at Answers.com This actually occurred in the accession of Didius Julianus.
ProductionThe Fall of the Roman Empire was one of Samuel Bronston's superproductions in Spain, with Marcus Aurelius's winter camp on the Danube shot in snow in the Sierra de Guadarrama, northern Madrid. The 'Battle of the Four Armies' involved 8,000 soldiers including 1,200 cavalry and was shot on an undulating plain at Manzanares el Real which allowed large numbers of soldiers to be visible over a long distance.The film's reconstruction of the Roman Forum at Las Matas near Madrid, at 400 x 230 meters (1312 x 754 feet) holds the record for the largest outdoor film set. The various ancient Rome settings covered {{convert}}.The Fall of the Roman Empire was a costly financial failure for producer Samuel Bronston who, after making such epics as John Paul Jones (1959), King of Kings (1961), El Cid (1961), and 55 Days at Peking (1963) had to stop all business activities. A bankruptcy notice in the New York Times on 6 August 1965, stated the cost of The Fall of the Roman Empire at $18,436,625. He announced his return with a planned epic about Isabella of Spain, but the film was never made.The Fall of the Roman Empire was one of the few Ultra Panavision 70 films not exhibited in Cinerama.In later years, Miramax would acquire the US distribution rights to the film. After the founders Bob and Harvey Weinstein split with Miramax parent Disney, they formed the Weinstein Company, who currently owns US distribution rights.UK distribution rights would pass to PolyGram Filmed Entertainment and subsequently Universal Studios.
Musical scoreDimitri Tiomkin's score, which is one of the notable features of the film, is more than 150 minutes in length. It is scored for a large orchestra, including an important part for cathedral organ. Several cues are extended compositions in their own right. These include Pax Romana in which Marcus Aurelius summons the governors of all the Roman provinces. Although Christopher Palmer stated in his book on film music, The Composer in Hollywood, that it was a march, the cue is actually in the style of a bolero. Other notable cues include those for The Roman Forum, composed to accompany Commodus's triumphal return to Rome as the newly-installed Emperor; a percussive scherzo for a barbarian attack by Ballomar's army; the Tarantella danced by the Roman mob on the evening presaging the gladiatorial combat between Livius and Commodus (which seems to be modelled on the Tarantella movement from the Piano Concerto of Tiomkin's teacher Ferruccio Busoni). The score was recorded by the Sinfonia of London (uncredited) at Shepperton Studios. The music editor was George Korngold, son of Erich Wolfgang Korngold. A sound track album was released by Columbia Records to coincide with the release of the film.
Cast

Actor Role 
Sophia Loren Lucilla 
Stephen Boyd Livius 
Alec Guinness Marcus Aurelius 
James Mason Timonides 
Christopher Plummer Commodus 
Anthony Quayle Verulus 
John Ireland Ballomar 
Omar Sharif Sohaemus 
Mel Ferrer Cleander 
Eric Porter Julianus 
Finlay Currie Senator 
Andrew Keir  Polybius 
Douglas Wilmer Niger 
George Murcell  Victorinus 
Norman Wooland  Virgilianus 
It was envisioned that Charlton Heston would be cast as Livius, but ultimately Stephen Boyd, who played opposite to Heston in Ben-Hur, got the part. It had been offered to Kirk Douglas, who turned it down.Richard Harris was originally cast as Commodus, but he was replaced by Christopher Plummer. Harris would later play the role of Marcus Aurelius in the 2000 film Gladiator. According to his published diaries, Charlton Heston also refused the role, mainly because he had recently appeared in El Cid and 55 Days at Peking. (It is also rumored that Charlton Heston turned down any part in the movie due to bad blood between himself and Sophia Loren, stemming from El Cid.)Alec Guinness was cast as Emperor Marcus Aurelius, and during the production he became good friends with Sophia Loren. On an evening out Sophia persuaded Alec to dance "The Twist" with her, which he did for the first time in his life. On the flight to Spain, Guinness was reading the script of the film when he was accosted by one of its authors. Guinness was asked if he was studying the lines, but he responded that he was rewriting the lines since he did not think much of them.Sara Montiel was cast as Lucilla but she turned it down.Sophia Loren, the heroine Lucilla, was the highest paid cast member at $1&nbsp;million.
Awards

Golden Globe Award for Best Original Score (nominated &#38; won)
Academy Award for Best Music, Score – Substantially Original (nominated)


NovelizationA novel based on the film is The Fall of the Roman Empire by Harry Whittington (Fawcett Publications, Inc. & Frederick Muller Ltd., 1964).
Home mediaThe first English-language DVD release was the basic theatrical release of the film, running for 2 hours 52 minutes, was first issued on DVD in 2004. A French DVD release, with sub-titles and/or French dubbing, and a full stereo soundtrack in both, had appeared in 2001. A deluxe edition containing two-disks and a limited collector's edition containing three disks were released on 29 April 2008, but they do not feature lost footage discovered too late to be included. This footage will be featured in an upcoming edition.http://www.amazon.com/dp/B00125WAXM The Blu-ray Disc was released in the United Kingdom on May 16, 2011.
See also

List of historical drama films
List of films set in ancient Rome
List of American films of 1964
List of Roman Emperors
The Five Good Emperors, of which Marcus Aurelius was the last
Decline of the Roman Empire


References{{Reflist}}
External links

{{IMDB title}}
{{Amg movie}}
Movie stills
New York Times movie review

{{Anthony Mann}}{{DEFAULTSORT:Fall Of The Roman Empire, The}}













ca:La caiguda de l'Imperi Romà
de:Der Untergang des Römischen Reiches
es:La caída del Imperio Romano
fr:La Chute de l'empire romain
it:La caduta dell'impero romano
nl:The Fall of the Roman Empire
ja:ローマ帝国の滅亡
pl:Upadek Cesarstwa Rzymskiego
pt:A Queda do Império Romano
ro:Căderea Imperiului Roman (film)
ru:Падение Римской империи (фильм)
sr:Пад Римског царства (филм)
sh:The Fall of the Roman Empire
sv:Romarrikets fall (film)
tr:Roma İmparatorluğu'nun Çöküşü (film)