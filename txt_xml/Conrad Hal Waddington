title{{Infobox scientist}}Conrad Hal Waddington CBE FRS FRSE (1905&ndash;1975) was a developmental biologist, paleontologist, geneticist, embryologist and philosopher who laid the foundations for systems biology.  He had wide interests that included poetry and painting, as well as left-wing political leanings.{{Citation needed}}
LifeWaddington, known as "Wad" to his friends and "Con" to family, was born to Hal and Mary Ellen (Warner) Waddington, 8 November 1905. Until nearly three years of age, Waddington lived with his parents in India, where his father worked on a tea estate in the Wayanad district. In 1910, at the age of four, he was sent to live with family in England including his aunt, uncle, and Quaker grandmother. His parents remained in India until 1928. During his childhood, he was particularly attached to a local druggist and distant relation, Dr. Doeg. Doeg, who Waddington called "Grandpa", introduced Waddington to a wide range of sciences from chemistry to geology.Robertson, Alan. 1977. "Conrad Hal Waddington. 8 November 1905&ndash;26 September 1975." Biographical Memoirs of Fellows of the Royal Society 23, 575-622. Pp. 575-76. During the year following the completion of his entrance exams to university, Waddington received an intense course in chemistry from E. J. Holmyard. Aside from being "something of a genius of a [chemistry] teacher," Holmyard introduced Waddington to the "Alexandrian Gnostics" and the "Arabic Alchemists." From these lessons in metaphysics, Waddington first gained an appreciation for interconnected holistic systems. Waddington reflected that this early education prepared him for Alfred North Whitehead's philosophy in the 1920s and 30s and the cybernetics of Norbert Wiener and others in the 1940s.Waddington, C. H. 1975. The Evolution of an Evolutionist. Ithica, NY: Cornell University Press. Pg. 2.He attended Clifton College and Sidney Sussex College, Cambridge. He took the Natural Sciences Tripos, earning a First in Part II in geology in 1926.Robertson, Alan. 1977. "Conrad Hal Waddington. 8 November 1905 &mdash; 26 September 1975." Biographical Memoirs of Fellows of the Royal Society 23, 575-622. Pg 577. He took up a Lecturership in Zoology and was a Fellow of Christ's College until 1942. His friends included Gregory Bateson, Walter Gropius, C. P. Snow, Solly Zuckerman, Joseph Needham, and J. D. Bernal.Robertson, Alan. 1977. "Conrad Hal Waddington. 8 November 1905 &mdash; 26 September 1975." Biographical Memoirs of Fellows of the Royal Society 23, 575-622. Pp. 579-580.Yoxen, Edward. 1986. "Form and Strategy in Biology: Reflections on the Career of C. H. Waddington." In A History of Embryology, edited by T. J Horder, J. A Witkowski, and C. C Wylie. Cambridge: Cambridge University Press. Pp. 310-11. His interests began with palaeontology but moved on to the heredity and development of living things. He also studied philosophy.During World War II he was involved in operational research with the Royal Air Force and became scientific advisor to the Commander in Chief of Coastal Command from 1944 to 1945.  After the war he became Professor of Animal Genetics at the University of Edinburgh.  He would stay at Edinburgh for the rest of life with the exception of one year (1960–1961) when he was a Fellow on the faculty in the Center for Advanced Studies at Wesleyan University in Middletown, Connecticut.{{cite web}}  His personal papers are largely kept at the University of Edinburgh library.Waddington was married twice. His first marriage produced a son, C. Jake Waddington, professor of physics at the University of Minnesota, but ended in 1936. He then married Justin Blanco White, daughter of the writer Amber Reeves, with whom he had two daughters, mathematician Dusa McDuff and anthropologist Caroline Humphrey.Robertson, Alan. 1977. Conrad Hal Waddington. 8 November 1905 &mdash; 26 September 1975. Biographical Memoirs of Fellows of the Royal Society 23, 575-622. P. 578In the early 1930s, Waddington and many other embryologists looked for the molecules that would induce the amphibian neural tube. The search was, of course, beyond the technology of that time, and most embryologists moved away from such deep problems. Waddington, however, came to the view that the answers to embryology lay in genetics, and in 1935 went to Thomas Hunt Morgan's Drosophila laboratory in California, even though this was a time when most embryologists felt that genes were unimportant and just played a role in minor phenomena such as eye colour.In the late 30's, Waddington produced formal models about how gene regulatory products could generate developmental phenomena, showed how the mechanisms underpinning Drosophila development could be studied through a systematic analysis of mutations that affected the development of the Drosophila wing (this was the essence of the approach that won the 1995 Nobel prize in medicine for Christiane Nüsslein-Volhard and Eric F. Wieschaus). In a period of great creativity at the end of the 1930s, he also discovered mutations that affected cell phenotypes and wrote his first textbook of developmental epigenetics, a term that then meant the external manifestation of genetic activity.Waddington also coined other essential concepts, such as canalisation, which refers to the ability of an organism to produce the same phenotype despite variation in genotype or environment. He also identified a mechanism called genetic assimilation which would allow an animal’s response to an environmental stress to become a fixed part of its developmental repertoire, and then went on to show that the mechanism would work. He thus demonstrated that the ideas of inheritance put forward by Jean-Baptiste Lamarck could, in principle at least, occur.In 1972, Waddington founded the Centre for Human Ecology.
Epigenetic landscapeWaddington's epigenetic landscape is a metaphor for how gene regulation modulates development.Goldberg, A. D., Allis, C. D., & Bernstein, E. (2007). Epigenetics: A landscape takes shape. Cell, 128, 635-638. One is asked to imagine a number of marbles rolling down a hill towards a wall. The marbles will compete for the grooves on the slope, and come to rest at the lowest points. These points represent the eventual cell fates, that is, tissue types. Waddington coined the term Chreode to represent this cellular developmental process. This idea was actually based on experiment: Waddington found that one effect of mutation (which could modulate the epigenetic landscape) was to affect how cells differentiated. He also showed how mutation could affect the landscape and used this metaphor in his discussions on evolution&mdash;he was the first person to emphasise that evolution mainly occurred through mutations that affected developmental anatomy.
Waddington as an organiser{{Expand section}}
Waddington was very active in advancing biology as a discipline. He contributed to a book on the role of the sciences in times of war, and helped set up several professional bodies representing biology as a discipline.http://www.ncbi.nlm.nih.gov/entrez/utils/fref.fcgi?PrId=3058&itool=Abstract-def&uid=14760651&nlmid=101168228&db=pubmed&url=http://dx.doi.org/10.1002/jez.b.20002 Hall BK. 2004. In search of evolutionary developmental mechanisms: the 30-year gap between 1944 and 1974. J Exp Zool B Mol Dev Evol. 2004 Jan 15;302(1):5-18.A remarkable number of his contemporary colleagues in Edinburgh became Fellows of the Royal Society during his time there, or shortly thereafter.Robertson, Alan. 1977. Conrad Hal Waddington. 8 November 1905 &mdash; 26 September 1975. Biographical Memoirs of Fellows of the Royal Society 23, 575-622. P. 585.Waddington was an old-fashioned intellectual who lived in both the arts and science milieus of the 1950s and wrote widely. His 1960 book "Behind Appearance; a Study Of The Relations Between Painting And The Natural Sciences In This Century" (MIT press) not only has wonderful pictures but is still worth reading.Waddington was, without doubt, the most original and important thinker about developmental biology of the pre-molecular age and the medal of the British Society for Developmental Biology is named after him.
References{{reflist}}
Selected works
Books

Waddington, C. H. (1939). An Introduction to Modern Genetics. London : George Alien &#38; Unwin Ltd.
Waddington, C. H. (1940). Organisers & genes. Cambridge: Cambridge University Press.
Waddington, C. H. (1941). The Scientific Attitude, Pelican Books
Waddington, C. H. (1946). How animals develop. London : George Allen &#38; Unwin Ltd.
Waddington, C. H. (1953). The Epigenetics of birds. Cambridge : Cambridge University Press.
Waddington, C. H. (1956). Principles of Embryology. London : George Allen &#38; Unwin.
Waddington, C. H. (1957). The Strategy of the Genes. London : George Allen &#38; Unwin.
Waddington, C. H. (1959). Biological organisation cellular and subcellular : proceedings of a Symposium. London: Pergamon Press.
Waddington, C. H. (1960). The ethical animal. London : George Allen &#38; Unwin.
Waddington, C. H. (1961). The human evolutionary system. In: Michael Banton (Ed.), Darwinism and the Study of Society. London: Tavistock.
Waddington, C. H. (1966). Principles of development and differentiation. New York: Macmillan Company.
Waddington, C. H. (1966). New patterns in genetics and development. New York: Columbia University Press.
Waddington, C. H., ed. (1968–72). Towards a Theoretical Biology. 4 vols. Edinburgh: Edinburgh University Press.
Waddington, C. H., Kenny, A., Longuet-Higgins, H.C., Lucas, J.R. (1972). The Nature of Mind, Edinburgh: Edinburgh University Press (1971-3 Gifford Lectures in Edinburgh, online)
Waddington, C. H., Kenny, A., Longuet-Higgins, H.C., Lucas, J.R. (1973). The Development of Mind, Edinburgh: Edinburgh University Press (1971-3 Gifford Lectures in Edinburgh, online)
Waddington, C. H. (1977) (published posthumously). Tools for Thought. London: Jonathan Cape Ltd.


Papers

Waddington C. H. 1942. Canalization of development and the inheritance of acquired characters. Nature 150:563-565.
Waddington, C. H. 1953. Genetic assimilation of an acquired character. Evolution  7: 118-126.
Waddington, C. H. 1953. Epigenetics and evolution. Symp. Soc. Exp. Biol 7:186-199.
Waddington, C. H. 1956. Genetic assimilation of the bithorax phenotype. Evolution 10: 1-13.
Waddington, C. H. 1961. Genetic assimilation. Advances Genet. 10: 257-290.
Waddington, C. H. 1974. A Catastrophe Theory of Evolution. Annals of the New York Academy of Sciences 231: 32-42.


External links

NAHSTE Project Record of C.H. Waddington
Induction and the Origin of Developmental Genetics - works by Salome Gluecksohn-Schoenhimer and Conrad Hal Waddington
Epigenetics News

{{genarch}}{{Use dmy dates}}{{Persondata}}
{{DEFAULTSORT:Waddington, Conrad Hal}}














de:Conrad Hal Waddington
et:Conrad Hal Waddington
es:Conrad Hal Waddington
fr:Conrad Hal Waddington