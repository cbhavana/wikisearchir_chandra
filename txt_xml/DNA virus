titleA DNA virus is a virus that has DNA as its genetic material and replicates using a DNA-dependent DNA polymerase. The nucleic acid is usually double-stranded DNA (dsDNA) but may also be single-stranded DNA (ssDNA). DNA viruses belong to either Group I or Group II of the Baltimore classification system for viruses. Single-stranded DNA is usually expanded to double-stranded in infected cells. Although Group VII viruses such as hepatitis B contain a DNA genome, they are not considered DNA viruses according to the Baltimore classification, but rather reverse transcribing viruses because they replicate through an RNA intermediate.
Group I: dsDNA viruses
Genome organization within this group varies considerably. Some have circular genomes (Baculoviridae, Papovaviridae and Polydnaviridae) while others have linear genomes (Adenoviridae, Herpesviridae and some phages). Some families have circularly permuted linear genomes (phage T4 and some Iridoviridae). Others have linear genomes with covalently closed ends (Poxviridae and Phycodnaviridae).A virus infecting archaea was first described in 1974. Several others have been described since: most have head-tail morphologies and linear double-stranded DNA genomes. Other morphologies have also been described: spindle shaped, rod shaped, filamentous, icosahedral and spherical. Additional morphological types may exist. Orders within this group are defined on the basis of morphology rather than DNA sequence similarity. It is thought that morphology is more conserved in this group than sequence similarity or gene order which is extremely variable. Three orders and 33 families are currently recognised. Four genera are recognised that have not yet been assigned a family. The species Sulfolobus turreted icosahedral virus is so unlike any previously described virus that it will almost certainly be placed in a new family on the next revision of viral families.Fifteen families are enveloped. These include all three families in the order Herpesvirales and the following families: Ascoviridae, Ampullaviridae, Asfarviridae, Baculoviridae, Fuselloviridae, Globuloviridae, Guttaviridae, Hytrosaviridae, Iridoviridae, Lipothrixviridae, Nimaviridae and Poxviridae.Bacteriophages belonging to the families Tectiviridae and Corticoviridae have a lipid bilayer inside the icosahedral protein capsid and the membrane surrounds the genome. The crenarchaeal virus Sulfolobus turreted icosahedral virus has a similar structure.The genomes in this group vary considerably from ~20 kilobases to over 1.2 megabases in length.
Host range
Species of the order Caudovirales and of the families Corticoviridae and Tectiviridae infect bacteria.Species of the order Ligamenvirales and the families Ampullaviridae,  Bacilloviridae, Bicaudaviridae, Clavaviridae, Fuselloviridae, Globuloviridae and Guttaviridae infect hyperthermophilic archaea species of the Crenarchaeota.Species of the order Herpesvirales and of the families Adenoviridae, Asfarviridae, Iridoviridae, Papillomaviridae, Polyomaviridae and Poxviridae infect vertebrates.Species of the families Ascovirus, Baculovirus, Hytrosaviridae, Iridoviridae and Polydnaviruses and of the genus Nudivirus infect insects.Species of the families Marseilleviridae, Megaviridae and Mimiviridae and the species Mavirus virophage and Sputnik virophage infect protozoa.Species of the family Nimaviridae infect crustaceans.Species of the family Phycodnaviridae and the species Organic Lake virophage infect algae. These are the only known dsDNA viruses that infect plants.Species of the family Plasmaviridae infect species of the class Mollicutes.Species of the genus Dinodnavirus infect dinoflagellates. These are the only known viruses that infect dinoflagellates.Species of the genus Rhizidiovirus infect stramenopiles. These are the only known dsDNA viruses that infect stramenopiles.Species of the genus Salterprovirus infect halophilic archaea species of the Euryarchaeota.
Taxonomy


Order Caudovirales
Family Myoviridae - includes Enterobacteria phage T4
Family Podoviridae - includes Enterobacteria phage T7
Family Siphoviridae - includes Enterobacteria phage &lambda;



Order Herpesvirales
Family Alloherpesviridae
Family Herpesviridae - includes human herpesviruses, Varicella Zoster virus
Family Malacoherpesviridae



Order Ligamenvirales
Family Lipothrixviridae
Family Rudiviridae



Unassigned families
Family Adenoviridae - includes viruses which cause human adenovirus infection
Family Ampullaviridae
Family Ascoviridae
Family Asfarviridae - includes African swine fever virus
Family Bacilloviridae
Family Baculoviridae
Family Bicaudaviridae
Family Clavaviridae
Family Corticoviridae
Family Fuselloviridae
Family Globuloviridae
Family Guttaviridae
Family Hytrosaviridae
Family Iridoviridae
Family Marseilleviridae
Family Megaviridae
Family Mimiviridae
Family Nimaviridae
Family Papillomaviridae
Family Phycodnaviridae
Family Plasmaviridae
Family Polydnaviruses
Family Polyomaviridae - includes Simian virus 40, JC virus, BK virus
Family Poxviridae - includes Cowpox virus, smallpox
Family Tectiviridae



Unassigned genera
Dinodnavirus
Nudivirus
Salterprovirus
Rhizidiovirus



Unassigned species
Bandicoot papillomatosis carcinomatosis virus
KIs-V
Haloarcula hispanica pleomorphic virus 1
Haloarcula hispanica SH1 virus
Mavirus virophage 
Organic Lake virophage
Sputnik virophage
Sulfolobus turreted icosahedral virus
Thermus thermophilus virus P23-77


Group II: ssDNA viruses
Although bacteriophages were first described in 1927, it was only in 1959 that Sinshemer working with the phage X174 showed that they could possess single stranded DNA genomes.Sinshemer RL (1959) J Mol Biol 1:37Sinshemer RL (1959) J Mol Biol 1:43 Despite this discovery until relatively recently it was believed that the majority of DNA viruses belonged to the double stranded clade. Recent work suggests that this may not be the case with single stranded viruses forming the majority of viruses found in sea water, fresh water, sediment, terrestrial, extreme, metazoan-associated and marine microbial mats.Desnues C, Rodriguez-Brito B, Rayhawk S, Kelley S, Tran T, Haynes M, Liu H, Furlan M, Wegley L, Chau B, Ruan Y, Hall D, Angly FE, Edwards RA, Li L, Thurber RV, Reid RP, Siefert J, Souza V, Valentine DL, Swan BK, Breitbart M, Rohwer F (2008) Biodiversity and biogeography of phages in modern stromatolites and thrombolites.Nature 452(7185):340-343Angly FE, Felts B, Breitbart M, Salamon P, Edwards RA, Carlson C, Chan AM, Haynes M, Kelley S, Liu H, Mahaffy JM, Mueller JE, Nulton J, Olson R, Parsons R, Rayhawk S, Suttle CA, Rohwer F (2006) The marine viromes of four oceanic regions. PLoS Biol 4(11):e368. The majority of these viruses have yet to be classified and assigned to genera and higher taxa. Because most of these viruses do not appear to be related or are only distantly related to known viruses additional taxa will be created for these.All viruses in this group require formation of a replicative form - a double stranded DNA intermediate - for genome replication. This is normally created from the viral DNA with the assistance of the host's own DNA polymerase. The evolutionary history of this group is currently poorly understood. However the parvoviruses have frequently invaded the germ lines of diverse animal species including mammals, fishes, birds, tunicates, arthropods and flatworms.Belyi VA, Levine AJ, Skalka AM (2010) Sequences from ancestral single-stranded DNA viruses in vertebrate genomes: the parvoviridae and circoviridae are more than 40 to 50 million years old. J Virol 84(23):12458-12462Liu H, Fu Y, Xie J, Cheng J, Ghabrial SA, Li G, Peng Y, Yi X, Jiang D. (2011) Widespread endogenization of Densoviruses and Parvoviruses in animal and human genomes. J Virol 85(19):9863-9876 In particular they have been associated with the human genome for ~98 million years.     
Host range
Families in this group have been assigned on the basis of the nature of the genome (circular or linear) and the host range. Eight families are currently recognised.The family Parvoviridae all have linear genomes while the other families have circular genomes. The Inoviridae and Microviridae infect bacteria; the Anelloviridae and Circoviridae infect animals (mammals and birds respectively); and the Geminiviridae and Nanoviridae infect plants. In both the Geminiviridae and Nanoviridae the genome is composed of more than a single chromosome. The Bacillariodnaviridae infect diatoms and have a unique genome: the major chromosome is circular (~6 kilobases in length): the minor chromosome is linear (~1 kilobase in length) and complementary to part of the major chromosome.
Taxonomy



Family Anelloviridae
Family Bacillariodnaviridae
Family Circoviridae
Family Geminiviridae
Family Inoviridae
Family Microviridae
Family Nanoviridae
Family Parvoviridae - includes Parvovirus B19


Unassigned species
A number of additional single stranded DNA viruses are known but are as yet unclassified. Among these are the parvovirus like viruses. These have linear single stranded DNA genomes but unlike the parvoviruses the genome is bipartate. This group includes the Bombyx mori densovirus type 2, Hepatopancreatic parvo-like virus and Lymphoidal parvo-like virus. A new family Bidensoviridae has been proposed for this group but this proposal has not been ratified by the ICTV to date.Tijssen P, Bergoin M (1995) Densonucleosis viruses constitute an increasingly diversified subfamily among the parvoviruses. Seminars in Virology 6, 347-355. Their closest relations appear to be the Brevidensoviruses (family Parvoviridae).Sukhumsirichart W, Attasart P, Boonsaeng V, Panyim S (2006) Complete nucleotide sequence and genomic organization of hepatopancreatic parvovirus (HPV) of Penaeus monodon. Virology 346(2):266-277Another new genus - as yet unnamed - has been proposed.Kim HK, Park SJ, Nguyen VG, Song DS, Moon HJ, Kang BK, Park BK (2011) Identification of a novel single stranded circular DNA virus from bovine stool. J Gen Virol This genus includes the species bovine stool associated circular virus and chimpanzee stool associated circular virus.Blinkova O, Victoria J, Li Y, Keele BF, Sanz C, Ndjango JB, Peeters M, Travis D, Lonsdorf EV, Wilson ML, Pusey AE, Hahn BH, Delwart EL (2010) Novel circular DNA viruses in stool samples of wild-living chimpanzees. J Gen Virol 91(Pt 1):74-86 The closest relations to this genus appear to be the Nanoviridae but further work will be needed to confirm this.A virus with a circular genome - sea turtle tornovirus 1 - has been isolated from a sea turtle with fibropapillomatosis.Ng TF, Manire C, Borrowman K, Langer T, Ehrhart L, Breitbart M (2009) Discovery of a novel single-stranded DNA virus from a sea turtle fibropapilloma by using viral metagenomics. J Virol 83(6):2500-2509 It is sufficiently unrelated to any other known virus that it may belong to a new family. The closest relations seem to be the Gyrovirinae. The proposed genus name for this virus is Tornovirus. Although ~50 archaeal viruses are known all the DNA viruses infecting archaea, with one known exception, have double stranded genomes. This exception is the Halorubrum pleomorphic virus 1 which has a unique structure and a circular genome.Pietilä MK, Laurinavicius S, Sund J, Roine E, Bamford DH (2010) The single-stranded DNA genome of novel archaeal virus Halorubrum pleomorphic virus 1 is enclosed in the envelope decorated with glycoprotein spikes. J Virol 84(2):788-798Most known fungal viruses have either double stranded DNA or RNA genomes. A single stranded DNA fungal virus - Sclerotinia sclerotiorum hypovirulence associated DNA virus 1 - has been described.Yu X, Li B, Fu Y, Jiang D, Ghabrial SA, Li G, Peng Y, Xie J, Cheng J, Huang J, Yi X (2010) A geminivirus-related DNA mycovirus that confers hypovirulence to a plant pathogenic fungus. Proc Natl Acad Sci USA 107(18):8387-8392 This virus appears to be related to the Geminiviridae but is distinct from them.An unusual - and as yet unnamed - virus has been isolated from the flatwom Girardia tigrina.Rebrikov DV, Bulina ME, Bogdanova EA, Vagner LL, Lukyanov SA (2002) Complete genome sequence of a novel extrachromosomal virus-like element identified in planarian Girardia tigrina. BMC Genomics 13;3:15. Because of its genome organisation, this virus appears to belong to an entirely new family. It is the first virus to be isolated from a flatworm.
Satellite viruses
Satellite viruses are small viruses with either RNA or DNA as their genomic material that require another virus to replicate. There are two types of DNA satellite viruses - the alphasatellites and the betasatellites - both of which are dependent on begomaviruses. At present satellite viruses are not classified into genera or higher taxa. Alphasatellites are small circular single strand DNA viruses that require a begomovirus for transmission. Betasatellites are small linear single stranded DNA viruses that require a begomovirus to replicate.
Notes

NCLDVs{{main}}
The asfarviruses, iridoviruses, mimiviruses, phycodnaviruses and poxviruses have been shown to belong to a single group.Iyer LM, Balaji S, Koonin EV, Aravind L (2006) Evolutionary genomics of nucleo-cytoplasmic large DNA viruses. Virus Res. 117(1):156-184 - the large nuclear and cytoplasmic DNA viruses. These are also abbreviated "NCLDV".{{cite journal}} This clade can be divided into two groups:

the iridoviruses-phycodnaviruses-mimiviruses group. The phycodnaviruses and mimiviruses are sister clades.
the poxvirus-asfarviruses group.

It is probable that these viruses evolved before the separation of eukaryoyes into the extant crown groups. The ancestral genome was complex with at least 41 genes including (1) the replication machinery (2) up to four RNA polymerase subunits (3) at least three transcription factors (4) capping and polyadenylation enzymes (5) the DNA packaging apparatus (6) and structural components of an icosahedral capsid and the viral membrane.
Bacteriophage evolution
Bacteriophages occur in over 140 bacterial or archaeal genera.Ackermann HW (2003) Bacteriophage observations and evolution. Res Microbiol 154(4):245-251 They arose repeatedly in different hosts and there are at least 11 separate lines of descent. Over 5100 bacteriophages have been examined in the electron microscope since 1959. Of these at least 4950 phages (96%) have tails. Of the tailed phages 61% have long, noncontractile tails (Siphoviridae). Tailed phages appear to be monophyletic and are the oldest known virus group.
Phylogenetic relationships
The family Ascoviridae appear to have evolved from the Iridoviridae.Federici BA, Bideshi DK, Tan Y, Spears T, Bigot Y (2009) Ascoviruses: superb manipulators of apoptosis for viral replication and transmission. Curr Top Microbiol Immunol 328:171-196. The family Polydnaviridae may have evolved from the Ascoviridae. Molecular evidence suggests that the Phycodnaviridae  may have evolved from the family Iridoviridae.Wilson WH, Van Etten JL, Allen MJ (2009) The Phycodnaviridae: the story of how tiny giants rule the world. Curr Top Microbiol Immunol 328:1-42 These four families (Ascoviridae, Iridoviridae, Phycodnaviridae and Polydnaviridae) may form a clade but more work is needed to confirm this.  Based on the genome organisation and DNA replication mechanism it seems that phylogenetic relationships may exist between the rudiviruses (Rudiviridae) and the large eukaryal DNA viruses: the African swine fever virus (Asfarviridae), Chlorella viruses (Phycodnaviridae) and poxviruses (Poxviridae).Prangishvili D, Garrett RA (2004) Exceptionally diverse morphotypes and genomes of crenarchaeal hyperthermophilic viruses. Biochem Soc Trans 32(Pt 2):204-208The nucleocytoplasmic large DNA virus group (Asfarviridae, Iridoviridae, Marseilleviridae, Mimiviridae, Phycodnaviridae and Poxviridae) along with three other families - Adenoviridae, Cortiviridae and Tectiviridae - and the phage Sulfolobus turreted icosahedral virus and the satellite virus Sputnik all possess double β-barrel major capsid proteins suggesting a common origin.Krupovi M, Bamford DH (2008) Virus evolution: how far does the double beta-barrel viral lineage extend? Nat. Rev. Microbiol 6:941-948Based on the analysis of the DNA polymerase the genus Dinodnavirus may be a member of the family Asfarviridae.Ogata H, Toyoda K, Tomaru Y, Nakayama N, Shirai Y, Claverie JM, Nagasaki K (2009) Remarkable sequence similarity between the dinoflagellate-infecting marine girus and the terrestrial pathogen African swine fever virus. Virol J 6:178 Further work on this virus will required before a final assignment can be made.The families Adenoviridae and Tectiviridae appear to be related structurally.Benson SD, Bamford JK, Bamford DH, Burnett RM (1999) Viral evolution revealed by bacteriophage PRD1 and human adenovirus coat protein structures. Cell 98(6):825-833.Based on the analysis of the coat protein Sulfolobus turreted icosahedral virus may share a common ancestry with the Tectiviridae.A protein common to the families Bicaudaviridae, Lipotrixviridae and Rudiviridae and the unclassified virus Sulfolobus turreted icosahedral virus is known suggesting a common origin.Keller J, Leulliot N, Cambillau C, Campanacci V, Porciero S, Prangishvilli D, Forterre P, Cortez D, Quevillon-Cheruel S, van Tilbeurgh H (2007) Crystal structure of AFV3-109, a highly conserved protein from crenarchaeal viruses. Virol J 22;4:12Examination of the pol genes that encode the DNA dependent DNA polymerase in various groups of viruses suggests a number of possible evolutionary relationships.Knopf CW (1998) Evolution of viral DNA-dependent DNA polymerases. Virus Genes 16(1):47-58 All know viral DNA polymerases belong to the DNA pol families A and B. All possess a 3'-5'-exonuclease domain with three sequence motifs Exo I, Exo II and Exo III. The families A and B are distinguishable with family A Pol sharing 9 distinct consensus sequences and only two of them are convincingly homologous to sequence motif B of family B. The putative sequence motifs A, B, and C of the polymerase domain are located near the C-terminus in family A Pol and more central in family B Pol. Phylogenetic analysis of these genes places the adenoviruses (Adenoviridae), bacteriophages (Caudovirales) and the plant and fungal linear plasmids into a single clade. A second clade includes the  alpha- and delta-like viral Pol from insect ascovirus (Ascoviridae), mammalian herpesviruses (Herpesviridae), fish lymphocystis disease virus (Iridoviridae) and chlorella virus (Phycoviridae). The pol genes of the African swine fever virus (Asfarviridae), baculoviruses (Baculoviridae), fish herpesvirus (Herpesviridae), T-even bacteriophages (Myoviridae) and poxviruses (Poxviridae) were not clearly resolved. A second study showed that poxvirus, baculovirus and the animal herpesviruses form separate and distinct clades.Villarreal LP, DeFilippis VR (2000) A hypothesis for DNA viruses as the origin of eukaryotic replication proteins. J Virol 74(15):7079-7084 Their relationship to the Asfarviridae and the Myoviridae was not examined and remains unclear.  The polymerases from the archaea are similar to family B DNA Pols. The T4-like viruses infect both bacteria and archaeaZillig W, Prangishvilli D, Schleper C, Elferink M, Holz I, Albers S, Janekovic D, Götz D (1996) Viruses, plasmids and other genetic elements of thermophilic and hyperthermophilic Archaea. FEMS Microbiol Rev 18(2-3):225-236 and their pol gene resembles that of eukaryotes. The DNA polymerase of mitochondria resembles that of the T odd phages (Myoviridae).Shutt TE, Gray MW (2005) Bacteriophage origins of mitochondrial replication and transcription proteins. Trends Genet 22(2):90-95Only a single gene encoding the putative ATPase subunit of the terminase is conserved among all herpesviruses. To a lesser extent this gene is also found also in T4-like bacteriophages suggesting a common ancestor for these two groups of viruses.Davison AJ (2002) Evolution of the herpesviruses. Vet Microbiol 22;86(1-2):69-88A common origin for the Herpesviruses and the Caudoviruses has been suggested on the basis of parallels in their capsid assembly pathways and similarities between their portal complexes, through which DNA enters the capsid. Baker ML, Jiang W, Rixon FJ, Chiu W (2005) Common ancestry of herpesviruses and tailed DNA bacteriophages. J. Virol. 79:14967-14970 These two groups of viruses share a distinctive 12-fold arrangement of subunits in the portal complex.Baculoviruses evolved from with the Nudiviruses {{ma}}.Thézé J, Bézier A, Periquet G, Drezen JM, Herniou EA (2011) Paleozoic origin of insect large dsDNA viruses. Proc Natl Acad Sci USA 108(38):15931-15935Circoviruses may have evolved from a nanovirus.Gibbs MJ, Weiller GF (1999) Evidence that a plant virus switched hosts to infect a vertebrate and then recombined with a vertebrate-infecting virus. Proc Natl Acad Sci USA 96(14):8022-8027Given the similarities between the rep proteins of the alphasatellites and the nanoviruses, it is likely that the alphasatellites evolved from the nanoviruses.Xie Y, Wu P, Liu P, Gong H, Zhou X (2010) Characterization of alphasatellites associated with monopartite begomovirus/betasatellite complexes in Yunnan, China. Virol J 7:178 Further work in this area is needed to clarify this.
References
{{reflist}}
Additional reading


{{cite web}}
{{refend}}{{Baltimore classification}}
ar:فيروس دنا
ca:Virus ADN
cs:DNA viry
de:DNA-Virus
et:DNA-viirused
es:Virus ADN
fr:Virus à ADN
ko:DNA 바이러스
id:Virus DNA
it:Virus a DNA
nl:DNA-virus
ja:DNAウイルス
pl:Wirusy DNA
pt:Vírus DNA
ru:ДНК-содержащие вирусы
simple:DNA virus
sv:DNA-virus
zh:去氧核糖核酸病毒