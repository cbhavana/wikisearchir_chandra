title{{pp-semi-protected}}
{{US Constitution article series}}
The Nineteenth Amendment (Amendment XIX) to the United States Constitution prohibits any United States citizen to be denied the right to vote based on sex. It was ratified on August 18, 1920.The Constitution allows the states to determine the qualifications for voting, and until the 1910s most states disenfranchised women. The amendment was the culmination of the women's suffrage movement, which fought at both state and national levels to achieve the vote.Susan B. Anthony and Elizabeth Cady Stanton drafted the amendment and first introduced it in 1878; it was forty-one years later, in 1919, when the Congress submitted the amendment to the states for ratification. A year later, it was ratified by the requisite number of states, with Tennessee's ratification being the final vote needed to add the amendment to the Constitution.The Nineteenth Amendment was unsuccessfully challenged in Leser v. Garnett (1922). In that case, the Supreme Court rejected claims that the amendment was unconstitutionally adopted.
Text{{quote}}
BackgroundThe United States Constitution, adopted in 1789, left the boundaries of suffrage undefined. This authority was implicitly delegated to the individual states, all of which denied the right to women (with the exception of New Jersey, which initially carried women's suffrage but revoked it in 1807).While scattered movements and organizations dedicated to women's rights existed previously, the 1848 Seneca Falls Convention in New York is traditionally held as the start of the American women's rights movement. Suffrage was not a focus of the convention, however, and its advancement was minimal in the decades preceding the Civil War. While suffrage bills were introduced into most state legislatures during this period, they were generally disregarded and few came to a vote.{{harvnb}}.The women's suffrage movement took hold after the war, during the Reconstruction era (1865–1877). During this period, women's rights leaders advocated for inclusion of universal suffrage as a civil right in the Reconstruction amendments (the Thirteenth, Fourteenth, and Fifteenth Amendments). Despite their efforts, these amendments did nothing to promote women's suffrage.{{harvnb}}.{{harvnb}}.
Continued settlement of the western frontier, along with the establishment of territorial constitutions, allowed the issue to be raised continually at the state level. Through the activism of suffrage organizations and independent political parties, women's suffrage was established in the newly formed constitutions of Wyoming Territory (1869), Utah (1870), and Washington Territory (1883). Existing state legislatures began to consider suffrage bills, and several even held voter referenda, but they were unsuccessful. Efforts at the national level persisted through a strategy of congressional testimony, petitioning, and lobbying.{{harvnb}}.Two rival organizations, the National Woman Suffrage Association (NWSA) and the American Woman Suffrage Association (AWSA), were formed in 1869. The NWSA, led by suffrage leaders Elizabeth Cady Stanton and Susan B. Anthony, attempted several unsuccessful court challenges in the mid-1870s.{{harvnb}}. Their legal case, known as the New Departure strategy, was that the Fourteenth Amendment (granting universal citizenship) and Fifteenth Amendment (granting the vote irrespective of race) together served to guarantee voting rights to women. Three Supreme Court decisions from 1873 to 1875 rejected this argument, so these groups shifted to advocating for a new constitutional amendment.{{harvnb}}.
Proposal and ratification
The Nineteenth Amendment's text was drafted by Susan B. Anthony with the assistance of Elizabeth Cady Stanton. The proposed amendment was first introduced in the U.S. Senate colloquially as the "Anthony Amendment", by Senator Aaron A. Sargent of California. Sargent, who had met and befriended Anthony on a train ride in 1872, was a dedicated women's suffrage advocate. He had frequently attempted to insert women's suffrage provisions into unrelated bills, but did not formally introduce a constitutional amendment until January 1878.{{Harvnb}}. Stanton and other women testified before the Senate in support of the amendment.{{Harvnb}}. The proposal sat in a committee until it was considered by the full Senate and rejected in a 16 to 34 vote in 1887.{{cite web}}
A three-decade period known as "the doldrums" followed, during which the amendment was not considered by Congress and the women's suffrage movement achieved few victories.{{sfn}}{{sfn}} During this period, the suffragists pressed for the right to vote in the laws of individual states and territories while retaining the goal of federal recognition. A flurry of activity began in 1910 and 1911 with surprise successes in Washington and California.{{sfn}} Over the next few years, most western states passed legislation or voter referenda enacting full or partial suffrage for women.{{sfn}} These successes were linked to the 1912 election, which saw the rise of the Progressive and Socialist parties, as well as the election of Democratic President Woodrow Wilson.{{sfn}}{{sfn}} Not until 1914 was the constitutional amendment again considered by the Senate, where it was again rejected.
On January 12, 1915, a proposal to amend the Constitution to provide for women's suffrage was brought before the House of Representatives, but was defeated by a vote of 204 to 174. Another proposal was brought before the House on January 10, 1918. During the previous evening, President Wilson made a strong and widely published appeal to the House to pass the amendment. It was passed by the required two-thirds of the House, with only one vote to spare. The vote was then carried into the Senate. Wilson again made an appeal, but on September 30, 1918, the proposal fell two votes short of passage. On February 10, 1919, it was again voted upon and failed by only one vote.There was considerable desire among politicians of both parties to have the proposal made part of the Constitution before the 1920 general elections, so the President called a special session of the Congress so the proposal would be brought before the House again. On May 21, 1919, it passed the House, 42 votes more than necessary being obtained. On June 4, 1919, it was brought before the Senate and, after a long discussion, it was passed with 56 ayes and 25 nays. Within a few days, Illinois, Wisconsin, and Michigan ratified the amendment, their legislatures being in session. Other states followed suit at a regular pace, until the amendment had been ratified by 35 of the necessary 36 state legislatures. On August 18, 1920, Tennessee narrowly approved the Nineteenth Amendment, with 50 of 99 members of the Tennessee House of Representatives voting yes.{{Harvnb}}. This provided the final ratification necessary to enact the amendment.{{Harvnb}}.
Ratification timeline
The Congress proposed the Nineteenth Amendment on June 4, 1919, and the following states ratified the amendment.{{cite web}}{{cite news}}
{{div col}}

Wisconsin (June 10, 1919)
Illinois (June 10, 1919, reaffirmed on June 17, 1919)
Michigan (June 10, 1919)
Kansas (June 16, 1919)
New York (June 16, 1919)
Ohio (June 16, 1919)
Pennsylvania (June 24, 1919)
Massachusetts (June 25, 1919)
Texas (June 28, 1919)
Iowa (July 2, 1919)date on which approved by governor
Missouri (July 3, 1919)
Arkansas (July 28, 1919)
Montana (August 2, 1919)
Nebraska (August 2, 1919)
Minnesota (September 8, 1919)
New Hampshire (September 10, 1919)
Utah (October 2, 1919)
California (November 1, 1919)
Maine (November 5, 1919)
North Dakota (December 1, 1919)
South Dakota (December 4, 1919)
Colorado (December 15, 1919)
Kentucky (January 6, 1920)
Rhode Island (January 6, 1920)
Oregon (January 13, 1920)
Indiana (January 16, 1920)
Wyoming (January 27, 1920)
Nevada (February 7, 1920)
New Jersey (February 9, 1920)
Idaho (February 11, 1920)
Arizona (February 12, 1920)
New Mexico (February 21, 1920)
Oklahoma (February 28, 1920)
West Virginia (March 10, 1920, confirmed on September 21, 1920)
Washington (March 22, 1920)
Tennessee (August 18, 1920)
{{div col end}}Ratification was completed on August 18, 1920, and the following states subsequently ratified the amendment:
{{div col}}

Connecticut (September 14, 1920, reaffirmed on September 21, 1920)
Vermont (February 8, 1921)
Delaware (March 6, 1923, after being rejected on June 2, 1920)
Maryland (March 29, 1941 after being rejected on February 24, 1920; not certified until February 25, 1958)
Virginia (February 21, 1952, after being rejected on February 12, 1920)
Alabama (September 8, 1953, after being rejected on September 22, 1919)
Florida (May 13, 1969){{cite web}}
South Carolina (July 1, 1969, after being rejected on January 28, 1920; not certified until August 22, 1973)
Georgia (February 20, 1970, after being rejected on July 24, 1919)
Louisiana (June 11, 1970, after being rejected on July 1, 1920)
North Carolina (May 6, 1971)
Mississippi (March 22, 1984, after being rejected on March 29, 1920)
{{div col end}}Alaska and Hawaii were not states when the Nineteenth Amendment was ratified.
Leser v. GarnettThe amendment's validity was unanimously upheld in Leser v. Garnett, {{ussc}}.{{Harvnb}}.Oscar Leser sued to stop two women registered to vote in Baltimore, Maryland, because he believed that the Maryland Constitution limited the suffrage to men and the Maryland legislature had refused to vote to ratify the Nineteenth Amendment. Two months before, the federal government had proclaimed the amendment incorporated into the Constitution on August 26, 1920. First, Leser said the amendment "destroyed State autonomy" because it increased Maryland's electorate without the state's consent. The Court answered that the Nineteenth Amendment was worded like the Fifteenth Amendment, which had expanded state electorates without regard to race for over 50 years by that time despite being rejected by six states, including Maryland.Second, Leser claimed that the state constitutions in some ratifying states did not allow their legislatures to ratify. The Court replied that state ratification was a federal function which came from Article V of the Constitution and so is not subject to limitations by a state constitution.Third, those bringing suit asserted the Nineteenth Amendment was not adopted, because Tennessee and West Virginia violated their own rules of procedure. The Court ruled that the point was moot, because since then Connecticut and Vermont had ratified the amendment and so there was a sufficient number of ratifications for the Nineteenth Amendment to be considered adopted even without Tennessee and West Virginia. Also, the Court ruled that Tennessee and West Virginia's certifying of their ratifications was binding on the federal courts.Thus, the two women were permitted to be registered to vote in Baltimore.
EffectsFollowing the Nineteenth Amendment's adoption, many legislators feared that a powerful women's bloc would emerge in American politics. This led to the passage of such laws as the Sheppard–Towner Act of 1921, which expanded maternity care during the 1920s.{{Harvnb}}. However, a women's bloc did not emerge in American politics until the 1950s.{{Harvnb}}.
See also{{Portal}}

Women's suffrage in the United States
History of feminism
Women's rights


Notes{{Reflist}}
References
Footnotes{{Reflist}}
Bibliography{{refbegin}}

{{Cite book}}
{{Cite book}}
{{Cite book}}
{{Cite book}}
{{Cite book}}
{{Cite book}}
{{Cite encyclopedia}}
{{Cite book}}
{{Cite book}}
{{Cite encyclopedia}}
{{Cite book}}
{{refend}}
External links

National Archives: Nineteenth Amendment
Amendments to the Constitution, with ratification information
CRS Annotated Constitution: Nineteenth Amendment

{{US Constitution}}{{DEFAULTSORT:19}}





cs:Devatenáctý dodatek Ústavy Spojených států amerických
de:19. Zusatzartikel zur Verfassung der Vereinigten Staaten
es:Decimonovena Enmienda a la Constitución de los Estados Unidos
fa:اصلاحیه نوزدهم قانون اساسی ایالات متحده آمریکا
fr:XIXe amendement de la Constitution des États-Unis
he:התיקון ה-19 לחוקת ארצות הברית
mt:Id-Dsatax-il Emenda għall-Kostituzzjoni tal-Istati Uniti
nl:Negentiende amendement van de grondwet van de Verenigde Staten
ja:アメリカ合衆国憲法修正第19条
pl:19. poprawka do Konstytucji Stanów Zjednoczonych
ru:Девятнадцатая поправка к Конституции США
sq:Amendamenti i 19 i Kushtetutës amerikane
zh:美國憲法第十九修正案