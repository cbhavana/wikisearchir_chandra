titlePrecision viticulture is precision farming applied to optimize vineyard performance, in particular maximizing grape yield and quality while minimizing environmental impacts and risk Proffitt, T., R. Bramley, D. Lamb, and E. Winter.  2006.  Precision Viticulture: A New Era in Vineyard Management and Wine Production.   WineTitles, Adelaide.  ISBN 9780975685044.  This is accomplished by measuring local variation in factors that influence grape yield and quality (soil, topography, microclimate, vine health, etc.) and applying appropriate viticulture management practices (trellis design, pruning, fertilizer application, irrigation, timing of harvest, etc.)Bramley R.G.V., Hamilton R.P. 2004. Understanding variability in winegrape production systems. 1. Within vineyard variation in yield over several vintages. Australian Journal of Grape and Wine Research 10: 32-45.Bramley R.G.V. 2005. Understanding variability in winegrape production systems. 2. Within vineyard variation in quality over several vintages. Australian Journal of Grape and Wine Research 11: 33-42..  Precision viticulture is based on the premise that high in-field variability for factors that affect vine growth and grape ripening warrants intensive management customized according to local conditions.  Precision viticulture depends on new and emerging technologies such as global positioning systems (GPS), meteorologic and other environmental sensors, satellite and airborne remote sensing, and geographic information systems (GIS) to assess and respond to variability.
Background
Precision viticulture is unique in its emphasis on vineyard management according to local variation, and in its use of science and technology to accomplish this.  While Australian viticulturalists are generally recognized as leaders in precision viticulture, and while viticulturalists worldwide have embraced the approach, the fundamental concepts have deep roots in the traditions of Old World winemaking regions.  Terroir, a related French concept, refers to the special geographic qualities or "sense of place" embodied in the wine produced in a particular region Robinson, J. (ed).  2006.  The Oxford Companion to Wine, Third Edition.  Oxford University Press.  ISBN 0198609906.Precision agriculture emphasizes "doing the right thing, in the right place, at the right time", and is practical for viticulture because of high local variability of conditions within vineyards, and because of responsiveness to intensive management in terms of increased grape yield and quality.  According to CSIRO, Australia CSIRO, 2008. Australia Precision Viticulture Overview,  http://www.csiro.au/science/Precision-Viticulture.html, accessed December 15, 2008 "Typically grape yield varies eight to ten-fold under uniform management";  "patterns of yield variation are stable in time and driven by soil and topographic variation";  and "patterns of variation in fruit quality tend to be similar to those for yield, suggesting opportunities for zonal management and selective harvest".  Australian precision viticulture has focused on yield monitoring, whereas California precision viticulture has focused on remote sensing Goode, J.  2005.  The Science of Wine: from Vine to Glass.  University of California Press, Berkeley. ISBN 0520248007, ISBN 9780520248007.
Technology for Characterizing Vineyard Variation
Precision viticulture uses a broad set of enabling technologies to observe and respond to vineyard variability:  

Global Positioning Systems (GPS) provide satellite-based georeferencing for mapping vineyard environmental variability.



Meteorologic Stations monitor climatic factors important for vine growth and grape ripening, including temperature, precipitation, humidity, and wind.



Remote Sensing from satellite and airborne platforms provides images depicting vineyard conditions, for example vine productivity from normalized difference vegetation index (NDVI).



Digital Elevation Models (DEM) provide detailed topographic information.



High Resolution Soil Surveys provide detailed information about soil fertility and hydrologic characteristics.



Relational Databases organize environmental and economic information.



Geographic Information Systems (GIS) provide digital tools for map-based analysis.



Other Environmental Sensors monitor important biophysical factors such as solar radiation, soil moisture, and temperature regimes.


Precision Viticulture Management Practices
Precision viticulture draws upon a variety of management approaches, including zonal management, in which different areas of the vineyard are managed according to their unique conditions, and adaptive management, in which different management practices are applied according to observed needs and improved knowledge.  Trellis design, in terms of row orientation and geometry of vine support, and pruning practices can be tailored to optimize vine health, to protect grapes from frost, sunburn, and mildew damage, and to ensure even grape ripeningWeiss, S.B., D.C. Luth, and B. Guerra. 2003.  Potential solar radiation in a VSP trellis at 38°N latitude.  Practical Winery and Vineyard 25:16-27..  Irrigation and fertilizer application schedules, pest management, and selective harvest based on timing of ripening can all be managed to minimize costs and maximize vineyard performance based on observed needs.  Increasingly, precision viticulture, with its focus on management according to local variability, is coupled with organic farming, with its focus on environmentally friendly practices without the use of chemical pesticides and fertilizers, and with sustainable agriculture, with emphasis on long-term environmental stewardship and economic viability.
Future
Various integrative technological approaches are gaining increasing attention for application in precision viticulture:

Distributed Sensor Networks use strategic deployment of sensors throughout a vineyard to monitor key factors such as water stress and temperature.



Vineyard Models simulate microclimate, vine growth, grape ripening, and economic return on investment to evaluate management options.



Decision Support Systems (DSS) bring together vineyard environmental and economic databases, vineyard models, and GIS in an interactive software-based system to solve management problems and better make decisions.


See also


Precision agriculture
Terroir
Vineyard
Viticulture
Wine
Winemaking


References{{Reflist}}
External links

CSIRO Australia Precision Viticulture Overview
International Network on Precision Viticulture
Precision Viticulture in Australia
Precision Viticulture International
vines.org Wine Encyclopedia - Precision Viticulture


Further reading
Gladstones, J.  1992.  Viticulture and Environment.  WineTitles, Adelaide.  ISBN 1875130128Goode, J.  2005.  The Science of Wine: from Vine to Glass.  University of California Press, Berkeley. ISBN 0520248007, ISBN 9780520248007Proffitt, T., R. Bramley, D. Lamb, and E. Winter.  2006.  Precision Viticulture: A New Era in Vineyard Management and Wine Production.   WineTitles, Adelaide.  ISBN 9780975685044Sommers, B.J.  2008.  The Geography of Wine: How Landscapes, Cultures, Terroir, and the Weather Make a Good Drop.  Plume Press/Penguin Prentice-Hall Press.  ISBN 0452288908Swinchatt, J., and D.G. Howell.  2004.  The Winemaker's Dance: Exploring Terroir in the Napa Valley.  University of California Press, Berkeley.  ISBN 0520235134{{Viticulture}}
{{Winemaking}}
