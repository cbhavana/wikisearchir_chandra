title{{Infobox scientist}}Eva Jablonka ({{lang-he}}) is a theorist and geneticist, known especially for her interest in epigenetic inheritance. Born in 1952 in Poland, she emigrated to Israel in 1957.  She is a professor at the Cohn Institute for the History of Philosophy of Science and Ideas at Tel Aviv University. In 1981 she was awarded the Landau Prize of Israel for outstanding Master of Science (M.Sc) work and in 1988, the Marcus prize for outstanding Ph.D work.{{cite web}} She is a proponent of academic freedom, recognising that on such matters, "academic and political issues cannot really be kept apart", although she is not a proponent of simplistic solutions, and shows a preference to describe her own position.{{cite web}}
Work on evolutionary themesJablonka publishes about evolutionary themes, especially epigenetics. Her emphasis on non-genetic forms of evolution has received interest from those attempting to expand the scope of evolutionist thinking into other spheres. An example of her work in this area is the book Animal Traditions (2000), co-authored with Eytan Avital, in which they extend models of human cultural transmission to the non-human animal world, to show that cultural evolution has played an important role in the evolution of other animals.{{cite book}} Also ISBN 978-1840467-80-2 Jablonka has been described as being in the vanguard of an ongoing revolution within evolutionary biology, and was a keynote speaker at the Artificial Life XI conference in 2008.{{cite book}}In 2005, Jablonka co-authored with Marion Lamb the book Evolution in Four Dimensions, which is considered by some to be in the vanguard of an ongoing revolution within evolutionary biology. Building on the approach of evolutionary developmental biology, and recent findings of molecular and behavioral biology, they argue the case for the transmission of not just genes per se, but heritable variations transmitted from generation to generation by whatever means. They suggest that such variation can occur at four levels. Firstly, at the established physical level of genetics. Secondly, at the epigenetic level involving variation in the “meaning” of given DNA strands, in which variations in DNA translation during developmental processes are subsequently transmitted during reproduction, which can then feed back into sequence modification of DNA itself. The third dimension is one of particular interest to Jablonka, comprising the transmission of behavioural traditions. There are for example documented cases of food preferences being passed on, by social learning, in several animal species, which remain stable from generation to generation while conditions permit. The fourth dimension is symbolic inheritance, which is unique to humans, and in which traditions are passed on “through our capacity for language, and culture, our representations of how to behave, communicated by speech and writing.” {{cite journal}} In their treatment of the higher levels, Jablonka and Lamb distinguish their approach from the banalities of evolutionary psychology, of "memes", and even from Chomskyian ideas of universal grammar. The argue that there are constant interactions between the levels - epigenetic, behavioural and even symbolic inheritance mechanisms also produce selection pressures on DNA-based inheritance and can, in some cases, even help direct DNA changes themselves - so "evolving evolution". To liven their text, they utilise thought experiments and dialogue with a sceptical enquirer, one IM-Ifcha Mistraba, Aramaic, they say, for "the opposite conjecture".
PublicationsIn English:

{{cite doi}}
{{cite doi}}
Eva Jablonka and Marion J. Lamb (1995). Epigenetic Inheritance and Evolution: the Lamarckian Dimension, Oxford University Press. ISBN 0198540639,  ISBN 9780198540632,  ISBN 978-0198540632
Eytan Avital and Eva Jablonka. (2000) Animal Traditions: Behavioural Inheritance in Evolution. Cambridge University Press. ISBN 0521662737
Eva Jablonka and Marion J. Lamb (2005) Evolution in Four Dimensions: Genetic, Epigenetic, Behavioral, and Symbolic Variation in the History of Life. MIT Press. ISBN 0262101076
{{cite doi}}
{{cite journal}}
{{cite doi}} (First published 1990).

In Hebrew:

Eva Jablonka (1994) History of Heredity. Ministry of Defence Publishing House, Israel.
Eva Jablonka (1994–1997) Evolution: A Textbook in Evolutionary Biology for the Open University, Israel. Open University Press. 7 units. 700 pages.


References{{reflist}}
External links

Evan Jablonka CV
Epigenetic Inheritance and Evolution. Partial online text.
Evolution in Four Dimensions. Partial online text.

{{Persondata}}
{{DEFAULTSORT:Jablonka, Eva}}



he:חוה יבלונקה