title{{Infobox person}}
Laurene Powell Jobs (born 1963) is co-founder and President of the Board of College Track and widow of Steve Jobs, co-founder and former CEO of Apple Inc.
BiographyPowell Jobs received a BA degree from the University of Pennsylvania and a BSc degree in Economics (BSE) from the Wharton School of the University of Pennsylvania in 1985.{{cite web}} She received her MBA degree from the Stanford Graduate School of Business in 1991.{{cite web}}She married Steve Jobs on March 18, 1991, in a ceremony at the Ahwahnee Hotel in Yosemite National Park. Presiding over the wedding was Kobun Chino Otogawa, a Zen Buddhist monk. Their son, Reed, was born September 1991, followed by daughters Erin in 1995 and Eve in 1998.{{cite book}}  The family lives in Palo Alto, California.{{cite web}}Powell Jobs co-founded Terravera, a natural foods company that sells to retailers throughout Northern California. She also served on the board of directors of Achieva, which made online tools to help students study and to be more effective at taking tests. Before business school, Jobs worked for Merrill Lynch Asset Management and spent three years at Goldman Sachs as a fixed-income trading strategist.
PhilanthropyPowell Jobs turned her attention to non-profit entrepreneurship, focusing on education, women’s human rights, and the arts.{{cite news}}  As of 2011, she sits on the boards of directors of Teach for America; Global Fund for Women; KQED (PBS); EdVoice; New America Foundation; Stanford Schools Corporation; New Schools Venture Fund. She also sits on the advisory board of Stanford Graduate School of Business.  In 1997, Powell Jobs and Carlos Watson co-founded College Track, a nonprofit organization in East Palo Alto with a mission of improving high school graduation, college enrollment, and college graduation rates for "under-resourced" communities--students of color and from low-income families.{{cite web}} "Everybody graduates from high school and continues with their education," Powell Jobs has said in interviews.{{cite news}} Of College Track's high school graduates, 90 percent have gone to four-year colleges and 70 percent have finished college within six years, which is 46 percentage points higher than the national average for first-generation students. College Track has grown slowly. It currently operates in East Palo Alto, San Francisco, Oakland, and New Orleans. "We have a wait list of five cities where we’d like to open up centers," Powell Jobs has said. "We want to keep our standards high, though, and are reluctant to grow through franchising or through dissemination of our curriculum and training."Powell Jobs was appointed by President Barack Obama in December 2010 to be a Member of the White House Council for Community Solutions, which is charged with advising the President on the best ways to help communities across America solve important social challenges around education and job creation. The White House Council is Chaired by former Gates Foundation CEO Patty Stonesifer and also includes singer Jon Bon Jovi, eBay CEO John Donahoe and President of the Rockefeller Foundation Judith Rodin.  {{cite web}}
References{{Reflist}}{{Persondata}}
{{DEFAULTSORT:Powell Jobs, Laurene}}








fr:Laurene Powell Jobs
id:Laurene Powell Jobs
ta:லோரீன் பவல்
zh:勞倫·鮑威爾