title{{Primary sources}}
{{Infobox animanga/Header}}
{{Infobox animanga/Video}}
{{Infobox animanga/Footer}}{{nihongo}} is an anime and manga series written and illustrated by Osamu Tezuka. Originally planned by Tezuka as a color remake of the original anime adaptation of his popular manga series Tetsuwan Atom, unfavorable circumstances during the pre-production phase of the project led him to abandon it temporarily and create Jetter Mars instead. Although it remains as one of Tezuka's lesser known works, it gained a large following and is fondly remembered.
Development
Unlike the vast majority of Osamu Tezuka's characters, Jetter Mars wasn't first published in comic book form, written and illustrated by Tezuka. In 1977, Tezuka's animation studio Mushi Productions faced bankruptcy, leading Tezuka to work as an animation director for Toei Animation for some time. He wanted to produce a color anime adaptation for his landmark manga Tetsuwan Atom, (known to western audiences as Astroboy), but due to the financial distress of Mushi Productions, the copyrights on some of his characters were in limbo. Due to the deadline imposed on Tezuka by Toei to produce the new series, he wasn't able to secure the copyrights to his characters on time, which forced him to compromise his original project and redesign it somewhat, reworking it conceptually and visually to some extent.Tezuka then redesigned Atom slightly, and renamed him as Jetter Mars. Some aspects of Atom's original story are taken and used as the origin of Mars, such as him being created as a powerful android in the image of a boy. Two scientists are involved in Mars' creation, one conceiving him as a combat instrument and the other opposing that purpose, conceiving him as an instrument for peaceful pursuits. This element mirrors the disagreement found in the characters Dr. Tenma and Dr. Ochanomizu in Tetsuwan Atom. Some other borrowed concepts can be found across the series.Some storylines in the episodes of Jetter Mars were remakes of classic and beloved stories Tezuka wrote and drew in his Tetsuwan Atom manga, such as The Artificial Sun and The Last Day on Earth. The series was beautifully animated, and in the Tezuka tradition, it showcased many of his characters from his various manga works in different roles.In the same manner he wrote and illustrated his Tetsuwan Atom manga, Tezuka stayed away from graphic violence in Jetter Mars, although the typical traits of his works are also present in it, such as the importance of moral values, humanity and strong emotiveness.The series was received by Japan's population with mixed feelings, as some wanted the original Tetsuwan Atom and some accepted Jetter Mars wholeheartedly as a different character, as it was intended to be. This made Tezuka lose interest in the series and it finished with 27 episodes produced. Having successfully secured the copyrights on all his characters once again, Tezuka founded Tezuka Productions a few years later, and was able to produce the color anime adaptation of Tetsuwan Atom as he wanted initially, which became a hit, much in the same way as the original anime adaptation from the 60's.{{cite web}}
Plot
The storyline is set in the year 2015. Two scientist, Dr. Kawashimo and Dr. Yamanoue, have created a robot with advanced capabilities. Dr. Kawashimo created his miraculous artificial intelligence, making him almost human, while Dr. Yamanoue created the robot's body, endowing him with astoundingly powerful weaponry.
CharactersAs expected from Osamu Tezuka, he put his "character acting company", known as Star System, to use to define the cast of Jetter Mars. He created a few of the characters specifically for the series, such as Mars and Melchi, and the vast majority of the cast was classic and well-known characters from Tezuka's works, playing various roles. In the picture to the right, it is possible to identify many of Tezuka's characters, from left to right: Daidalos, Shunsuke Ban aka Higeoyaji and Tezuka himself in the upper row; Inspector Tawashi, Rock Holmes and Marukubi Boon in the middle row; and Tamao, Shibugaki, Spider, Chief Nakamura, HamEgg, Acetylene Lamp, Ken'ichi and Hyōtan-tsugi in the lower row; among some others.The following list describes the characters featured more prominently during the series:{{nihongo}}: The protagonist of the series. A powerful robot built in the image of a boy, he has a body that can be used for destruction of cataclysmic proportions, and a near-human artificial intelligence. He finds himself often in the predicament of choosing to use his gifts for pacific or destructive purposes.{{nihongo}}: Mars' non-speaking younger brother, with the body of a baby. Endowed with tremendous physical strength, his only word is {{nihongo}} (a word invented by Tezuka with no real meaning), which he always utters after displaying his power.{{nihongo}}: A robot made in the image of a young girl by Dr. Kawashimo. She possessed powers that enabled her to restore destroyed robots and machinery. Protector of Mars and Melchi, and "daughter" to Dr. Kawashimo.{{nihongo}}: Creator of Jetter Mars' incredible artificial intelligence and heart, and creator of Miri and Melchi. Opposing fellow scientist Dr. Yamanoue, he desired Mars to live a life of good purpose and peace.{{nihongo}}: Creator of Mars' body. He designed him as a machine for war, and thus named his creation after the roman god of war. Mars looked up to him as his father. He disappears after being buried during atomic tests and is left for dead.{{nihongo}}: In many of his works, Tezuka drew himself as a character, immersed in the universe of his creations, and interacted with his characters. He included himself also in Jetter Mars, appearing as friend and advisor to Mars.{{nihongo}} and {{nihongo}}: Two cartoony little characters, that appeared whimsically in nonsensical situations, as comic relief. Tezuka's signature, they appeared in all of his works, be it manga or animated, as they were a form of dialogue between Tezuka and his readers, developed during the years. Hyōtan-tsugi usually appeared falling in front of a character at the most inappropriate of times, and getting kicked out angrily by them, and Spider usually appeared in moments of tension, repeatedly uttering his trademark phrase, {{nihongo}}, roughly "Here ta meet ya!". Additionally, Hyōtan-tsugi appeared in each episode during the opening intro and end credits sequences.{{nihongo}}: One of Tezuka's most recognizable characters in his Star System, he appeared playing non-praiseworthy roles, as usual for him.
Episode list
The series had 27 episodes, each lasting 24 minutes approximately. Broadcasted by Fuji TV, on Thursday nights. As with all Tezuka's works, the series made use of his Star System cast technique, featuring many of his characters from previous works (manga or animated) as guests performing various roles. The following list includes the episode title, airing date and guest characters.{{cite web}}
{{cite web}}

Episode # Title Guest character Aired 
1 Mars is born, year 2015{{nihongo}} Faitan February 3, 1977 
2 Counterfeiting robot gang{{nihongo}} none February 10, 1977 
3 Why is Mars crying?{{nihongo}} Mad Mask February 17, 1977 
4 Goodbye, little brother!{{nihongo}} Tack February 24, 1977 
5 Talent, the greatest robot in history{{nihongo}} HamEggs March 3, 1977 
6 The girl that came from the star of dreams{{nihongo}} Dori March 10, 1977 
7 Missing Miri{{nihongo}} Skunk Kusanagi March 17, 1977 
8 Where did dad go?{{nihongo}} No.1, No.2 March 24, 1977 
9 Lamp, the space trader{{nihongo}} Acetylene Lamp March 31, 1977 
10 My lil' brother's name is Melchi{{nihongo}} Dr.Spice, Mrs. Tabasko April 7, 1977 
11 Freshman Mars{{nihongo}} Mason April 14, 1977 
12 Jam Bond, Secret agent{{nihongo}} Jam Bond April 21, 1977 
13 Honey, robot exchange-student{{nihongo}} Honey April 28, 1977 
14 The vampire from outer space{{nihongo}} Kuroro May 5, 1977 
15 Melchi likes Mouster{{nihongo}} Mouster May 12, 1977 
16 Zaza, the wandering planet{{nihongo}} Zazians May 19, 1977 
17 The samurai robot from the seventh year of the Tenpō era{{nihongo}} Yumi, Samurai robot June 2, 1977 
18 Resurrect, ancient robot{{nihongo}} Daidalos June 16, 1977 
19 Mars' first love{{nihongo}} Agunea, Marukubi Boon June 23, 1977 
20 Mars becomes a young boss{{nihongo}} Black, Dr. Jin June 30, 1977 
21 Mighty Robot Joe{{nihongo}} Joe Asnaro July 7, 1977 
22 Android Lullaby{{nihongo}} Mayumi July 21, 1977 
23 The wandering robot{{nihongo}} Adios July 28, 1977 
24 Miri, alone again{{nihongo}} Saromi August 18, 1977 
25 The wolf-boy from space{{nihongo}} none September 1, 1977 
26 Adios returns{{nihongo}} Dr. Kuromaru September 8 , 1977 
27 The flight beyond tomorrow!{{nihongo}} Sari September 15, 1977 

Manga Adaptation
The anime series also was adapted into a few manga issues published as one-shot stories, based on the storylines shown in the anime. They weren't written or illustrated by Osamu Tezuka, but by various licensed artists. The issues are listed as follows:

Televiland Comics Issue
One-shot story illustrated by Shigeto Ikehara, published in the first issue of the magazine in 1979.



Bōken-ō Manga Magazine Issue
Illustrated by Rentarō Iwata for the defunct Bōken-ō manga magazine.



Televi-Magazine Issue
Illustrated by Kai Nanase and published in 1979.



Chū&#39;ichi Jidai Magazine Story
Illustrated by Saisuke Hiraga, published from April to August, 1979. Short-story divided in five chapters.


Dvd Release
Jetter Mars was released by Avex on March 27, 2009, as a five-disc Dvd Box set which contains the complete series.{{cite web}}
Merchandise
Several records containing the musical score of Jetter Mars were released during the series run. These are listed as follows{{cite web}}1. {{nihongo}}

Jetter Mars only LP record, it contained several songs along a mini-drama. The songs included in the record:
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}

2. Single Releases

Several records containing single songs were released. These are listed as follows:



Mars Single Record 1, included the songs:

{{nihongo}}
{{nihongo}}



Mars Single Record 2, included the songs:

{{nihongo}}
{{nihongo}}



Mars Single Record 3, a re-release of the first single record, it also included a track containing the sound effects of the anime:

{{nihongo}}
{{nihongo}}



Mars Single Record 4, with the same content of single records 1 and 3, except it didn&#39;t include the sound effects track. It also contained a Drama track, with the voices and sound effects of the first episode of the anime:

{{nihongo}}
{{nihongo}}
Drama track: {{nihongo}}



Mars Single Record 5, its contents were practically the same as Single Record 4, only with some editing variations.

{{nihongo}}
{{nihongo}}
Drama track: {{nihongo}}



Mars Single Record 6, included the opening and ending songs of the Jetter Mars anime, along the theme song for the anime adaptation of Yumiko Igarashi and Kyoko Mizuki&#39;s classic shōjo manga Candy Candy:




{{nihongo}}
{{nihongo}}
{{nihongo}}
{{nihongo}}


Staff
Involved in the production of the series:{{cite web}}Original concept, creator: Osamu Tezuka
Planning: Kōji Bessho (Fuji TV), Takeshi Tamiya
In charge of production: Kiyoshi Ono (first season), Kichirō Sugahara (final season)
Series composer: Masao Maruyama
Script: Masaki Tsuji, Shunichi Yukimuro, Yoshitaka Suzuki, Masaru Yamamoto, Hiroyuki Hoshiyama
Chief Director: Rintarō
Producers: Sumiko Chiba, Noboru Ishiguro, Wataru Mizusawa, Masami Hatano, Katsutoshi Sasaki, Yugo Serizawa, Jihiro Taizumi, Rintarō
Character Design Supervisor: Akio Sugino
Animation Supervisors: Akio Sugino, Toshio Mori, Satoshi Jingu, Wataru Mibu, Tsuneo Kashima, Toyō Ashida
Graphic Designer: Liang Wei Huang
Art: Shohei Kawamoto, Liang Wei Huang, Tadao Kubota
Photography: Masaaki Sugaya
Editing: Masaaki Hanai
Audio Director: Ryōsuke Koide (Arts Pro)
Recording: Hideyuki Tanaka
In collaboration with production: Mad House
Production: Fuji TV, Toei Animation
Music: Nobuyoshi Koshibe
Read also

List of Osamu Tezuka manga
List of Osamu Tezuka anime
Osamu Tezuka's Star System
Tezuka Award
Tezuka Osamu Cultural Prize


References{{reflist}}{{Astro Boy}}









ar:كوكي العجيب
es:Jet Marte
it:Capitan Jet
ja:ジェッターマルス
pt:O Menino Biônico