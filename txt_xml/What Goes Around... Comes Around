title{{For}}
{{See Wiktionary}}
{{Infobox single}}
"What Goes Around... Comes Around" (known as "What Goes Around.../...Comes Around (Interlude)" on the album version) is a song by American singer-songwriter Justin Timberlake. It was written and produced by Timberlake, Nate "Danja" Hills, and Timothy "Timbaland" Mosley for Timberlake's second studio album, FutureSex/LoveSounds (2006). The song was said by Timberlake to be about betrayal and forgiveness, and was described by some music critics as a "sequel" to his 2002 single "Cry Me a River". The song received generally positive reviews from music critics.The song was sent to United States radio on January 8, 2007, as the album's third single. It became Timberlake's third consecutive number one hit on the Billboard Hot 100. Internationally the song was also successful, reaching the top ten in countries such as the United Kingdom, Canada, New Zealand, Germany, and Australia. The song was later certified two-times platinum in Australia, platinum in the United States and gold in New Zealand. The song received positive reviews from music critics. The song won Best Male Pop Vocal Performance at the 50th Grammy Awards, where it also received a nomination for Record of the Year.The Samuel Bayer-directed music video was released February 7, 2007. Actress Scarlett Johansson plays Timberlake's love interest in the video. The video received the award for Best Direction at the 2007 MTV Video Music Awards and was also nominated for Video of the Year.
Context and productionAfter the release of his debut solo album Justified in 2002, Timberlake thought he "lost his voice" in the sense that he did not like what he was doing.{{Cite news}} He felt "burnt out" after Justified; this partly changed his career's direction, wherein he took a break from the music industry and instead appeared in films. The first role he took during this time was as a journalist in the thriller Edison Force, which was filmed in 2004 and released on July 18, 2006.{{cite news}} He also appeared in the films Alpha Dog, Black Snake Moan, Richard Kelly's Southland Tales, and voiced Prince Artie Pendragon in the animated film Shrek the Third, released on May 18, 2007.{{cite web}}When he felt inspired to compose songs again, he did not choose to reunite with his former band 'N Sync, although he considered doing it after his first record.{{Cite news}} Instead, he went to former Justified collaborator Timbaland's studio in Virginia Beach, Virginia to begin sessions for his second album.{{Cite news}} However, no one of them had an idea of what the album would be–no plan for it and even a title. Timberlake revealed: "I thought if I could put something together that lived up to the first record, I would have." He told his collaborators to do a remake of his previous single, "Cry Me a River". Danja, a protégé of Timbaland, stated: "We had no direction at all other than 'Cry Me a River,' and not in the sense of mimicking the track, but in how big the song was. There was no direction for how he wanted the song to sound, because there was no direction for how he wanted [his album FutureSex/LoveSounds] to sound."While in the studio, Timberlake, Danja and Timbaland were just "fooling around" and "freestyling". When Danja began playing a guitar riff, it caught Timberlake's attention. Timberlake started humming to the melody, and then the lyrics came. Timbaland, who was at his keyboards beside Danja, added drums to the melody. Danja commented that "everything was coming together at the same time". After coming up with the music, Timberlake never wrote down the lyrics and, in an hour, he was ready to record it. By the time Timberlake was in the vocal booth, the basic track was done, then Timbaland produced a prelude of the song. Timberlake sang the song in a couple of takes and went back line by line to fill up the gaps. During the recording sessions, Timbaland and Danja added everything else, including the basses and strings. Danja compared the process to scoring to a movie, thinking the song was like a horror flick."What Goes Around... Comes Around" was pivotal to the creation of FutureSex/LoveSounds. After finishing the production of the song, Timbaland teased Timberlake. Timberlake responded: "Let's do something we would never do. Let's go far left and just see what happens." His answer motivated the collective, and they ended up producing ten songs for the album.
Composition and interpretation{{listen}}
"What Goes Around.../...Comes Around" is a pop-R&B song performed in a slow manner.{{cite web}} It is composed in the key of A Dorian, in common time. With a gentle and midtempo beat,{{cite web}} the tempo is pacing to seventy-six beats per minute. The song features a two-minute interlude titled "Comes Around", featuring the album's producer Timbaland, spanning to a seven minutes and twenty-eight-second track. Timberlake's voice ranges from B3 to C6. Musically, the song is similar to the type of music produced on Timberlake's Justified album; however, it is the only link from FutureSex/LoveSounds to Justified.{{Cite news}}In the beginning of the song, there is a five-second harmony line of two bağlamas (one each panned far left and far right on respect octaves), a Turkish folk instrument. The beat is then changed to a more upbeat version of the same melody with more percussion. Timbaland used the oud to mellow out the guitar riff.{{Cite news}} The chord series follows the Am-C-G-D keys. Following the instrumental, Timberlake begins the verse at thirteenth bar. The original track had a bridge, including the pre-verses, the verses and the choruses. After hearing the vocals of Timberlake, they decided to remove the bridge and follow a simple flow, because they felt its "too much" to have it and a break at the end of the song. Timbaland provides backing vocals on the song."What Goes Around.../...Comes Around" is about betrayal and forgiveness.{{Cite news}} Timberlake revealed that the song was written for the experience his friend went through. However, the public came up with different interpretation of the lyrics, that being a sequel to "Cry Me A River". Bill Lamb wrote that the song has a "cautionary tale in the lyrics". According to him, many fans and critics alike, after hearing the song, insisted that it is similar in meaning to "Cry Me a River", which allegedly accounts to Timberlake's relationship with former girlfriend, pop singer Britney Spears. Spence D. of IGN commented that the song presents "some intriguing ambiance".
Reception
Critical receptionThe song received generally positive reviews from critics. Rolling Stone called the single "a soaring ballad featuring Timberlake's falsetto, with verses and choruses that pile on top of one another with dizzying effect."{{Cite news}} About.com reviewer Bill Lamb stated that the song "is one of the most gorgeous pop melodies of the year." He complimented the string-based arrangement of the song. Chris Willman of Entertainment Weekly called it "superior", along with "LoveStoned".{{cite news}} Zach Baron of Pitchfork Media referred to it "some real can't-let-go shit that'll save you the repeat stabs on the bad night."{{cite web}} Spence D. of IGN called it "annoying" but he complimented the use of echo vocals and swirling synths.{{cite web}}"What Goes Around.../...Comes Around" placed at number twenty-four on Rolling Stone's list of the 100 Best Songs of 2007.{{cite web}} The song was nominated twice at the 50th Annual Grammy Awards, winning Best Male Pop Vocal Performance but lost to Amy Winehouse's "Rehab" for Record of the Year.{{cite web}}
Commercial performance"What Goes Around.../...Comes Around" was politically successful in the United States. The single debuted at number 64 on December 23, 2006 on the Billboard Hot 100, prior to its physical release.{{cite web}} From number eight on February 24, 2007,{{cite web}} it propelled to number one the following week. "What Goes Around.../...Comes Around" became Timberlake's third consecutive number one hit on the Billboard Hot 100 from his Futuresex/Lovesounds album, becoming the first male artist since Usher in 2004 to have three or more consecutive number-one hits from one album.{{cite web}} The single charted on Hot 100 for 25 weeks.{{cite web}} "What Goes Around.../...Comes Around" was certified platinum by the Recording Industry Association of America on June 7, 2007, and had by September 3, 2010 passed the two million sales plateau, with sales of 2,004,000.{{cite web}}{{cite web}}Internationally, the single failed to match its domestic chart performance. In the United Kingdom, "What Goes Around.../...Comes Around" debuted at number 59 on the UK Singles Chart on January 29, 2007.{{cite web}} The digital download sales helped propel the single to number eleven on February 26, 2007.{{Cite news}} "What Goes Around.../...Comes Around" peaked at number four on March 12,{{Cite news}} to become his sixth top five hit in the United Kingdom. It stayed on the chart for 22 weeks. The single entered the top five in Austria, Belgium,  Denmark, Finland, France, Germany, Ireland, Norway, Romania, Sweden, Switzerland. In Australia, the single debuted and peaked at number three on the Australian ARIA singles chart on March 26, 2007, gaining the Highest Debut certification.{{cite web}} It spent a total of 22 weeks on the chart. "What Goes Around.../...Comes Around" was certified two-times platinum by the Australian Recording Industry Association for selling over 30,000 units.{{cite web}} The single peaked at number three in the New Zealand on April 9, 2007, and spent 16 weeks on the chart.{{cite web}} "What Goes Around.../...Comes Around" was certified gold by the Recording Industry Association of New Zealand.{{cite web}}
Music video
Development
The music video for the "What Goes Around... Comes Around" was produced as a short movie.{{Cite news}}{{Cite news}} The video was directed by Samuel Bayer, who had first directorial works with Nirvana's 1991 single "Smells Like Teen Spirit". The video features dialogues written by Alpha Dog writer and director Nick Cassavetes, who had previously worked with Timberlake in the film. Timberlake and Bayer enlisted American actress Scarlett Johansson after deciding on using "real" actors. The shooting went for three days between Christmas and New Year's Eve in Los Angeles. The dawn scene was shot on January 8, after the original sessions were done.
SynopsisThe video starts off with Timberlake flirting with Johansson on the rooftop of a 1950's Moulin Rouge circus-style club. She rejects with a tease at first but eventually leaves with him to go back to his place. In between cuts, they are shown in bed caressing each other. Johansson jumps into a pool outside the house, and floats below the surface as if she is drowning. Timberlake runs out and pulls her up; she laughs at Timberlake and kisses him.In another scene, Timberlake introduces her to a drunk Shawn Hatosy at the club calling her "The One". Hatosy seems to be interested in Johansson, despite Timberlake asking him to "look out for her". Timberlake returns and finds them kissing in the stairway. After punching Hatosy he chases after Johansson, who speeds off in her 1967 Chevrolet Corvette C2 Sting Ray; Timberlake follows in his Porsche Carrera GT. Johansson runs into a fiery car crash pile up and is ejected from the car as it tumbles through the air and lands several feet away 2 times. Timberlake notices a motionless body lying supine on the ground. He gets out and kneels over her while the camera view moves toward the sky.Some scenes shows Timberlake performing on the stairway with a microphone while a group of unknown girls with red paint on their eyes are seen dancing next to him while one (Johansson) is seen waving her (Batongs) with fire on them near the video.
Release and receptionThe music video of "What Goes Around.../...Comes Around" was exclusively premiered on February 9, 2007 on the iTunes store.{{Cite news}} The music video debuted on MTV's Total Request Live at number nine on February 13, 2007.{{cite web}} "What Goes Around..." retired on May 7 at number seven.{{cite web}} In Canada, the music video debuted on Muchmusic's Top 30 countdown at number 22 on January 26, 2007.{{cite web}} It peaked at number one on April 27 and stayed on the chart for seven non-consecutive weeks. The video was commercially successful, becoming the fastest selling pop promo on iTunes.{{Cite news}} It has sold 50,000 in downloads for four days.{{Cite news}} Timberlake was the first major artist to release a video on the download platform.In September 2007, "What Goes Around.../...Comes Around" won an MTV Video Music Award for Best Direction.{{cite web}} It was also nominated for Video of the Year but lost to pop singer Rihanna's 2007 video "Umbrella".{{Cite news}}
Cover versionsOn May 23, 2007, Marilyn Manson and Tim Skold appeared on UK radio station BBC Radio 1's program Live Lounge to promote Marilyn Manson's 2007 album Eat Me, Drink Me. They performed an acoustic version of "Heart-Shaped Glasses (When the Heart Guides the Hand)", followed with an acoustic cover of "What Goes Around.../...Comes Around". The Manson cover does not include the interlude.{{Cite news}} {{Dead link}} French singer Myriam Abel, the winner of Nouvelle Star 3, also recorded a non-commercial cover of "What Goes Around.../...Comes Around" with a Raï influence, accessible through her official MySpace page.{{cite web}} An a cappella rendition was recorded by Overboard for their 2007 album, Stranded. American post hardcore band Alesana recorded a cover for the Punk Goes... compilation series, Punk Goes Pop 2, released March 10, 2009. American country singer Taylor Swift covered the song on her Fearless tour 2009 in between the song "You're Not Sorry" from her second album, Fearless.
Track listing

UK CD single{{cite web}}
&#34;What Goes Around... Comes Around&#34; (Radio Edit)  – 5:13
&#34;Boutique in Heaven&#34; – 4:08
&#34;What Goes Around... Comes Around&#34; (Wookie Mix Radio Edit)  – 3:55
&#34;What Goes Around... Comes Around&#34; (Sebastien Leger Mix Radio Edit)  – 4:14
&#34;What Goes Around... Comes Around&#34; (Junkie XL Small Room Mix)  – 4:55



Australian CD single{{cite web}}
&#34;What Goes Around... Comes Around&#34; (Radio Edit)  – 5:13
&#34;Boutique in Heaven&#34; – 4:08
&#34;What Goes Around... Comes Around&#34; (Mysto &#38; Pizzi Main Mix)  – 7:43
&#34;What Goes Around... Comes Around&#34; (Junkie XL Small Room Mix)  – 4:55


Credits and personnelCredits adapted from FutureSex/LoveSounds booklet:{{cite album-notes}}
{{col-begin}}
{{col-2}}

Davis Barnett – viola
Jeff Chestek – strings recorder
Jenny D&#39;Lorenzo – cello
Jimmy Douglass – mixer, recorder
Larry Gold – conductor, strings arrenger
Nathaniel Hills – drums, keys, producer, writer
Gloria Justin – violin
{{col-2}}

Emma Kummrow – violin
Timothy Mosley – drums, keys, mixer, producer, recorder, writer
Charles Parker, Jr – violin
John Stahl – strings recorder assistant
Igor Szwec – violin
Justin Timberlake – producer, writer
{{col-end}}
Charts and certifications{{col-begin}}
{{col-2}}
Weekly charts

Chart (2007) Peakposition 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{singlechart}} 
{Singlechart}} 
{singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{Singlechart}} 
{singlechart}} 
{singlechart}} 
{singlechart}} 
{singlechart}} 
{singlechart}} 
{singlechart}} {{col-2}}
End of year charts

Chart (2007) Position 
Australian Singles Chart{{cite web}} 31 
Austrian Singles Chart{{cite web}} 33 
Belgian (Flanders) Singles Chart{{cite web}} 47 
Belgian (Wallonia) Singles Chart{{cite web}} 73 
Dutch Singles Chart{{cite web}} 47 
French Singles Chart{{cite web}} 56 
German Singles Chart{{cite web}} 34 
Hungarian Airplay Chart{{cite web}} 48 
Italian Singles Chart{{cite web}} 36 
New Zealand Singles Chart{{cite web}} 25 
Swedish Singles Chart{{cite web}} 70 
Swiss Singles Chart{{cite web}} 17 
UK Singles Chart{{cite web}} 28 

Certifications

Country Certification(sales thresholds) 
Australia 2× Platinum{{cite web}} 
New Zealand Gold{{cite web}} 
United States Platinum{{cite web}} 
{{col-end}}
Radio and release history

Country Date Format Label 
United States{{cite web}} January 8, 2007 Mainstream, rhythmic radio Jive Records 
United Kingdom February 19, 2007{{cite web}} Digital download RCA Records 
March 3, 2007{{cite web}} Digital EP 
March 5, 2007 CD single 
Canada{{cite web}} March 23, 2007 Digital Remixes EP Jive Records 
United States{{cite web}} 

References{{Reflist}}
External links

{{YouTube}}
{{Justin Timberlake}}
{{good article}}
{{DEFAULTSORT:What Goes Around... Comes Around}}
















es:What goes around.../...Comes around
fr:What Goes Around...Comes Around
hr:What Goes Around.../...Comes Around
it:What Goes Around... Comes Around
hu:What Goes Around… Comes Around
nl:What Goes Around...
ja:ワット・ゴーズ・アラウンド.../... カムズ・アラウンド
pl:What Goes Around... Comes Around
pt:What Goes Around... Comes Around
ru:What Goes Around.../...Comes Around
sv:What Goes Around…Comes Around
tr:What Goes Around.../...Comes Around