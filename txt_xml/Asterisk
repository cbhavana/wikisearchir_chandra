title{{about}}
{{redirect}}
{{Punctuation marks}}
An asterisk (*; Late {{lang-lat}}, from {{lang-el}}, asteriskos, "little star")ἀστερίσκος, Henry George Liddell, Robert Scott, A Greek-English Lexicon, on Perseus is a typographical symbol or glyph. It is so called because it resembles a conventional image of a star.  Computer scientists and mathematicians often pronounce it as star (as, for example, in the A* search algorithm or C*-algebra). In English, an asterisk is usually five-pointed in sans-serif typefaces, six-pointed in serif typefaces, and six- or eight-pointed when handwritten.The asterisk is derived from the need of the printers of family trees in feudal times for a symbol to indicate date of birth. The original shape was seven-armed,{{Citation needed}} each arm like a teardrop shooting from the center. Many cultures have their own unique versions of the asterisk.{{Citation needed}}In computer science, the asterisk is commonly used as a wildcard character, or to denote pointers, repetition, and multiplication.
Usage
Typography

The asterisk is used to call out a footnote, especially when there is only one on the page. Less commonly, multiple asterisks are used to denote different footnotes on a page. (i.e., *, **, ***) Typically, an asterisk is positioned after a word or phrase and preceding its accompanying footnote.
Three spaced asterisks centered on a page may represent a jump to a different scene or thought.
A group of three asterisks arranged in a triangular formation {{Unicode}} is called an asterism.
One or more asterisks may be used to strike out portions of a word to avoid offending by using the full form of a profanity (f**k), to preserve anonymity (Peter J***), or to avoid profanation of a holy name, especially in Jewish usage (G*d).
Asterisks are sometimes used as an alternative to typographical bullets to indicate items of a list.
Asterisks can be used in textual media to represent *emphasis* when bold text is not available (e.g. email).


Linguistics
Historical linguisticsIn historical linguistics, an asterisk immediately before a word indicates that the word is not directly attested, but has been reconstructed on the basis of other linguistic material (see also comparative method).In the following example, the Proto-Germanic word ainlif is a reconstructed form.

*ainlif → endleofan → eleven

A double asterisk indicates a form that would be expected according to rule, but is not actually found:

For the plural, **kubar would be expected, but separate masculine plural akābir أكابر and feminine plural kubrayāt كبريات are found as irregular forms.


Generativist tradition in linguisticsIn generativism, especially syntax, an asterisk  in front of a word or phrase indicates that the word or phrase is ungrammatical.

wake her up / *wake up her
An asterisk before a parenthesis indicates that the lack of the word or phrase inside is ungrammatical, while an asterisk after a parenthesis indicates that the existence of the word or phrase inside is ungrammatical.

go *(to) the station
go (*to) home


AmbiguitySince a word marked with an asterisk could mean either "unattested" or "impossible", it is important in some contexts to distinguish these meanings.  In general, authors retain asterisk for "unattested", and prefix ˣ, **, or a superscript "?" for the latter meaning.
Music

In musical notation the sign  indicates when the sustain pedal of the piano should be lifted.
In liturgical music, an asterisk is often used to denote a deliberate pause.


Computing
Computer science

In computer science, the asterisk is used in regular expressions to denote zero or more repetitions of a pattern; this use is also known as the Kleene star or Kleene closure after Stephen Kleene.
In the Unified Modeling Language, the asterisk is used to denote zero to many classes.


Computer interfaces

In some command line interfaces, such as the Unix shell and Microsoft&#39;s Command Prompt, the asterisk is the wildcard character and stands for any string of characters. This is also known as a wildcard symbol. A common use of the wildcard is in searching for files on a computer. For instance, if a user wished to find a document called Document&nbsp;1, search terms such as Doc* and D*ment* would return this file. Document* would also return any file that begins with Document.
In some graphical user interfaces, in particular older Microsoft applications, an asterisk is prepended to the current working document name shown in a window&#39;s title bar to indicate that unsaved changes exist. The asterisk was also used as a mask to hide passwords being entered into a text box, until Windows XP where this was changed to a bullet.
In Commodore (and related) filesystems, an asterisk appearing next to a filename in a directory listing denotes an improperly closed file, commonly called a &#34;splat file.&#34;
In travel industry Global Distribution Systems, the asterisk is the display command to retrieve all or part of a Passenger Name Record.
In HTML web forms, an asterisk can be used to denote required fields.
Chat Room etiquette calls on one asterisk to correct a misspelled word that has already been submitted. For example, one could post lck, then follow it with luck* or *luck.
Enclosing a phrase between two asterisks is used to denote an action the user is &#34;performing&#34;, e.g. *pulls out a paper*, although this usage is also common on forums, and less so on most chat rooms due to /me or similar commands. Hyphens (-action-) and double colons (::action::) are also used for similar purposes.


Adding machines and printing calculators

Some international models of adding machines and printing calculators use the asterisk to denote the total, or the terminal sum or difference of an addition or subtraction sequence, respectively, sometimes on the keyboard where the total key is marked with an asterisk and sometimes a capital T, and on the printout.


Programming languagesMany programming languages and calculators use the asterisk as a symbol for multiplication. It also has a number of special meanings in specific languages, for instance:

In some programming languages such as the C, C++, and Go programming languages, the asterisk is used to dereference or to declare a pointer variable.
In the Common Lisp programming language, the names of global variables are conventionally set off with asterisks, *LIKE-THIS*.
In the Ada, Fortran, Perl, Python, Ruby programming languages, in some dialects of the Pascal programming language, and many others, a double asterisk is used to signify exponentiation: 5**3 is 5*5*5 or 125.
In the Perl programming language, the asterisk is used to refer to the typeglob of all variables with a given name.
In the programming languages Ruby and Python, * has two specific uses. First, the unary * operator applied to a list object inside a function call will expand that list into the arguments of the function call. Second, a parameter preceded by * in the parameter list for a function will result in any extra positional parameters being aggregated into a tuple (Python) or array (Ruby), and likewise in Python a parameter preceded by ** will result in any extra keyword parameters being aggregated into a dictionary.
In the APL language, the asterisk represents the exponential and exponentiation functions. 


Comments in computing{{main}}
In the B programming language and languages that borrow syntax from it,
like C, PHP, Java, or C#, comments (parts of the code not intended to be compiled into the program) are marked by an asterisk combined with the slash:/* Here is a comment.
   The compiler will ignore it. */Some Pascal-like programming languages, for example, Object Pascal, Modula-2, Modula-3, and Oberon, as well as several other languages including ML, Mathematica, AppleScript, OCaml, Standard ML, and Maple, use an asterisk combined with a parenthesis:(* This is a comment.
   The compiler will ignore it. *)CSS, while not strictly a programming language, also uses the slash-star comment format.body {
 /* This ought to make the text more readable for far-sighted people */
 text-size:24pt;
}

MathematicsThe asterisk has many uses in mathematics. The following list highlights some common uses and is not exhaustive.

stand-alone

:

An arbitrary point in some set. Seen, for example, when computing Riemann sums or when contracting a simply connected group to the singleton set { {{unicode}} }.
as a unary operator, denoted in prefix notation

:

The Hodge dual operator on vector spaces *: A^k \rightarrow A^{n-k}.
as a unary operator, written as a subscript

:

The pushforward (differential) of a smooth map f between two smooth manifolds, denoted f{{unicode}}.
as a unary operator, written as a superscript

:

The complex conjugate of a complex number (the more common notation is \bar{z}).Complex Conjugate - from Wolfram MathWorld
The conjugate transpose, Hermitian transpose, or adjoint matrix of a matrix.
Hermitian adjoint.
The multiplicative group of a ring, especially when the ring is a field. E.g. \mathbb{C}^* = \mathbb{C}-\{0\}.
The dual space of a vector space V, denoted V*.
The combination of an indexed collection of objects into one example, e.g. the combination of all the cohomology groups Hk(X) into the cohomology ring H*(X).
In statistics, z* and t* are given critical points for z-distributions and t-distributions, respectively.
as a binary operator, in infix notation

:

A notation for an arbitrary binary operator.
The free product of two groups.
f {{unicode}} g is a convolution of f with g.

The asterisk is used in all branches of mathematics to designate a correspondence between two quantities denoted by the same letter – one with the asterisk and one without.
Mathematical typographyIn fine mathematical typography, the Unicode character U+2217 ({{unicode}}) "math asterisk" is available (HTML entity &amp;lowast;). This character also appeared in the position of the regular asterisk in the PostScript symbol character set in the Symbol font included with Windows and Macintosh operating systems and with many printers. It should be used in fine typography for a large asterisk that lines up with the other mathematical operators.
Statistical resultsIn many scientific publications, the asterisk is employed as a shorthand to denote the statistical significance of results when testing hypotheses. When the likelihood that a result occurred by chance alone is below a certain level, one or more asterisks are displayed. Popular significance levels are 0.05 (*), 0.01 (**), and 0.001 (***).
Human genetics

In human genetics, * is used to denote that someone is a member of a haplogroup and not any of its subclades (see ).


TelephonyOn a Touch-Tone telephone keypad, the asterisk (called star, or less commonly, palm or sextile){{Cite patent}} is one of the two special keys (the other is the number sign (pound sign or hash or, less commonly, octothorp or square)), and is found to the left of the zero. They are used to navigate menus in Touch-Tone systems such as Voice mail, or in Vertical service codes.
Cricket

In cricket, it signifies a total number of runs scored by a batsman without losing his wicket, e.g. 107* means &#39;107 not out&#39;. When written before a player&#39;s name on a scorecard, it indicates the captain of the team.



It is also used on television when giving a career statistic during a match. For example, 47* in a number of matches column means that the current game is the player&#39;s 47th.


Instant messaging and SMSWhen typos are made, it is customary to fix the mistake by sending another message, adding an asterisk to the beginning or end of the fixed line.{{cite book}} Sometimes only a single word is corrected in this way, if it's obvious where the mistake was in the original message.

Example 1 
Tom:  hey. how is brownie? 
Jane:  ugh. my stupid xat ran away again  
Jane:  
cat
 
Tom:  omg!  sorry to hear that :( 
When the writer wants to explain actions in a kind of 'narrated' style while messaging, verbs are sometimes written between two asterisks, such as "*nods*" or "*shakes head*". This is known as an "emote".
Economics

In economics, the use of an asterisk after a letter indicating a variable such as price, output, or employment indicates that the variable is at its optimal level (that which is achieved in a perfect market situation). For instance, p* is the price level p when output y is at its corresponding optimal level of y*.



Also in international economics asterisks are commonly used to denote economic variables in a foreign country. So, for example, &#34;p&#34; is the price of the home good and &#34;p*&#34; is the price of the foreign good, etc.


Education

In the GCSE and A-Level examinations in the United Kingdom and the PSLE in Singapore, A* (&#34;A-star&#34;) is a special top grade that is distinguished from grade A.


Games

Certain categories of character types in role-playing games are called splats, and the game supplements describing them are called splatbooks. This usage originated with the shorthand &#34;*book&#34; for this type of supplement to various World of Darkness games, such as Clanbook: Ventrue (for Vampire: The Masquerade) or Tribebook: Black Furies (for Werewolf: The Apocalypse), and this usage has spread to other games with similar character-type supplements. For example, Dungeons & Dragons Third Edition has had several lines of splatbooks: the &#34;X &#38; Y&#34; series including Sword & Fist and Tome & Blood prior to the &#34;3.5&#34; revision, the &#34;Complete X&#34; series including Complete Warrior and Complete Divine, and the &#34;Races of X&#34; series including Races of Stone and Races of the Wild.
In many MUDs and MOOs, as well as &#34;male&#34;, &#34;female&#34;, and other more esoteric genders, there is a gender called &#34;splat&#34;, which uses an asterisk to replace the letters that differ in standard English gender pronouns. For example, h* is used rather than him or her. Also, asterisks are used to signify doing an action, for example, &#34;*action*&#34;
Game show producer Mark Goodson used a six-pointed asterisk as his trademark. It is featured prominently on many set pieces from The Price Is Right.
Scrabble players put an asterisk after a word to indicate that an illegal play was made.{{cite web}}


Baseball

In recent years, the asterisk has come into use on scorecards to denote a &#34;great defensive play.&#34;Baseball Almanac - Scoring Baseball: Advanced Symbols However, off scorecards, it can have the same connotation as in other sports; see below.


Competitive sports and games

In colloquial usage, an asterisk is used to indicate that a record is somehow tainted by circumstances, which are putatively explained in a footnote referenced by the asterisk.See e.g. {{cite news}} This usage arose after the 1961 baseball season in which Roger Maris of the New York Yankees broke Babe Ruth&#39;s 34-year-old single-season home run record. Because Ruth had amassed 60 home runs in a season with only 154 games, compared to Maris&#39;s 61 over 162 games, baseball commissioner Ford Frick announced that Maris&#39; accomplishment would be recorded in the record books with an explanation (often referred to as &#34;an asterisk&#34; in the retelling). In fact, Major League Baseball had no official record book at the time, but the stigma remained with Maris for many years, and the concept of a real or figurative asterisk denoting less-than-official records has become widely used in sports and other competitive endeavors. A 2001 TV movie about Maris&#39; record-breaking season was called 61* (pronounced sixty-one asterisk) in reference to the controversy.



In February 2011 the United States Olympic Committee and the Ad Council launched an anti-steroid campaign called &#34;Play Asterisk Free&#34;Facebook.com aimed at teens.  The campaign, whose logo uses a heavy asterisk, first launched in 2008 under the name Don&#39;t Be An Asterisk.Adcouncil.org, Ad Council, August 8, 2008


Barry BondsFans critical of Barry Bonds, who has been accused of using performance-enhancing drugs during his baseball career, invoked the asterisk notion as he approached and later broke Hank Aaron's career home run record.See e.g. {{cite news}} After Bonds hit his record-breaking 756th home run on August 7, 2007, fashion designer and enterpreneur Marc Ecko purchased the home run ball from the fan who caught it, and ran a poll on his Web site to determine its fate. On September 26, Ecko revealed on NBC's Today show that the ball will be branded with an asterisk and donated to the Baseball Hall of Fame. The ball, marked with a die-cut asterisk, was finally delivered to the hall on July 2, 2008 after Marc Ecko unconditionally donated the artifact rather than loaning it to the hall as originally intended.
MarketingAsterisks (or other symbols) are commonly used in advertisements to refer readers to special terms/conditions for a certain statement, commonly placed below the statement in question. For example: an advertisement for a sale may have an asterisk after the word "sale" with the date of the sale at the bottom of the advertisement, similar to the way footnotes are used.
Encodings{{see also}}
The Unicode standard states that the asterisk is distinct from the Arabic five pointed star (U+066D), the asterisk operator (U+2217), and the heavy asterisk (U+2731).Detailed descriptions of the characters The ISO Latin 1 character repertoireThe symbols are compared below (the display depends on your browser's font).

Asterisk  Asterisk Operator  Heavy Asterisk  Small Asterisk  Full Width Asterisk  Open Centre Asterisk 
* ∗ ✱ ﹡ ＊ ✲ 


Low Asterisk  Arabic star  East Asian reference mark  Teardrop-Spoked Asterisk  Sixteen Pointed Asterisk 
⁎ ٭  ※  ✻ ✺ 


  Unicode Decimal UTF-8 HTML Displayed 
Asterisk U+002A &#38;#42; 2A   * 
Small Asterisk U+FE61 &#38;#65121; EF B9 A1   ﹡ 
Full Width Asterisk U+FF0A &#38;#65290; EF BC 8A   ＊ 
Low Asterisk U+204E &#38;#8270; E2 81 8E   ⁎ 
Asterisk Operator (Math Asterisk) U+2217 &#38;#8727; E2 88 97 &#38;lowast; ∗ 
Heavy Asterisk U+2731 &#38;#10033; E2 9C B1   ✱ 
Open Centre Asterisk U+2732 &#38;#10034; E2 9C B2   ✲ 
Eight Spoked Asterisk U+2733 &#38;#10035; E2 9C B3   ✳ 
Sixteen Pointed Asterisk U+273A &#38;#10042; E2 9C BA   ✺ 
Teardrop-Spoked Asterisk U+273B &#38;#10043; E2 9C BB   ✻ 
Open Centre Teardrop-Spoked Asterisk U+273C &#38;#10044; E2 9C BC   ✼ 
Heavy Teardrop-Spoked Asterisk U+273D &#38;#10045; E2 9C BD   ✽ 
Four Teardrop-Spoked Asterisk U+2722 &#38;#10018; E2 9C A2   ✢ 
Four Balloon-Spoked Asterisk U+2723 &#38;#10019; E2 9C A3   ✣ 
Heavy Four Balloon-Spoked Asterisk U+2724 &#38;#10020; E2 9C A4   ✤ 
Four Club-Spoked Asterisk U+2725 &#38;#10021; E2 9C A5   ✥ 
Heavy Teardrop-Spoked Pinwheel Asterisk U+2743 &#38;#10051; E2 9D 83   ❃ 
Balloon-Spoked Asterisk U+2749 &#38;#10057; E2 9D 89   ❉ 
Eight Teardrop-Spoked Propeller Asterisk U+274A &#38;#10058; E2 9D 8A   ❊ 
Heavy Eight Teardrop-Spoked Propeller Asterisk U+274B &#38;#10059; E2 9D 8B   ❋ 
Arabic star U+066D &#38;#1645; D9 AD   ٭ 
East Asian reference mark U+203B &#38;#8251; E2 80 BB   ※ 
Tag Asterisk U+E002A &#38;#917546; F3 A0 80 AA    

See also

Arabic star
Asterism (typography)
Star (glyph)


References{{reflist}}
ca:Asterisc
cy:Seren (symbol)
da:Asterisk
de:Sternchen (Schriftzeichen)
es:Asterisco
eo:Asterisko
eu:Izartxo
fr:Astérisque
xal:Одн темдг
ko:별표
hr:Zvjezdica (pravopis)
io:Asterisko
it:Asterisco
he:כוכבית
kk:Жұлдызша
lt:Asteriskas
hu:Csillag (írásjel)
nl:Asterisk
ja:アスタリスク
no:Asterisk
nn:Asterisk
pl:Asterysk
pt:Asterisco
ro:Asterisc
ru:Звёздочка (типографика)
sk:Hviezdička (typografia)
sr:Астериск
sh:Asterisk
fi:Asteriski
sv:Asterisk
th:ดอกจัน
tr:Yıldız işareti
uk:Зірочка
zh:星號