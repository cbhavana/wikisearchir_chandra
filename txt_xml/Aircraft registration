title{{Infobox aviation}}An aircraft registration is a unique alphanumeric string that identifies a civil aircraft, in similar fashion to a licence plate on an automobile.  In accordance with the Convention on International Civil Aviation all aircraft must be registered with a national aviation authority and they must carry proof of this registration in the form of a legal document called a Certificate of Registration at all times when in operation.  Most countries also require the aircraft registration to be imprinted on a permanent fireproof plate mounted on the fuselage for the purposes of post-fire/post-crash aircraft accident investigation.Because airplanes typically display their registration numbers on the aft fuselage just forward of the tail, in earlier times more often on the tail itself, the registration is often referred to as the "tail number". Although each aircraft registration is unique, some, but not all countries allow it to be re-used when the aircraft has been sold, destroyed or retired. For example N3794N is assigned to a Mooney M20F.N3794N It had been previously assigned to a Beechcraft Bonanza (specifically, the aircraft in which Buddy Holly was killed). Also note that an individual aircraft may be assigned different registrations during its existence. This can be because the aircraft changes ownership, state of registration, or in some cases for vanity reasons.
Certificate of RegistrationThe Certificate of Registration contains contact information used by national authorities for enforcement purposes, and for the purposes of disseminating Airworthiness Directives to aircraft owners.  Most national authorities require that the aircraft owner update said contact information immediately or as soon as possible any time there is a change in the same.{{See also}} for information about registering aircraft ELTs.
International standards The first use of aircraft registrations was based on the radio callsigns allocated at the London International Radiotelegraphic Conference in 1913. This was modified by agreement by the International Bureau at Berne and published on April 23, 1913. Although initial allocations were not specifically for aircraft but for any radio user, the International Air Navigation Convention held in Paris in 1919 made allocations specifically for aircraft registrations based on the 1913 callsign list. The agreement stipulated that the nationality marks were to be followed by a hyphen then a group of four letters that must include a vowel (and for the convention Y was considered to be a vowel).At the International Radiotelegraph Convention at Washington in 1927 the list of markings was revised and adopted from 1928, these allocations are the basis of the currently used registrations. The marking have been amended and added to over the years and the allocations and standard are managed by the International Civil Aviation Organization (ICAO).Article 20 of the Chicago Convention on International Civil Aviation (signed in 1944) requires that all signatory countries register aircraft over a certain weight with a national aviation authority. Upon registration, the aircraft receives its unique "registration" which must be displayed prominently on the aircraft.Annex 7 to the Convention on International Civil Aviation describes the definitions, location, and measurement of nationality and registration marks. The aircraft registration is made up of a prefix selected from the country's callsign prefix allocated by the International Telecommunication Union (ITU) (making the registration a quick way of determining the country of origin) and the registration suffix. Depending on the country of registration, this suffix is a numeric or alphanumeric code and consists of one to five digits or characters respectively.The ICAO provides a supplement to Annex 7 which provides an updated list of approved Nationality and Common Marks used by various countries.
Country-specific usage
When painted on the fuselage, the prefix and suffix are separated by a dash (for example YR-BMA), but when entered in a flight plan, the dash is omitted (for example YRBMA). In the United States, the prefix and suffix are painted without a dash. Aircraft flying privately usually use their registration as their radio callsign, but many aircraft flying in commercial operations (especially charter, cargo, and airlines) use the ICAO airline designator or a company callsign. In some instances, it may be sufficient to simply display the suffix letters, with the country prefix omitted. For example, gliders registered in Australia would omit the VH prefix and simply display the suffix. Obviously this is only suitable where the aircraft does not fly in the airspace of another country.Even if the suffix consists solely of alphabetical characters in a certain country, gliders and ultralights may sometimes use digits instead. For example, in Germany, D-ABCD can be a powered airplane while D-1234 is a glider. In Australia, early glider registration suffixes began with the letters "G-AU", and it is not uncommon to find such gliders only displaying the last two letters of the suffix, as they lacked the range to travel internationally. For example, VH-GIQ would simply be displayed as IQ.Different countries have different registration schemes: Canadian registrations start with C, British with G, German with D, and so forth. A comprehensive list is tabulated below.
United StatesIn the United States, the registration number is also referred to as an "N-number", as all aircraft registered there have a number starting with N. An alpha-numeric system is used because of the large numbers of aircraft registered in the United States. An N-number may only consist of one to five characters, must start with a digit other than zero, and cannot end in a run of more than two letters. In addition, N-numbers may not contain the letters I or O, due to their similarities with the numerals 1 and 0.FAA registration numbering scheme Each alphabetic character in the suffix can have one of 24 discrete values, while each numeric digit can be one of 10, except the first, which can take on only nine values. This yields a total of 915,399 possible registration numbers in the namespace, though certain combinations are reserved either for government use or for other special purposes.  With so many possible calls radio shortcuts are used.  Normally when flying entirely within the United States an aircraft would not identify itself starting with "N", since that is assumed.  Also, after initial contact is made with an aircraft control site only the last two or three characters are typically used.The following are the combinations that could be used:

N1 to N9 — Federal Aviation Administration (FAA) internal use only
N1A to N9Z
N1AA to N9ZZ
N10 to N99 — Federal Aviation Administration (FAA) internal use only
N10A to N99Z
N10AA to N99ZZ
N100 to N999
N100A to N999Z
N100AA to N999ZZ
N1000 to N9999
N1000A to N9999Z
N10000 to N99999

An older aircraft (registered before 31 December 1948) may have a second letter in its identifier, identifying the category of aircraft. This additional letter is not actually part of the aircraft identification (e.g. NC12345 is the same registration as N12345). Aircraft category letters have not been included on any registration numbers issued since 1 January 1949, but they still appear on antique aircraft for authenticity purposes. The categories were:

C = airline, commercial and private
G = glider
L = limited
R = restricted (such as cropdusters and racing aircraft)
S = state
X = experimental

For example, N-X-211, the Ryan NYP aircraft flown by Charles Lindbergh as the Spirit of St. Louis, was registered in the experimental category.
Decolonisation and independenceThe impact of decolonisation and independence on aircraft registration schemes has varied from place to place. Most countries on independence have had a new allocation granted taken from either an existing ITU allocation or the former countries ITU allocation. For example after partition in 1947 India retained the VT designation used by the British Raj, while Pakistan adopted the AP designation from the newly allocated ITU callsigns APA-ASZ. Hong Kong, which formerly used the VR-H designation, had the "VR" replaced with the Chinese "B" upon the 1997 Handover to China, resulting in aircraft designations with only four letters in total (as opposed to the international norm of five letters).
List of countries/regions and their registration prefixes and patterns


Country / Region Registration Prefix Presentation 
Afghanistan YA YA-AAA to YA-ZZZ 
Albania ZA ZA-AAA to ZA-ZZZ 
Algeria 7T 7T-AAA to 7T-ZZZ 
Andorra C3 C3-AAA to C3-ZZZ 
Angola D2 D2-AAA to D2-ZZZ 
Anguilla VP-A VP-AAA to VP-AZZ 
Antigua and Barbuda V2 V2-AAA to V2-ZZZ 
Argentina LVLQ LV-AAA to LV-ZZZLQ-AAA to LQ-ZZZ (Official use only)  
Armenia EK EK-10000 to EK-99999 
Aruba P4 P4-AAA to P4-ZZZ 
Australia VH VH-AAA to VH-ZZZ 
Austria OE OE-AAA to OE-KZZOE-BAA to OE-BZZ (governmental service)OE-LAA to OE-LZZ (airlines operating scheduled flights)OE-VAA to OE-VZZ (test registrations)OE-WAA to OE-WZZ (amphibian and sea planes)OE-XAA to OE-XZZ (helicopters)OE-0001 to OE-5999 (gliders)OE-9000 to OE-9999 (motor gliders) 
Azerbaijan 4K 4K-AZ1 to 4K-AZ9994K-10000 to 4K-99999 
Bahamas C6 C6-AAA to C6-ZZZ 
Bahrain A9C A9C-AA to A9C-ZZ 
Bangladesh S2 S2-AAA to S2-ZZZ 
Barbados 8P 8P-AAA to 8P-ZZZ 
Belarus EW EW-10000 to EW-99999 (ex-Soviet Union registrations)EW-100AA to EW-999ZZ Aircraft in general, except:EW-200PA to EW-299PA for Boeing 737EW-100PJ to ER-299PJ for CRJ EW-001PA, EW-85815 governmentalEW-0001L to EW-9999L for balloons 
Belgium OO OO-AAA to OO-ZZZ (OO-Q never used)OO-BAA to OO-BZZ (preferred for Balloons)OO-YAA to OO-ZAA (preferred for gliders)OO-01 to OO-499 (Homebuilt aircraft)OO-501 to OO-999 &#38; OO-A01 to OO-Z99 (Microlights) 
Belize V3 V3-AAA to V3-ZZZ 
Benin TY TY-AAA to TY-ZZZ 
Bermuda VP-B, VQ-B VP-BAA to VP-BZZ, VQ-BAA to VQ-BZZ 
Bhutan A5 A5-AAA to A5-ZZZ 
Bolivia CP CP-1000 to CP-9999 
Bosnia and Herzegovina T9 T9-AAA to T9-ZZZ 
Bosnia and Herzegovina E7 E7-AAA to E7-ZZZ 
Botswana A2 A2-AAA to A2-ZZZ 
Brazil PPPRPTPU PP-AAA to PP-ZZZPR-AAA to PR-ZZZPT-AAA to PT-ZZZPU-AAA to PU-ZZZ (Microlights/Experimental aircraft) 
British Virgin Islands VP-L VP-LAA to VP-LZZ 
Brunei V8 V8-AAA to V8-ZZZV8-AA1 to V8-ZZ9V8-001 to V8-999 
Bulgaria LZ LZ-AAA to LZ-ZZZ 
Burkina Faso XT XT-AAA to XT-ZZZ 
Burundi 9U 9U-AAA to 9U-ZZZ 
Cambodia XU XU-AAA to XU-ZZZ 
Cameroon TJ TJ-AAA to TJ-ZZZ 
Canada C C-FAAA to C-FZZZC-GAAA to C-GZZZC-IAAA to C-IZZZ (ultralight aeroplanes only) 
Cape Verde D4 D4-AAA to D4-ZZZ 
Cayman Islands VP-C VP-CAA to VP-CZZ 
Central African Republic TL TL-AAA to TL-ZZZ 
Chad TT TT-AAA to TT-ZZZ 
Chile CC CC-AAA to CC-ZZZ
CC-CAA to CC-CZZ (commercial aircraft)
CC-PAA to CC-PZZ (private aircraft) 
Republic of China (Taiwan) B B-10000 to B-99999 
People's Republic of China B B-1000 to B-9999 
Hong Kong, China B-H (formerly VR-H)B-KB-L  B-HAA to B-HZZ B-KAA to B-KZZB-LAA to B-LZZ 
Macau, China B-M (formerly CS- during Portuguese rule before 1999) B-MAA to B-MZZ 
Colombia HJHK HJ-1000A to HJ-9999Z (Microlights) HK-1000A to HK-9999Z 
Comoros D6 D6-AAA to D6-ZZZ 
Congo, Republic of TN TN-AAA to TN-ZZZ 
Cook Islands E5 E5-AAA to E5-ZZZ 
Congo, Democratic Republic of 9Q 9Q-AAA to 9Q-ZZZ 
Costa Rica TI TI-AAA to TI-ZZZ 
Côte d'Ivoire TU TU-AAA to TU-ZZZ 
Croatia 9A 9A-AAA to 9A-ZZZ9A-GAA to 9A-GZZ (Gliders)9A-HAA to 9A-HZZ (Helicopters) 
Cuba CU CU-C1000 to CU-C1999 (Airlines, cargo operations)http://www.airliners.net/photo/Cessna-337-Super/1165794/L/, http://www.airliners.net/photo/Cubana-Cargo/Tupolev-Tu-204CE/1243594/L/ and http://www.airliners.net/photo/Aerogaviota/Mil-Mi-8T/0215919/L/CU-E1000 to CU-E1999 (Agricultural Aircraft)Tupolev Tu-154 Monography, Dimitry Komissarov, Aerofax 2007 (ISBN 1-85780-241-1)CU-H1000 to CU-H1999 (Helicopters)CU-N1000 to CU-N1999 (Private Aircraft)CU-T1000 to CU-T1999 (Airlines, passenger flights)CU-U1000 to CU-U1999 (Ultralights)http://www.airliners.net/photo/Untitled/Polarismotor%20FIB%20582/1512287/L/ 
Cyprus, Republic of 5B 5B-AAA to 5B-ZZZ 
Czech Republic OK OK-AAA to OK-ZZZOK-AAA 00 to OK-ZZZ 99 (Microlights)OK-0000 to OK-9999 (Gliders &#38; balloons)OK-A000 to OK-A999 (Ultralight gliders)Decree No. 155/2005 Coll. of the Ministry of Informatics of the Czech Republic, §&nbsp;9&nbsp;(3)h) 
Denmark OY {{ref}} OY-AAA to OY-ZZZOY-HAA to OY-HZZ (Helicopters) 
Djibouti J2 J2-AAA to J2-ZZZ 
Dominica J7 J7-AAA to J7-ZZZ 
Dominican Republic HI HI-100AA to HI-999ZZ 
East Timor 4W {{ref}}  
Ecuador HC HC-AAA to HC-ZZZ 
Egypt SU SU-AAA to SU-XXZSU-ZAA to SU-ZZZSU-001 to SU-999 (Gliders and balloons) 
El Salvador YS YS-AAA to YS-ZZZ 
Equatorial Guinea 3C 3C-AAA to 3C-ZZZ 
Eritrea E3 E3-AAAA to E3-ZZZZ 
Estonia ES ES-AAA to ES-ZZZ 
Ethiopia ET ET-AAA to ET-ZZZ 
Falkland Islands VP-F VP-FAA to VP-FZZ 
Faroe Islands OY {{ref}} OY-AAA to OY-ZZZOY-HAA to OY-HZZ (Helicopters)OY-XAA to OY-XZZ (Gliders) 
Fiji Islands DQ DQ-AAA to DQ-ZZZ 
Finland OH OH-AAA to OH-ZZZOH-001 to OH-999 (gliders)OH-G001 to OH-G999 (autogyros)OH-U001 to OH-U999 (ultralights) 
France F F-AAAA to F-ZZZZF-CAAA to F-CZZZ (Gliders) F-OAAA to F-OZZZ (Overseas Territories) F-PAAA to F-PZZZ (Homebuilt) F-WAAA to F-WZZZ (Test and Delivery)F-ZAAA to F-ZZZZ (State owned)&#34;department number&#34;-AA to -ZZ &#38; -AAA to -ZZZ (Ultralights)[e.g.: 59-ABC] 
French West Indies F-OG F-OGAA to F-OGZZ 
French Guyana F-O F-OAAA to F-OZZZ 
Gabon TR TR-AAA to TR-ZZZ 
Gambia C5 C5-AAA to C5-ZZZ 
Georgia 4L 4L-AAA to 4L-ZZZ4L-10000 to 4L-99999 
Germany D D-AAAA to D-AZZZ for aircraft with more than 20t MTOW D-BAAA to D-BZZZ for aircraft with 14t-20t MTOW D-CAAA to D-CZZZ for aircraft with 5,7t-14t MTOW D-EAAA to D-EZZZ for single engine aircraft up to 2t MTOW D-FAAA to D-FZZZ for single engine aircraft from to 2t-5,7t MTOW D-GAAA to D-GZZZ for multi-engine aircraft up to 2t MTOW D-IAAA to D-IZZZ for multi-engine aircraft from 2t-5,7t MTOW D-HAAA to D-HZZZ for rotorcraft D-KAAA to D-KZZZ for powered gliders D-LAAA to D-LZZZ for airships D-MAAA to D-MZZZ for powered sports aircraft D-NAAA to D-NZZZ for non-powered sports aircraft D-OAAA to D-OZZZ for manned free ballons D-0001 to D-9999 for Gliders 
Ghana 9G 9G-AAA to 9G-ZZZ 
Gibraltar VP-G VP-GAA to VP-GZZ 
Greece SX SX-AAA to SX-ZZZ 
Greenland OY {{ref}} OY-AAA to OY-ZZZ 
Grenada J3 J3-AAA to J3-ZZZ 
Guatemala TG TG-AAA to TG-ZZZ 
Guinea 3X 3X-AAA to 3X-ZZZ 
Guinea Bissau J5 J5-AAA to J5-ZZZ 
Guyana 8R 8R-AAA to 8R-ZZZ 
Haiti HH HH-AAA to HH-ZZZ 
Honduras HR HR-AAA to HR-ZZZ 
Hungary HA HA-AAA to HA-ZZZ 
Iceland TF TF-AAA to TF-ZZZTF-100 to TF-999 (Microlights) 
India VT VT-AAA to VT-ZZZVT-HAA to VT-HZZ (Helicopters) 
Indonesia PK PK-AAA to PK-ZZZ 
Iran EP EP-AAA to EP-ZZZ 
Iraq YI YI-AAA to YI-ZZZ 
Ireland EI EI-AAA to EI-ZZZ 
Isle of ManITU code 'M' is registered to the United Kingdom. The Isle of Man is not a sovereign entity in international law. M M-AAAA to M-ZZZZ 
Israel 4X 4X-AAA to 4X-ZZZ 
Italy I I-AAAA to I-ZZZZ 
Jamaica 6Y 6Y-AAA to 6Y-ZZZ 
Japan JA JA-0001 to JA-9999JA-001A to JA-999ZJA-01AA to JA-99ZZJA-A001 to JA-A999 (Balloons) 
Jordan JY JY-AAA to JY-ZZZ 
Kosovo, Republic of Z6 Z6-AAA to Z6-ZZZ 
Kazakhstan UP UP-AAA01 to UP-ZZZ99 (Suffix letters refer to aircraft type) 
Kenya 5Y 5Y-AAA to 5Y-ZZZ 
Kiribati T3 T3-AAA to T3-ZZZ 
Korea, People's Democratic Rep. P P-500 to P-999 
Korea, Republic of HLCivil Aircraft Safety Authority (in Korean) HL0000 to HL0599 for gliderHL0600 to HL0799 for airshipHL1000 to HL1799 for piston engineHL2000 to HL2099 for piston engineHL5100 to HL5499 for turbo propHL6100 to HL6199HL6200 to HL6299HL7100 to HL7199 for single turbojetHL7200 to HL7299 HL7300 to HL7399HL7400 to HL7499HL7500 to HL7599HL7600 to HL7699HL8200 to HL8299HL8400 to HL8499HL8600 to HL8699HL9100 to HL9199HL9200 to HL9299HL9200 to HL9299HL9300 to HL9399HL9400 to HL9499HL9500 to HL9599HL9600 to HL9699 
Kuwait 9K 9K-AAA to 9K-ZZZ 
Kyrgyzstan EX EX-100 to EX-999EX-10000 to EX-99999 
Laos RDPL RDPL-10000 to RDPL-99999 
Latvia YL YL-AAA to YL-ZZZ 
Lebanon OD OD-AAA to OD-ZZZ 
Lesotho 7P 7P-AAA to 7P-ZZZ 
Liberia A8 A8-AAA to A8-ZZZ 
Libya 5A 5A-AAA to 5A-ZZZ 
Liechtenstein HB HB-AAA to HB-ZZZ (shares allocation with Switzerland) 
Lithuania LY LY-AAA to LY-ZZZ 
Luxembourg LX{{ref}} LX-AAA to LX-ZZZLX-BAA to LX-BZZ (Balloons)LX-CAA to LX-CZZ (Glider)LX-HAA to LX-HZZ (Helicopters)LX-XAA to LX-XZZ (Ultralights) 
Macedonia Z3 Z3-AAA to Z3-ZZZZ3-HAA to Z3-HZZ (Helicopters)Z3-UA-001 to Z3-UA-999 (Ultralight) Z3-OAA to Z3-OZZ (Hot air balloons) 
Madagascar 5R 5R-AAA to 5R-ZZZ 
Malawi 7Q 7Q-AAA to 7Q-ZZZ 
Malaysia 9M 9M-AAA to 9M-ZZZ9M-EAA to 9M-EZZ (Amateur-built)9M-UAA to 9M-UZZ (Microlight) 
Maldives 8Q 8Q-AAA to 8Q-ZZZ 
Mali TZ TZ-AAA to TZ-ZZZ 
Malta 9H 9H-AAA to 9H-ZZZ 
Marshall Islands V7 V7-0001 to V7-9999 
Mauritania 5T 5T-AAA to 5T-ZZZ 
Mauritius 3B 3B-AAA to 3B-ZZZ 
Mexico XAXBXC XA-AAA to XA-ZZZXB-AAA to XB-ZZZXC-AAA to XC-ZZZ 
Micronesia, Federated States of V6 V6-AAA to V6-ZZZ 
Moldova ER ER-AAA to ER-ZZZER-10000 to ER-99999 
Monaco 3A 3A-AAA to 3A-ZZZ3A-HAA to 3A-HZZ (Helicopters) 
Mongolia JU JU-1000 to JU-9999 
Montenegro 4O 4O-AAA to 4O-ZZZ 
Montserrat VP-M VP-MAA to VP-MZZ 
Morocco CN CN-AAA to CN-ZZZ 
Mozambique C9 C9-AAA to C9-ZZZ 
Myanmar XYXZ XY-AAA to XY-ZZZXZ-AAA to XZ-ZZZ (Not Used) 
Namibia V5 V5-AAA to V5-ZZZ 
Nauru C2 C2-AAA to C2-ZZZ 
Nepal 9N 9N-AAA to 9N-ZZZ 
Netherlands PH PH-AAA to PH-ZZZPH-1A1 to PH-9Z9 (Microlights)PH-100 to PH-9999 (Gliders) 
Netherlands Antilles PJ PJ-AAA to PJ-ZZZ 
New Zealand ZK  ZK-A**, ZK-B**, ZK-GA*, ZK-HA* reserved for historical aircraft including helicopters and gliders since 1987
ZK-FA*, ZK-FB* balloons
ZK-G** gliders
ZK-H**, ZK-I** helicopters
ZK-RA*, ZK-RB*, ZK-RC*, ZK-RD* gyrocopters
ZK-Q** marks are prohibited by ICAO
Remainder for fixed-wing aircraft{{cite web}} 
Nicaragua YN YN-AAA to YN-ZZZ 
Niger 5U 5U-AAA to 5U-ZZZ 
Nigeria 5N 5N-AAA to 5N-ZZZ 
Norway LN LN-AAA to LN-ZZZ Aircraft in general, except:LN-G**, GlidersLN-O**, HelicoptersLN-C**, BalloonsLN-Y**, Ultralight Aircraft 
Oman A4O A4O-AA to A4O-ZZ 
Pakistan AP AP-AAA to AP-ZZZ 
Palestine SU-YE4 SU-YAA to SU-YZZ{{ref}} 
Panama HP HP-1000AA to HP-9999ZZ 
Papua New Guinea P2 P2-AAA to P2-ZZZ 
Paraguay ZP ZP-AAA to ZP-ZZZ 
Peru OB OB-1000 to OB-9999 
Philippines RP-C RP-C0001 to RP-C9999 
Poland SP,SN SP-AAA to SP-ZZZSP-1000 to SP-3000, SP-8000 (gliders) SP-SAAA to SP-SZZZ (ultralights) SN-00AA (police and border guard) 
Portugal CR CR-AAA to CR-ZZZ 
Portugal CS CS-AAA to CS-ZZZ Aircraft in general, except:CS-H**, Helicopters CS-X**, Experimental CS-U**, Ultralight Aircraft 
Qatar A7 A7-AAA to A7-ZZZ 
Réunion Island F-OD F-ODAA to F-ODZZ 
Romania YR YR-AAA to YR-ZZZYR-1000 to YR-9999 (Gliders) 
Russian Federation RA RA-00001 to RA-99999RA-0001A to RA-9999Z 
Russian Federation RF RF-00001 to RF-99999 (state-owned aircraft) 
Rwanda 9XR 9XR-AA to 9XR-ZZ 
Saint Helena/Ascension VQ-H VQ-HAA to VQ-HZZ 
Saint Kitts and Nevis V4 V4-AAA to V4-ZZZ 
Saint Lucia J6 J6-AAA to J6-ZZZ 
Saint Vincent and the Grenadines J8 J8-AAA to J8-ZZZ 
Samoa 5W 5W-AAA to 5W-ZZZ 
San Marino T7 T7-AAA to T7-ZZZT7-001 to T7-999 (Microlights) 
São Tomé and Príncipe S9 S9-AAA to S9-ZZZ 
Saudi Arabia HZ HZ-AAA to HZ-ZZZHZ-AA1 to HZ-ZZ99HZ-AAA1 to HZ-ZZZ99HZ-AAAA to HZ-ZZZZ 
Senegal 6V 6V-AAA to 6V-ZZZ 
Serbia YU YU-AAA to YU-ZZZ 
Seychelles S7 S7-AAA to S7-ZZZ 
Sierra Leone 9L 9L-AAA to 9L-ZZZ 
Singapore 9V 9V-AAA to 9V-ZZZ 
Slovakia OM OM-AAA to OM-ZZZOM-AAAA to OM-ZZZZ (Ultralight)OM-M000 to OM-M999 (Microlights)OM-0000 to OM-9999 (Gliders) 
Slovenia S5 S5-AAA to S5-ZZZS5-HAA to S5-HZZ (Helicopters) 
Solomon Islands H4 H4-AAA to H4-ZZZ 
Somalia 6O 6O-AAA to 6O-ZZZ 
South Africa ZSZTZU ZS-AAA to ZS-ZZZ (type certified aircraft)ZT-AAA to ZT-ZZZ (not used)ZU-AAA to ZU-ZZZ (non-type certified aircraft) 
South Sudan   
Spain EC EC-AAA to EC-WZZEC-YAA to EC-ZZZ (Homebuilt aircraft)EC-AA0 to EC-ZZ9 (Ultralight)EC-001 to EC-999 (Test and delivery) 
Sri Lanka 4R 4R-AAA to 4R-ZZZ 
Sudan ST ST-AAA to ST-ZZZ 
Surinam PZ PZ-AAA to PZ-ZZZ 
Swaziland 3D 3D-AAA to 3D-ZZZ 
Sweden SE SE-AAA to SE-ZZZSE-YAA to SE-VZZ ultralights 
Switzerland HB HB-AAA to HB-ZZZHB-1 to HB-9999 for Gliders and Motorgliders 
Syria YK YK-AAA to YK-ZZZ 
Tahiti F-OH F-OHAA to F-OHZZ 
Tajikistan EY EY-10000 to EY-99999 
Tanzania 5H 5H-AAA to 5H-ZZZ 
Thailand HS HS-AAA to HS-ZZZ 
Togo 5V 5V-AAA to 5V-ZZZ 
Tonga A3 A3-AAA to A3-ZZZ 
Trinidad and Tobago 9Y 9Y-AAA to 9Y-ZZZ 
Tunisia TS TS-AAA to TS-ZZZ 
Turkey TC TC-AAA to TC-ZZZ 
Turkmenistan EZ EZ-A100 to EZ-Z999 
Turks and Caicos VQ-T VQ-TAA to VQ-TZZ 
Tuvalu T2 T2-AAA to T2-ZZZ 
Uganda 5X 5X-AAA to 5X-ZZZ 
Ukraine UR UR-AAA to UR-ZZZUR-10000 to UR-99999 
United Arab Emirates A6 A6-AAA to A6-ZZZ 
United Kingdom G G-AAAA to G-ZZZZG-1-1 to G-99-99 (Test and delivery)G-A00AA to G-Z99ZZ (Concorde use in 1979 and 1980). 
United Nations{{ref}} 4U 4U-AAA to 4U-ZZZ 
United States of America N N1 to N99999N1A to N9999ZN1AA to N999ZZ 
Uruguay CX CX-AAA to CX-ZZZ 
Uzbekistan UK UK-10000 to UK-99999 
Vanuatu YJ YJ-AA1 to YJ-ZZ99 
Venezuela YV YV0001 to YV9999YV-0001A to YV-9999PYV-AAA1 to YV-ZZZ9 (Official use) 
Vietnam VN VN-1000 to VN-9999VN-A100 to VN-A999 
Yemen 7O 7O-AAA to 7O-ZZZ 
Zambia 9J 9J-AAA to 9J-ZZZ 
Zimbabwe Z Z-AAA to Z-ZZZ 

Notes

{{note}} Has not been used on any aircraft previously.
{{note}} OY-Hab is reserved for helicopters, OY-Xab is for gliders only and OY-Bab is preferred for hot-air balloons.
{{note}} Besides the United Nations, intergovernmental organizations (IGOs) such as the European Union and NATO do not have the power to create aircraft registrations.
{{note}} NATO has registered its aircraft with Luxembourg.


Pre-1928 AllocationsNote: in the suffix pattern, n represents a number, x represents a letter

Country / Region Registration Prefix Suffix Pattern 1913 radio call letters 
Abyssinia A-B{{ref}} A-Bxxx  
Afghanistan Y-A{{ref}} Y-Axxx  
Albania B-A{{ref}} B-Axxx  
Argentina R-A{{ref}} R-Axxx LIA to LRZ 
Australia G-AU{{cite book}} G-AUxx VHA to VKZ 
Austria-Hungary and Bosnia-Herzegovina   HAA to HFZ, OGA to OMZ and UNA to UZZ 
Belgium O-B{{ref}} O-Bxxx ONA to OTZ 
Bolivia C-B{{ref}} C-Bxxx none 
Brazil P-B{{ref}} P-Bxxx SNA to STZ 
Canada G-C G-Cxxx, except G-CYxx VAA to VGZ (Newfoundland: VOA to VOZ) 
G-CY (military aircraft) G-CYxx 
Bulgaria B-B{{ref}} B-Bxxx LXA to LZZ 
Chile B-C{{ref}} B-Cxxx COA to CPZ 
China X-C{{ref}} X-Cxxx none 
Colombia C- C-n to C-nnn none 
Costa Rica K-C{{ref}} K-Cxxx  
Cuba C-C{{ref}} C-Cxxx none 
Czechoslovakia L-B{{ref}} L-Bxxx  
Danzig 
Y-M{{ref}} Y-Mxxx Not applicable 
Dz- Dz-nnn  
Denmark T-D{{ref}} T-Dxxx OUA to OZZ 
Dominica Z-D{{ref}} Z-Dxxx  
Ecuador E-E{{ref}}{{ref}} E-Exxx  
Egypt   SUA to SUZ 
El Salvador Y-S{{ref}} Y-Sxxx  
Estonia E-A{{ref}} E-Axxx  
Finland K-S{{ref}} K-Sxxx  
France F-{{ref}} F-xxxx F and UAA to UMZ 
Germany D- D-nnnn A, D and KAA to KCZ 
Greece S-G{{ref}} S-Gxxx SVA to SZZ 
Guatemala L-G{{ref}} L-Gxxx  
Haiti H-H{{ref}}{{ref}} H-Hxxx  
Hedjaz A-H{{ref}} A-Hxxx  
Honduras X-H{{ref}} X-Hxxx  
Hungary 
H-H{{ref}} H-Hxxx  
H-O{{ref}} H-Oxxx  
India G-I G-Ixxx VTA to VWZ 
Italy I-{{ref}} I-xxxx I 
Japan J-{{ref}} J-xxxx J 
Latvia B-L{{ref}} B-Lxxx 
Liberia L-L{{ref}} L-Lxxx  
Lithuania Z-L{{ref}} Z-Lxxx  
Luxembourg L-U{{ref}} L-Uxxx 
Mexico   XAA to XCZ 
Monaco M-M{{ref}} M-Mxxx CQA to CQZ 
M-O{{ref}} M-Oxxx 
Morocco   CNA to CNZ 
Netherlands H-N{{ref}} H-Nxxx PAA to PMZ 
New Zealand G-NZ G-NZxx VLA to VMZ 
Nicaragua A-N{{ref}} A-Nxxx 
Norway   LAA to LHZ 
Panama S-P{{ref}} S-Pxxx  
Persia P-I{{ref}} P-Ixxx  
Peru O-P{{ref}} O-Pxxx  
Poland P-P{{ref}} P-Pxxx  
Portugal C-P{{ref}} C-Pxxx CRA to CTZ 
Romania C-R{{ref}} C-Rxxx CVA to CVZ 
Russia   R 
Serbia-Croatia-Slavonia X-S{{ref}} X-Sxxx  
Siam H-S{{ref}} H-Sxxx HGA to HHZ 
South Africa G-UA G-UAxx VNA to VNZ 
Spain M-{{ref}} M-xxxx EAA to EGZ 
Sweden S-A{{ref}} S-Axxx SAA to SMZ 
Switzerland C-H{{ref}} C-Hnnn  
United States of America N{{ref}} N-xxxx KDA to KZZ, N and W 
United Kingdom KUsed 1919-20, K-100 to K-175 allocated, reallocated to G-EAxx series in 1920. {{cite book}} K-nnn B, G and M (British colonies not autonomous: VPA to VSZ) 
G-E G-EAxx, G-EBxx, G-EDCA 
G-F (lighter than air craft) G-FAAx 
G-G (gliders) G-GAAx 
Uruguay C-U{{ref}}{{ref}} C-Uxxx CWA to CWZ 


Notes
{{note}} Adopted at the International Commission for Air Navigation (ICAN) meeting of 13 October 1919.{{cite web}}
{{note}} Adopted at the ICAN meeting of 13 July 1922.{{cite web}}
{{note}} Adopted at the ICAN meeting of 25 October 1922.
{{note}} Adopted at the ICAN meeting of 28 February 1923.
{{note}} The ICAN meeting of 28 February 1923 amended Ecuador&#39;s marks to E-U, Haiti&#39;s to H-E, and Uruguay&#39;s marks to C-M. They were restored to their original marks at the following meeting on 26 June.
{{note}} Adopted at the ICAN meeting of 26 June 1923.
{{note}} Amended at the ICAN meeting of 26 June 1923.
{{note}} Adopted at the ICAN meeting of 3 March 1924.
{{note}} Adopted at the ICAN meeting of 14 October 1924.
{{note}} Adopted at the ICAN meeting of 6 April 1925.
{{note}} Adopted at the ICAN meeting of 6 October 1925.
{{note}} Adopted at the ICAN meeting of 3 November 1926.
{{note}} Adopted at the ICAN meeting of 25 April 1927.


See also

Belgium aircraft registration and serials
List of aircraft by tail number
Tail code
Serial number
United Kingdom aircraft registration
United Kingdom military aircraft serials
United States military aircraft serials


References{{More footnotes}}
{{reflist}}
External links{{col-begin}}
{{col-2}}

Searchable worldwide registration database
Aruba Aircraft Register
Australian Aircraft Register
Austrian Aircraft Register
Belgian Aircraft Register
Brazilian Aircraft Register
British Aircraft Register
Canadian Aircraft Register
Danish Aircraft Register
Dutch Aircraft Register
Dutch Historic Aircraft Registers
Finnish Aircraft Register
French  Aircraft Register
Guatemalan Aircraft Register
Indian Aircraft Register
{{col-2}}

International Registry of Mobile Assets, pursuant to the Cape Town Treaty
Irish Aircraft Register
Latvian Aircraft Register
Lebanese Aircraft Register
Luxembourg Aircraft Register
New Zealand Aircraft Register
Norwegian Aircraft Register
Singapore Aircraft Register
South African Aircraft Register
Swedish Aircraft Register
Swiss Aircraft Registry
United States Aircraft Registry
Article 20 of the Convention on International Civil Aviation
Annex 7 to the Convention on International Civil Aviation
Supplement to Annex 7 of the Convention on International Civil Aviation
{{col-end}}{{Aviation lists}}{{DEFAULTSORT:Aircraft Registration}}



ar:تسجيل طائرة
cs:Imatrikulace (letectví)
da:Nationale kendingsbogstaver (fly)
de:Luftfahrzeugkennzeichen
es:Matrícula (aeronaves)
fr:Liste des préfixes OACI d'immatriculation des aéronefs
it:Codice di registrazione degli aeromobili
hu:Lajstromjel
nl:Vliegtuigregistratienummer
ja:機体記号
no:Nasjonale kjennetegn på fly
pl:Międzynarodowy kod samolotowy
pt:Prefixo aeronáutico
ru:Регистрация воздушных судов
simple:Aircraft registration
sv:Luftfartygsregister
zh:航空器註冊編號