title{{nofootnotes}}

{Infobox Ship Image}}
{{Infobox Ship Career}}
{{Infobox Ship Characteristics}} 
{{nihongo}} was one of four Takao-class heavy cruisers, designed to be an improvement over the previous Myōkō-class design. These ships were fast, powerful and heavily armed, with enough firepower to hold their own against any cruiser in any other navy in the world. The Takao-class ships were approved under the 1927 fiscal year budget as part of the Imperial Japanese Navy's strategy of the Decisive Battle, and forming the backbone of a multipurpose long range strike force. Maya was built by the Kawasaki shipyards in Kobe.  It shares its name with the early Japanese gunboat Maya, and per Japanese naval naming conventions, is named after a mountain, specifically Mount Maya outside of Kobe.
History
At the start of the Pacific War, Maya was assigned to support the invasion of the Philippine Islands.  From January through March 1942, the Maya was involved in operations to seize the oil-rich Dutch East Indies. On March&nbsp;3, 1942 the Maya was present at the sinking of the US gunboat USS Asheville (PG-21), south of Java.Returning to Japan in April 1942, Maya was assigned to the unsuccessful pursuit of Admiral William F. Halsey's Task Force 16.2 after the Doolittle Raid on Tokyo, which had damaged the aircraft carrier Ryuho during her conversion. In May–June 1942, she participated in the successful invasion of the Aleutian Islands. In August 1942, the Maya was assigned south, to the reinforcement of the Solomon Islands, and participated in the Battle of the Eastern Solomons. The Maya was based out of Truk through the remainder of 1942. However, during the American bombardment of Henderson Field on November&nbsp;14, 1942, the Maya task force was attacked by the USS Flying Fish (SS-229), which missed the Maya with six torpedoes. Later, the Maya was attacked by Douglas SBD "Dauntless" dive-bombers of VB-10 squadron, one of which dropped a 500-lb (227&nbsp;kg) bomb astern of the ship, but whose starboard wing struck the Maya{{'s}} mainmast; the plane crashed into the ship's portside and ignited 4.7-inch (120&nbsp;mm) shells. Thirty-seven crewmen were killed. The Maya returned to Yokosuka for repairs and refit in January 1943, and was then reassigned to the Northern fleet, supporting supply missions to the Kurile Islands and the Aleutian islands. On March&nbsp;26, 1943, the Maya participated in the Battle of the Komandorski Islands, off Kamchatka Peninsula. Rear Admiral (later Vice Admiral) Charles H. McMorris Task Group 16.6's USS Richmond (CL-9), USS Salt Lake City (CA-25) and four destroyers engaged Vice Admiral Hosogaya's Fifth Fleet cruisers Nachi, Maya, Tama, Abukuma and two destroyers, escorting a convoy with troops and supplies for the isolated garrison on Attu.　The Maya catapulted her spotter aircraft and launched Type 93 "Long Lance" torpedoes, but scored no hits. In a four-hour running gun battle, the Salt Lake City and the USS Bailey (DD-492) were damaged by gunfire. The Maya and the Nachi were also damaged in the exchange of fire and the Japanese were forced to abort their resupply mission.After repairs again at Yokosuka, the Maya returned to the Kuriles in late April, and became flagship of the Fifth Fleet, assisting in the evacuation of Kiska island after the loss of Attu to the Americans in August 1943. After refit in Yokosuka with additional twin-mount Type-96 AA guns (bringing its total to 16 barrels), the Maya accompanied the Chōkai back to Truk, arriving in late September, and started shuttling troops and supplies between Truk and Rabaul. In November, the Maya was attacked by SBD Dauntless dive-bombers from the USS Saratoga (CV-3). A bomb hit the aircraft deck portside above the No.&nbsp;3 engine room and started a major fire. Seventy crewmen were killed. Emergency repairs were made at Rabaul, and the Maya returned to Yokosuka at the end of 1943.At Kure, the Maya embarked two  Aichi E13A1 Jake long-range scout planes, troops and materials. A monkey, donated to the Maya by the Kure Zoo, was also embarked. During the voyage, the aircrew taught the monkey to smartly salute the senior officers, much to their annoyance. From April through June 1944, the Maya supported other units in the defense of the Philippines, culminating in the June&nbsp;19–20, 1944 Battle of the Philippine Sea, in which the Maya was damaged slightly by near-misses. On June&nbsp;20, 1944, the Maya retired with the remnants of the fleet via Okinawa to Yokosuka, where the aircrew and their pet monkey disembarked.  On October 22, 1944, the Battle of Leyte Gulf, Maya was assigned to Cruiser Division 5 with sister ships Atago, Takao and Chōkai. On October&nbsp;23, 1944, Maya was part of the Battle of the Palawan Passage. At 05:33, the Maya’s sister-ships Atago and Takao were torpedoed by the American submarine USS Darter (SS-227). The Atago sank in approximately 18&nbsp;minutes. Twenty minutes later the submarine USS Dace (SS-247) fired six torpedoes at the Maya, mistaking it for a Kongō-class battleship; the Maya was struck by four torpedoes portside: one in the forward chain locker, another opposite No.&nbsp;1 gun turret, a third in No.&nbsp;7 boiler room and the last in the aft engine room. Powerful secondary explosions followed immediately, and by 06:00 Maya was dead in the water and listing heavily to port. She sank five minutes later, taking 336 officers and men to the bottom, including her last captain ({{coord}}).{{cite web}}  The Akishimo rescued 769 men, and transferred them to the  battleship Musashi , which was sunk the following day; 143 of Maya{{'s}} crewmen were lost with Musashi. Thus, from the final crew of 1,105&nbsp;crewmen, 479 were lost.
Armament and equipment notes
From the end of December 1943 through April 1944, the Maya underwent a massive refit. Her No.&nbsp;3 8-inch (200&nbsp;mm) turret was removed. Her twin-mount 25&nbsp;mm AA&nbsp;guns, 4.7-inch (120&nbsp;mm) dual purpose guns, twin torpedo tube mounts and the seaplane hangar were also removed. With substantial weight saved, thirteen Type 96 triple-mount 25&nbsp;mm AA&nbsp;guns and six twin 5-inch (127&nbsp;mm) DP guns were installed, as were nine Type 96 single-mount 25&nbsp;mm AA&nbsp;guns and 36 Type&nbsp;93 13&nbsp;mm MGs. Four Type&nbsp;92 quadruple torpedo mounts, loaded with 16 Type&nbsp;93 "Long Lance" torpedoes, were also fitted, as well as a Type&nbsp;22 surface-search radar. A centerline depth charge rail is installed aft. As a result of this work, the Maya{{'s}} displacement rose to 13,140&nbsp;tons.In a final refit at Yokosuka in July 1944, eighteen more Type&nbsp;96 single-mount 25&nbsp;mm AA&nbsp;guns were installed. This brought the Maya{{'s}} total 25&nbsp;mm suite to 66&nbsp;barrels.
References
Books

{{cite book}}
{{cite book}}
{{cite book}}


External links

{{cite web}}


Notes
{{reflist}}{{Takao class cruiser}}{{DEFAULTSORT:Maya}}







de:Maya (1932)
it:Maya (incrociatore)
ja:摩耶 (重巡洋艦)
pl:Maya (1932)
pt:IJN Maya
ro:Crucișătorul japonez Maya
vi:Maya (tàu tuần dương Nhật)
zh:摩耶號重巡洋艦