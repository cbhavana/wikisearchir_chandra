titleKilling The Goose That Laid the Golden Eggs is among the best known of Aesop's Fables (Perry 87) and use of the phrase has become idiomatic of an unprofitable action motivated by greed.
The story and its moralAvianus and Caxton tell different stories of a goose that lays a golden egg, where other versions have a hen,{{cite web}} as in Townsend: "A cottager and his wife had a Hen that laid a golden egg every day. They supposed that the Hen must contain a great lump of gold in its inside, and in order to get the gold they killed it. Having done so, they found to their surprise that the Hen differed in no respect from their other hens. The foolish pair, thus hoping to become rich all at once, deprived themselves of the gain of which they were assured day by day."{{cite web}}

In early tellings, there is sometimes a commentary warning against greed rather than a pithy moral. This is so in Jean de La Fontaine's fable  of La Poule aux oeufs d'or (Fables V.13),{{cite web}} which begins with the sentiment that 'Greed loses all by striving all to gain' and comments at the end that the story can be applied to those who become poor by trying to outreach themselves. It is only later that the morals most often quoted today began to appear. These are 'Greed oft o’er reaches itself' (Joseph Jacobs, 1894){{cite web}} and 'Much wants more and loses all' (Samuel Croxall, 1722).Available on Google Books, pp.228-9 books.google.co.uk It is notable also that these are stories told of a goose rather than a hen.The English idiom, sometimes shortened to "Killing the golden goose", derives from this fable. It is generally used of a short-sighted action that destroys the profitability of an asset. Caxton's version of the story has the goose's owner demand that it lay two eggs a day; when it replied that it could not, the owner killed it.{{cite web}} The same lesson is taught by Ignacy Krasicki's fable of "The Farmer":



A farmer, bent on doubling the profits from his land,
Proceeded to set his soil a two-harvest demand.
Too intent thus on profit, harm himself he must needs:
Instead of corn, he now reaps corn-cockle and weeds.
There is another variant on the story, recorded by Syntipas (Perry Index 58) and appearing in Roger L'Estrange's 1692 telling as "A Woman and a Fat Hen" (Fable 87): A good Woman had a Hen that laid her every day an Egg. Now she fansy’d to her self, that upon a larger Allowance of Corn, this Hen might be brought in time to lay twice a day. She try’d the Experiment; but the Hen grew fat upon’t, and gave quite over laying. His comment on this is that 'we should set Bounds to our Desires, and content our selves when we are well, for fear of losing what we had.' Another of Aesop's fables with the moral of wanting more and losing everything is The Dog and the Bone.The majority of illustrations of "The Goose that laid the Golden Eggs" picture the farmer despairing after discovering that he has killed the goose to no purpose. This was one of several fables applied to political issues by the American illustrator Thomas Nast. In this case his picture of the baffled farmer, advised by a 'Communistic Statesman', referred to the rail strike of 1877 and appeared in Harpers Weekly for March 16, 1878. Captioned Always killing the goose that lays the golden eggs, the farmer stands for the politically driven union members. His wife and children sorrow in the background.{{cite web}}
Eastern instancesAn Eastern analogue is found in the Suvannahamsa Jataka,{{cite web}} which appears in the fourth section of the Buddhist book of monastic discipline (Vinaya). In this the father of a poor family is reborn as a swan with golden feathers and invites them to pluck and sell a single feather from his wings to support themselves, returning occasionally to allow them another. The greedy mother of the family eventually plucks all the feathers at once, but they then turn to ordinary feathers; when the swan recovers its feathers they too are no longer gold. The moral drawn there is:





Contented be, nor itch for further store.
They seized the swan - but had its gold no more.

North of India, in the formerly Persian territory of Sogdiana, it was the Greek version of the story that was known. Among the 8th century murals in Panjakent, in the western Sugdh province of Tajikistan, there is a panel from room 1, sector 21, representing a series of scenes moving from right to left where it is possible to recognize the same person first in the act of checking a golden egg and later killing the animal in order to get more eggs, only to understand the stupidity of his idea at the very end of the sequence. A local version of the story still persists in the area but ends differently with the main character eventually becoming a king.See Matteo Compareti's description of the murals at vitterhetsakad.se
See also

Jataka tales
Pâté de Foie Gras (Asimov)


References

Aesop's Fables, a new translation by V. S. Vernon Jones (London: W. Heinemann, 1912), p. 2.
{{Reflist}}
External links

15th-20th century book illustrations online

{{DEFAULTSORT:Goose That Laid The Golden Eggs, The}}





he:התרנגולת שהטילה ביצי זהב
ja:ガチョウと黄金の卵