title{{Infobox scientist}}
George Fitzgerald Smoot III (born February 20, 1945) is an American astrophysicist, cosmologist, Nobel laureate, and $1 million TV quiz show prize winner (Are You Smarter Than a 5th Grader?). He won the Nobel Prize in Physics in 2006 for his work on COBE with John C. Mather that led to the measurement "of the black body form and anisotropy of the cosmic microwave background radiation."This work helped further the big-bang theory of the universe using the Cosmic Background Explorer Satellite (COBE).Horgan, J. (1992) Profile: George F. Smoot &ndash; COBE's Cosmic Cartographer, Scientific American 267(1), 34-41. According to the Nobel Prize committee, "the COBE project can also be regarded as the starting point for cosmology as a precision science."{{cite press release}}
Smoot donated his share of the Nobel Prize money, less travel costs, to a charitable foundation.{{cite news}}Currently he is a professor of physics at the University of California, Berkeley, senior scientist at the Lawrence Berkeley National Laboratory, and since 2010, a professor of physics at the Paris Diderot University, France. In 2003, he was awarded the Einstein Medal.
Biography
Early life and educationSmoot was born in Yukon, Florida. He graduated from Upper Arlington High School in Upper Arlington, Ohio, in 1962. He studied mathematics before switching to the Massachusetts Institute of Technology where he obtained dual bachelor's degrees in mathematics and physics in 1966 and a Ph.D. in particle physics in 1970.{{cite press release}}Although Smoot attended MIT, he was not the same Smoot who was laid end to end to measure the Harvard Bridge between Cambridge and Boston;{{cite web}}{{cite web}} this was his cousin Oliver R. Smoot, an MIT alumnus who later served as the chairman of the American National Standards Institute.{{cite web}}
Initial researchGeorge Smoot switched to cosmology and began work at Lawrence Berkeley National Laboratory, collaborating with Luis Walter Alvarez on the HAPPE (High Altitude Particle Physics Experiment), a stratospheric weather balloon designed to detect antimatter in Earth's upper atmosphere, the presence of which was predicted by the now obscure steady state theory of cosmology.He then took up an interest in cosmic microwave background radiation (CMB), previously discovered by Arno Allan Penzias and Robert Woodrow Wilson in 1964. There were, at that time, several open questions about this topic, relating directly to fundamental questions about the structure of the universe. Certain models predicted the universe as a whole was rotating, which would have an effect on the CMB: its temperature would depend on the direction of observation. With the help of Alvarez and Richard A. Muller, Smoot developed a differential radiometer which measured the difference in temperature of the CMB between two directions 60 degrees apart. The instrument, which was mounted on a Lockheed U-2 plane, made it possible to determine that the overall rotation of the universe was zero, which was within the limits of accuracy of the instrument. It did, however, detect a variation in the temperature of the CMB of a different sort. That the CMB appears to be at a higher temperature on one side of the sky than on the opposite side, referred to as a dipole pattern, has been explained as a Doppler effect of the Earth's motion relative to the area of CMB emission, which is called the last scattering surface. Such a Doppler effect arises because the Sun, and in fact the Milky Way as a whole, is not stationary, but rather is moving at nearly 600&nbsp;km/s with respect to the last scattering surface. This is probably due to the gravitational attraction between our galaxy and a concentration of mass like the Great Attractor.
COBE
At that time, the CMB appeared to be perfectly uniform excluding the distortion caused by the Doppler effect as mentioned above. This result contradicted observations of the universe, with various structures such as galaxies and galaxy clusters indicating that the universe was relatively heterogeneous on a small scale. However, these structures formed slowly. Thus, if the universe is heterogeneous today, it would have been heterogeneous at the time of the emission of the CMB as well, and observable today through weak variations in the temperature of the CMB. It was the detection of these anisotropies that Smoot was working on in the late 1970s. He then proposed to NASA a project involving a satellite equipped with a detector that was similar to the one mounted on the U-2 but was more sensitive and not influenced by air pollution. The proposal was accepted and incorporated as one of the instruments of the satellite COBE, which cost US$160 million. COBE was launched on November 18, 1989, after a delay owing to the destruction of the Space Shuttle Challenger. After more than two years of observation and analysis, the COBE research team announced on 23 April 1992 that the satellite had detected tiny fluctuations in the CMB, a breakthrough in the study of the early universe.{{cite journal}} The observations were "evidence for the birth of the universe" and led Smoot to say regarding the importance of his discovery that "if you're religious, it's like looking at God."{{cite news}}{{cite news}}
The success of COBE was the outcome of prodigious teamwork involving more than 1,000 researchers, engineers and other participants. John Mather coordinated the entire process and also had primary responsibility for the experiment that revealed the blackbody form of the CMB measured by COBE. George Smoot had main responsibility for measuring the small variations in the temperature of the radiation.{{cite press release}}Smoot collaborated with San Francisco Chronicle journalist Keay Davidson to write the general-audience book Wrinkles in Time, that chronicled his team's efforts.{{cite book}} In the book The Very First Light,  John Mather and John Boslough complement and broaden the COBE story,{{cite book}} and suggest that George Smoot violated team policy by leaking news of COBE's discoveries to the press before NASA's formal announcement, a leak that, to Mather, smacked of self-promotion and betrayal. Smoot eventually apologized for not following the agreed publicity plan and Mather said tensions eventually eased. Mather acknowledged that "George had brought COBE worldwide publicity" the project might not normally have received.{{cite news}}{{cite news}}
Recent projectsAfter COBE, Smoot took part in another experiment involving a stratospheric balloon, MAXIMA, which had improved angular resolution compared to COBE, and refined the measurements of the anisotropies of the CMB. Smoot has continued CMB observations and analysis and is currently a collaborator on the third generation CMB anisotropy Planck satellite. He is also a collaborator of the design of the Supernova/Acceleration Probe (SNAP), a satellite which is proposed to measure the properties of dark energy.{{cite web}} He has also assisted in analyzing data from the Spitzer Space Telescope in connection with measuring far infrared background radiation.{{cite web}}
Public appearancesShortly after being notified of his Nobel prize, Smoot appeared as the guest conductor of the University of California marching band as it presented a choreographed representation of the Big Bang at halftime of a football game.  He followed his performance with a celebratory ramble through Memorial Stadium, greeted row by row and section by section with waves of applause.  It was referred to as an "only in Berkeley" occasion.{{Citation needed}}  Smoot had a cameo appearance as himself in "The Terminator Decoupling", episode 17 of the second season of The Big Bang Theory. He contacted the show as a fan of their often physics-based plots and was incorporated into an episode featuring him lecturing at a fictional physics symposium.{{cite web}} {{Dead link}}On September 18, 2009, Smoot appeared as the final contestant on the last episode of the Fox television show "Are You Smarter Than a 5th Grader?". He reached the final question, "What U.S. state is home to Acadia National Park?", to which he gave the  correct answer "Maine", becoming the second person to win the one million dollar prize.{{cite web}} {{Dead link}}On December 10, 2009, he appeared in a BBC interview of Nobel laureates, discussing the value science has to offer society.
References{{Reflist}}
Publications

Lubin, P. M. &#38; G. F. Smoot.  "Search for Linear Polarization of the Cosmic Background Radiation", Lawrence Berkeley National Laboratory (LBNL), United States Department of Energy, (Oct. 1978).
Gorenstein, M. V.&#38; G. F. Smoot.  "Large-Angular-Scale Anisotropy in the Cosmic Background Radiation", Lawrence Berkeley National Laboratory (LBNL), United States Department of Energy, (May 1980).
Smoot, G. F., De Amici, G., Friedman, S. D., Witebsky, C., Mandolesi, N., Partridge, R. B., Sironi, G., Danese, L. &#38; G. De Zotti.  "Low Frequency Measurement of the Spectrum of the Cosmic Background Radiation", Lawrence Berkeley National Laboratory (LBNL), United States Department of Energy, (June 1983).
Smoot, G. F., De Amici, G., Levin, S. &#38; C. Witebsky.  "New Measurements of the Cosmic Background Radiation Spectrum", Lawrence Berkeley National Laboratory (LBNL), United States Department of Energy, (Dec. 1984).
Smoot, G., Levin, S. M., Witebsky, C., De Amici, G., Y. Rephaeli.  "An Analysis of Recent Measurements of the Temperature of the Cosmic Microwave Background Radiation", Lawrence Berkeley National Laboratory (LBNL), United States Department of Energy, (July 1987).
Ade, P., Balbi, A., Bock, J., Borrill, J., Boscaleri, A., de Bernardis, P., Ferreira, P. G., Hanany, S., Hristov, V. V., Jaffe, A. H., Lange, A. E., Lee, A. T., Mauskopf, P. D., Netterfield, C. B., Oh, S., Pascale, E., Rabii, B., Richards, P. L., Smoot, G. F., Stompor, R., Winant, C. D. &#38; J. H. P. Wu.  "MAXIMA-1: A Measurement of the Cosmic Microwave Background Anisotropy on Angular Scales of 10' to 5 degrees", Lawrence Berkeley National Laboratory (LBNL), United States Department of Energy, National Aeronautics and Space Administration (NASA), National Science Foundation (NSF), KDI Precision Products, Inc., Particle Physics and Astronomy Research Council UK, (June 4, 2005).
{{cite book}}


External links

George Smoot official website at UC Berkeley
Biography and Bibliographic Resources, from the Office of Scientific and Technical Information, United States Department of Energy
George Smoot biography at the Lawrence Berkeley National Laboratory
George Smoot curriculum vitae at the Lawrence Berkeley National Laboratory
Smoot Group Cosmology official website at the Lawrence Berkeley National Laboratory
Low gravity phase separator U.S. Patent 4027494 at the United States Patent Office
Nobel Prize announcement at the Nobel Foundation
Contemplating the birth of the universe
George Smoot on the design of the universe
George Smoot at the Internet Movie Database
George Smoot chairs the prestigious Eutelsat Star Awards


{{Nobel Prize in Physics Laureates 2001-2025}}
{{2006 Nobel Prize winners}}{{Persondata}}
{{DEFAULTSORT:Smoot, George F.}}








ar:جورج سموت
bn:জর্জ এফ স্মুট
bg:Джордж Смут
ca:George Smoot
cs:George F. Smoot
da:George F. Smoot
de:George F. Smoot
es:George F. Smoot
eo:George Smoot
eu:George Smoot
fa:جرج اسموت
fr:George Fitzgerald Smoot
gl:George Smoot
io:George Fitzgerald Smoot
id:George Smoot
it:George Fitzgerald Smoot
ht:George Smoot
ku:George Smoot
hu:George Smoot
mr:जॉर्ज एफ. स्मूट
ms:George F. Smoot
nl:George Smoot
ja:ジョージ・スムート
no:George F. Smoot
pnb:جارج سموٹ
pl:George F. Smoot
pt:George Fitzgerald Smoot III
ro:George Fitzgerald Smoot
ru:Смут, Джордж
simple:George F. Smoot
sk:George Smoot
sl:George Fitzgerald Smoot III.
fi:George Smoot
sv:George F. Smoot
ta:ஜியார்ஜ் ஸ்மூட்
th:จอร์จ สมูท
tr:George Smoot
uk:Джордж Смут
wuu:George F. Smoot
yo:George Smoot
zh:乔治·斯穆特