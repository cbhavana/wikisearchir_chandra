title
"FCIC" redirects here.
For a division within the Risk Management Agency of the U.S. Department of Agriculture with the same initials, see Federal Crop Insurance Corporation.
For the information department within the United States government's General Services Administration, go to Federal Citizen Information Center.

The Financial Crisis Inquiry Commission (FCIC) is a ten-member commission appointed by the United States government with the goal of investigating the causes of the financial crisis of 2007–2010. The Commission {{cite news}} has been nicknamed the Angelides Commission after the chairman, Phil Angelides. The Commission has been compared to the Pecora Commission, which investigated the causes of the Great Depression in the 1930s, and has been nicknamed the New Pecora Commission. Analogies have also been made to the 9/11 Commission, which examined the September 11 terrorist attacks. The Commission does have the ability to subpoena documents and witnesses for testimony, a power that the Pecora Commission had but the 9/11 Commission did not. The first public hearing of the Commission was held on January 13, 2010, with the presentation of testimony from various banking officials.{{Cite web}} Hearings continued during 2010 with "hundreds" of other persons in business, academia, and government testifying.Bloomberg-Dimon, Blankfein, Mack to TestifyThe Commission reported its findings in January 2011. It concluded that "the crisis was avoidable and was caused by: Widespread failures in financial regulation, including the Federal Reserve’s failure to stem the
tide of toxic mortgages; Dramatic breakdowns in corporate governance including too many financial firms acting
recklessly and taking on too much risk; An explosive mix of excessive borrowing and risk by households and Wall Street that put the financial system on a collision course with crisis; Key policy makers ill prepared for the crisis, lacking a full understanding of the financial system they oversaw; and systemic breaches in accountability and ethics at all levels.“Financial Crisis Inquiry Commission-Press Release-January 27, 2011FCIC Report-Conclusions Excerpt
Creation and statutory mandateThe Commission was created by section 5 of the Fraud Enforcement and Recovery Act of 2009 (Public Law 111-21), signed into law by President Barack Obama on May 20, 2009. That section of the Act:

Set the purpose of the Commission, i.e. &#34;to examine the causes, domestic and global, of the current financial and economic crisis in the United States.&#34;
Set its composition of 10 members, appointed on a bipartisan and bicameral basis in consultation with relevant Committees. Six members are to be chosen by the congressional majority, the Democrats (three of these by the Speaker of the House and three by the Senate Majority Leader) and four by the congressional minority, the Republicans (two from the House Minority Leader and two from the Senate Minority Leader).
Expressed the &#34;sense of the Congress that individuals appointed to the Commission should be prominent United States citizens with national recognition and significant depth of experience in such fields as banking, regulation of markets, taxation, finance, economics, consumer protection, and housing&#34; and also provided that &#34;no member of Congress or officer or employee of the federal government or any state or local government may serve as a member of the Commission.&#34;
Provided that Commission&#39;s chair be selected jointly by the congressional majority leadership and that the vice chair be selected jointly by the congressional minority leadership, and that the chair and vice chair may not be from the same political party.
Set the &#34;functions of the Commission&#34; as:
{{quote}}

Authorized the Commission to &#34;hold hearings, sit and act at times and places, take testimony, receive evidence, and administer oaths&#34; and &#34;require, by subpoena or otherwise, the attendance and testimony of witnesses and the production of books, records, correspondence, memoranda, papers, and documents.&#34; This subpoena power was also held by the Pecora Commission, but not the 9/11 Commission.
Provided that &#34;a report containing the findings and conclusions of the Commission&#34; shall be submitted to the President and to the Congress on December 15, 2010, and that at the discretion of the chairperson of the Commission, the report may include reports or specific findings on any financial institution examined by the Commission.
Provides that the chairperson of the Commission shall, not later than 120 days after the date of submission of the final report,  appear before the Senate Banking Committee and the House Financial Services Committee to testify regarding the Commission&#39;s findings.
Provides for the termination of the Commission 60 days after the submission of the final report.


CompositionSpeaker of the House Nancy Pelosi of California and Senate Majority Leader Harry Reid of Nevada each made three appointments, while House Minority Leader John Boehner of Ohio and Senate Minority Leader Mitch McConnell of Kentucky each made two appointments:

Phil Angelides (chairman) - Pelosi (jointly chosen as chair by Pelosi and Reid)
Bill Thomas (vice chairman) - Boehner (jointly chosen as vice chair by Boehner and McConnell)
Brooksley Born (Pelosi)
Byron Georgiou (Reid)
Bob Graham (Reid)
Keith Hennessey (McConnell)
Douglas Holtz-Eakin (McConnell)
Heather Murren (Reid)
John W. Thompson (Pelosi)
Peter J. Wallison (Boehner)


Commission's investigation and public response{{Prose}}
As part of its inquiry, the Commission will hold a series of public hearings throughout the year including, but not limited to, the following topics: avoiding future catastrophe, complex financial derivatives, credit rating agencies, excess risk and financial speculation, government-sponsored enterprises, the shadow banking system, subprime lending practices and securitization, and too big to fail.The first meeting of the Commission took place in Washington on September 17, 2009, and consisted of opening remarks by Commissioners.On January 13, 2010, Lloyd Blankfein testified before the Commission, that he considered Goldman Sachs' role as primarily that of a market maker, not a creator of the product (i.e.; subprime mortgage-related securities).http://www.npr.org/blogs/money/2010/01/financial_crisis_inquiry_commi.html,retrieved 04/19/2010. Goldman Sachs was sued on April 16, 2010 by the SEC for the fraudulent selling of collateralized debt obligations tied to subprime mortgages, a product which Goldman Sachs had createdhttp://www.businessweek.com/news/2010-04-16/goldman-sachs-sued-by-sec-for-fraud-on-mortgage-backed-cdos.html, retrieved April 19, 2010February 26–27 the Commission heard from academic experts and economists on issues related to the crisis. The following experts have appeared before the Commission in public or in private: Martin Baily, Markus Brunnermeier, John Geanakoplos, Pierre-Olivier Gourinchas, Gary Gorton, Dwight Jaffee, Simon Johnson, Anil Kashyap, Randall Kroszner, Annamaria Lusardi, Chris Mayer, David Moss, Carmen M. Reinhart, Kenneth T. Rosen, Hal S. Scott, Joseph E. Stiglitz, John B. Taylor, Mark Zandi and Luigi Zingales.April 7–9, 2010, Alan Greenspan, Chuck Prince and Robert Rubin testified before the Commission on subprime lending and securitization.May 5–6, former Bear Stearns CEO Jimmy Cayne, former SEC Chairman Christopher Cox, Tim Geithner and Hank Paulson are scheduled to appear before the Commission.Writer Joe Nocera of the New York Times praised the commission's approach and technical expertise in understanding complex financial issues during July 2010.New York Times-Joe Nocera-Hearings that Aren't Just Theater-July 2010July 27, The composition of the commission has changed several times since its formation.  The executive director J. Thomas Greene was replaced by Wendy M. Edelberg, an economist from the Federal Reserve.  Five of the initial fourteen senior staff members resigned, including Matt Cooper, a journalist who was writing the report.  Darrell Issa, a top Republican on the House Oversight and Government Reform Committee, questioned the Federal Reserve's involvement as a possible conflict of interest, and there has been disagreement among some commission members on what information to make public and where to place blame. Mr. Angelides called the criticisms "silly, stupid Washington stuff," adding: "I don’t know what Mr. Issa’s agenda is, but I can tell you what ours is."  In a joint interview the commission’s chairman, Phil Angelides (D), and vice chairman, Bill Thomas (R), said that the turnover’s effects had been exaggerated and that they were optimistic about a consensus.Staff Losses and Dissent May Hurt Crisis Panel; New York Times; September 2, 2010
ReportThe Commission's final report was initially due to Congress on December 15, 2010, but was not released until January 27, 2011.Harsh Words for Regulators in Crisis Commission Report; New York Times; January 27, 2011 The Commission concluded that "the
crisis was avoidable and was caused by: Widespread failures in financial regulation, including the Federal Reserve’s failure to stem the
tide of toxic mortgages; Dramatic breakdowns in corporate governance including too many financial firms acting
recklessly and taking on too much risk; An explosive mix of excessive borrowing and risk by households and Wall Street that put the
financial system on a collision course with crisis; Key policy makers ill prepared for the crisis, lacking a full understanding of the financial system
they oversaw; and systemic breaches in accountability and ethics at all levels.“FCIC Report-Conclusions Excerpt-January 2011  A dissent by Peter Wallison of the American Enterprise Institute claimed that the crisis was caused by government affordable housing policies rather than market forces.  However, Wallison's views have not been supported by subsequent detailed analyses of mortgage market data.Michael Simkovic, Competition and Crisis in Mortgage Securitization
References{{Reflist}}
External links

Financial Crisis Inquiry Commission Website
Official live streaming video of the proceedings of the Financial Crisis Inquiry Commission
Profiles and photos of commissioners
Testimony of Alan Greenspan - Financial Crisis Inquiry Commission - Wednesday, April 7, 2010
{{NYTtopic}}


Commission Reports

FCIC Report-Conclusions Excerpt-January 2011
FCIC Report-Full Text-January 2011







