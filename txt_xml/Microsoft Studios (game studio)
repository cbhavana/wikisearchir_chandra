title{{Use mdy dates}}
{{Infobox company}}Microsoft Studios is the video game production wing for Microsoft, responsible for the development and publishing of games for the Xbox, Xbox 360, Games for Windows and Windows Phone platforms. They were established in 2002 as Microsoft Game Studios to coincide with the release of the Xbox, before being re-branded in 2011. Microsoft Studios develops and publishes games in conjunction with first and third party development studios under their publishing label.
History
2002

Microsoft Studios acquired Rare Ltd. from Nintendo and the Stamper brothers (Chris and Tim), who owned 49% and 51% of the company respectively. The buyout is believed to be one of the most expensive purchases of a video game development studio ever, at a cost of around {{US$}}.{{Cite web}} As a result of the buyout, Microsoft Studios also acquired the rights to Rare&#39;s original IPs, including the Banjo-Kazooie, Conker and Perfect Dark franchises.


2006

Microsoft Studios announced that they have purchased Lionhead Studios, known for developing critically acclaimed titles such as Black & White and Fable.{{Cite web}}
Microsoft Studios acquired Massive Incorporated, an in-game advertising company to provide additional revenues from their gaming platforms.{{Cite web}}


2007

Microsoft Studios announced the opening of a European office in Reading, England, headed by General manager Phil Spencer.{{cite web}}
Microsoft Studios disbanded FASA Studio, best known for their work on the MechWarrior series.
Halo developers Bungie announced that they would split from Microsoft Studios in order become a privately held independent company.


2008

Microsoft Studios disbanded casual games studio Carbonated Games.
Microsoft Studios announced the formation of Xbox Live Productions to develop &#34;high-quality digital content&#34; for Xbox Live Arcade.{{Cite web}}


2009

Microsoft Studios announced the closure of both Ensemble Studios and Aces Studio, due to the effects of the late-2000s financial crisis and the restructuring of their game development studios.
Phil Spencer was promoted to Corporate Vice President of Microsoft Studios in order to replace the retiring Shane Kim.{{cite web}}
Microsoft Studios formed Good Science Studio to develop software for Kinect.


2010

Microsoft Studios formed a Mobile Gaming studio focused on developing gaming and entertainment multimedia for Windows Phone.{{cite web}}
Microsoft Studios forms new internal studio named Microsoft Flight Development Team to bring Microsoft Flight to PC.{{cite web|url=http://www.microsoft.com/games/en-us/community/pages/gamescom.aspx|title=MGS bring Microsoft Flight to PC|


2011

Microsoft Studios opened new development studios in London, Redmond, Victoria, and Virginia.{{cite web}}{{cite web}}{{cite web}}{{cite web}}
Microsoft Studios acquired indie game developer Twisted Pixel Games, known for developing titles including 'Splosion Man and The Gunstringer.http://majornelson.com/2011/10/12/welcome-twisted-pixel-to-microsoft-studios/http://www.joystiq.com/2011/10/12/microsoft-buys-indie-developer-twisted-pixel/
Microsoft Studios recruits for new development studios in Redmond — Microsoft Studios LEAP and Platform Next Studios.{{cite web}}{{cite web}}


Software development studios

First Party Third Party Former/Defunct 


United States


343 Industries – Halo series
Good Science Studio – Kinect Adventures, Kinect Fun Labs
Kids and Lifestyle Entertainment – Untitled Project
MGS Family – Untitled Project
MGS Mobile Gaming – ilomilo
Microsoft Studios LEAP – Untitled Project
Platform Next Studios – Untitled Project
Turn 10 Studios – Forza Motorsport series
Twisted Pixel Games – The Gunstringer, 'Splosion Man, Ms. Splosion Man, The Maw, Comic Jumper: The Adventures of Captain Smiley
Xbox Live Productions – South Park Let's Go Tower Defense Play! ,South Park: Tenorman's Revenge


Europe


Lionhead Studios – Fable series, Black & White series
MGS Soho – Untitled Project
Rare Ltd. – Kinect Sports, Banjo-Kazooie series, Viva Piñata, Perfect Dark series


Canada


BigPark – Kinect Joy Ride
MGS Vancouver – Untitled Project
MGS Victoria – Untitled Project


Other


Microsoft Flight Development Team – Microsoft Flight
 

4J Studios – Banjo-Kazooie, Banjo-Tooie, Minecraft, Perfect Dark (Xbox Live Arcade ports)
Bongfish GmbH – Harms Way
Certain Affinity – Crimson Alliance, Halo 2 (Map packs), Halo Reach (Map packs), Halo: Combat Evolved Anniversary (Map packs), Halo Waypoint
Climax Group – Sudeki, Bloodforge
Crytek – Ryse
Double Fine – Iron Brigade, Happy Action Theater
Epic Games – Gears of War series, Shadow Complex
Frontier Developments – Kinectimals, Kinect Disneyland Adventures
Ftoni Studios – Untitled Project
FunTown World Limited – FunTown Mahjong
Gaia Industries – Street Trace NYC
Gastronaut Studios – Small Arms, Gel: Set & Match
Gas Powered Games – Age of Empires Online
Grasshopper Manufacture – Diabolical Pitch, Sine Mora
Grounding Inc. – Project Draco
Harmonix – Dance Central series
Hidden Path Entertainment – Wits and Wagers
iNiS – Lips series
Interserv International Inc – Word Puzzle
Krome Studios – Full House Poker, Scene It? Box Office Smash, Game Room, Viva Piñata: Party Animals
Little Boy Games – Go! Go! Break Steady
Luma Arcade – The Harvest
Mistwalker – Lost Odyssey
Naked Sky Entertainment – RoboBlitz
NanaOn-Sha – Haunt
NinjaBee – Band of Bugs, A Kingdom for Keflings, A World of Keflings, Cloning Clyde, Dash of Destruction, Outpost Kaloki X
Q Entertainment – Ninety-Nine Nights series
Remedy Entertainment – Alan Wake series
Robot Entertainment – Orcs Must Die!
Ruffian Games – Crackdown series
Runic Games – Torchlight series
Sarbakan – Lazy Raiders
Screenlife / WXP – Scene It? Lights, Camera, Action
Shadow Planet Productions – Insanely Twisted Shadow Planet
Signal Studios – Toy Soldiers
Singapore-MIT GAMBIT Game Lab – CarneyVale: Showtime
Ska Studios – The Dishwasher series
Slapdash Games – TiQal
Slick Entertainment – Scrap Metal
Smartbomb Interactive – Snoopy Flying Ace
SouthEnd Interactive – Lode Runner 2009, ilomilo
Starfire Studios – Fusion: Genesis
Strange Flavour – Totemball
Tequila Works - Deadlight
TikGames - Texas Hold 'em 
Torpex Games - Schizoid
Undead Labs – Untitled Xbox Live Arcade Project
Vector Unit – Hydro Thunder Hurricane
Zen Studios – Pinball FX series
 

Aces Studio – Microsoft Flight Simulator series
Artoon – Blinx series , Blue Dragon
Beep Industries – Voodoo Vince
BioWare – Mass Effect
Bizarre Creations – Project Gotham Racing series
Bungie – Halo series
Carbonated Games – Hexic series
Digital Anvil – Brute Force, Freelancer
Ensemble Studios – Age of Empires series
FASA Studio – MechWarrior series
feelplus – Lost Odyssey
Fuel Industries – Tinker
Hired Gun – Halo 2 for Windows Vista
Indie Built – Amped series
Ninja Theory – Kung Fu Chaos
Turbine – Asheron's Call
 

Owned franchises and properties



Aegis Wing
Age of Empires
Age of Mythology
Azurik
Banjo-Kazooie
BattleTech
Black & White
Blast Corps
Blinx
Bloodforge
Blood Wake
Blue Dragon
Brute Force
Combat Flight Simulator WWII Europe
Comic Jumper: The Adventures of Captain Smiley
Conker
Crackdown
Crimson Alliance
Crimson Skies
Fable
Forza Motorsport
Freelancer
Fusion Genesis
Fuzion Frenzy
Game Chest Series
 

Grabbed by the Ghoulies
Halo
Haunt
Hexic
Hydro Thunder
ilomilo
Iron Brigade
Infinite Undiscovery
It's Mr. Pants
Jawbreaker
Jetpac
Jet Force Gemini
Joy Ride
Kakuto Chojin
Kameo
Killer Instinct
Kinect Adventures
Kinect Fun Labs
Kinect Sports
Kodu
Kung Fu Chaos
Lips
Lost Odyssey
Magatama
Maximum Chase
MechAssault
MechCommander
MechWarrior
 

Microsoft Flight
Microsoft Flight Simulator
Microsoft Train Simulator
Microsoft Space Simulator
Midtown Madness
Milo and Kate
Monster Truck Madness
Motocross Madness
Ms. Splosion Man
Nightcaster
Ninety-Nine Nights
N.U.D.E.@Natural Ultimate Digital Experiment
Perfect Dark
Phantom Dust
Project Gotham Racing
Quantum Redshift
Rallisport Challenge
Rise of Nations
Sabreman
Shadowrun
Sneakers
'Splosion Man
Starlancer
Sudeki
Tao Feng: Fist of the Lotus
 

The Gunstringer
The Harvest
The Maw
The Movies
Tork: Prehistoric Punk
Toy Soldiers
True Fantasy Live Online
Viva Piñata
Voodoo Vince
Whacked!
XNA
Zoo Tycoon
 

See also

List of video games published by Microsoft Studios
List of video games published by Nintendo
List of video games published by Sony Computer Entertainment


References{{Reflist}}
External links

{{Official}}
Microsoft's Official Xbox website

{{Xbox}}
{{Microsoft}}
{{Microsoft Studios (game studio)}}
{{XBLA Summer of Arcade}}





ar:مايكروسوفت جيم ستوديوز
ca:Microsoft Game Studios
cs:Microsoft Studios
de:Microsoft Game Studios
es:Microsoft Game Studios
fa:استودیو مایکروسافت
fr:Microsoft Game Studios
ko:마이크로소프트 게임 스튜디오
id:Microsoft Game Studios
lt:Microsoft Studios
hu:Microsoft Studios
nl:Microsoft Game Studios
no:Microsoft Game Studios
pt:Microsoft Game Studios
ro:Microsoft Game Studios
ru:Microsoft Game Studios
fi:Microsoft Game Studios
sv:Microsoft Game Studios
th:ไมโครซอฟท์เกมสตูดิโอส์
zh:微软游戏工作室