titleDNA origami is the nanoscale folding of DNA to create arbitrary two and three dimensional shapes at the nanoscale. The specificity of the interactions between complementary base pairs make DNA a useful construction material through design of its base sequences. Developed by Paul Rothemund at the California Institute of Technology, the process involves the folding of a long single strand of viral DNA aided by multiple smaller "staple" strands. These shorter strands bind the longer in various places, resulting in various shapes including a smiley face and a coarse map of China and the Americas, along with many three dimensional structures such as cubes. To produce a desired shape, images are drawn with a raster fill of a single long DNA molecule. This design is then fed into a computer program which calculates the placement of individual staple strands. Each staple binds to a specific region of the DNA template, and thus due to Watson-Crick base pairing the necessary sequences of all staple strands are known and displayed. The DNA is mixed and then heated and cooled. As the DNA cools the various staples pull the long strand into the desired shape. Designs are directly observable via several methods including atomic force microscopy, or fluorescence microscopy when DNA is coupled to fluorescent materials .Bottom-up self assembly methods are considered promising alternatives that offer cheap, parallel synthesis of nanostructures under relatively mild conditions. Many potential applications have been suggested in the literature, including enzyme immobilization, drug carry capsules, and nanotechnological self-assembly of materials. Though DNA is not the natural choice for building active structures for nanorobotic applications due to its lack of structural and catalytic versatility, several papers have examined the possibility of molecular walkers on origami and switches for algorithmic computing. DNA origami was the cover story of Nature on March 16, 2006.
See also

DNA nanotechnology
Molecular self-assembly
Folding@home


References

{{cite journal}}
{{cite journal}}
DNA 'organises itself' on silicon – BBC News 2009-08-17

{{Biochem-stub}}
{{nano-tech-stub}}
ar:أوريغامي الحمض النووي