title{{Infobox Film Movement}}
New Hollywood or post-classical Hollywood, sometimes referred to as the "American New Wave", refers to the time from roughly the late-1960s (Bonnie and Clyde, The Graduate) to the early 1980s (Heaven's Gate, One from the Heart) when a new generation of young filmmakers came to prominence in America, influencing the types of films produced, their production and marketing, and impacted the way major studios approached filmmaking. The films they made were part of the studio system, and these individuals were not "independent filmmakers", but they introduced subject matter and styles that set them apart from the studio traditions. New Hollywood has also been defined as a broader filmmaking movement influenced by this period, which has been called the “Hollywood renaissance”.Geoff King, New Hollywood Cinema, I.B Tauris, London, 2002. pp.1-4
Background and overviewFollowing the Paramount Case and the advent of television, both of which severely weakened the traditional studio system, Hollywood studios initially used spectacle to retain profitability. Technicolor became used far more frequently, and widescreen processes and technical improvements, such as Cinemascope, stereo sound and others such as 3-D, were invented in order to retain the dwindling audience and compete with television, but were generally not successful in increasing profits.David E James,  Allegories of Cinema, American film in the Sixties, Princeton University Press, New York, 1989, pp.14-26The 1950s and early 60s saw a Hollywood dominated by musicals, historical epics, and other films that benefited from the larger screens, wider framing and improved sound. However, audience share continued to dwindle, and by the mid-1960s had reached alarmingly low levels. Several costly flops, including Cleopatra, Tora, Tora, Tora, and Hello, Dolly!, and failed attempts to imitate the success of My Fair Lady and The Sound of Music, put great strain on the studios.Thomas Schatz, “The New Hollywood”, in Film Theory goes to the Movies, by Jim Collins et al (eds.) Routledge,  New York, 1993, pp.15-20By the time the baby boomer generation was coming of age in the 1960s, 'Old Hollywood' was rapidly losing money; the studios were unsure how to react to the much changed audience demographics. The marked change during the period was from a middle aged high school educated audience in the mid 60s, to a younger, college-educated, more affluent one; by the mid 70s, 76% of all movie-goers were under 30, and 64% had gone to college.John Belton, American Cinema/American Culture, McGraw/Hill, New York, 1993, p.290
European art films (especially the Commedia all'italiana, the French New Wave, and the Spaghetti Western) and Japanese cinema were making a splash in America — the huge market of disaffected youth seemed to find relevance and artisic meaning in movies like Michelangelo Antonioni's Blowup, with its oblique narrative structure and full-frontal female nudity.David A Cook, “Auteur Cinema and the film generation in 70s Hollywood”, in The New American Cinema by Jon Lewis (ed), Duke University Press, New York, 1998, pp.1-4Stefan Kanfer, The Shock of Freedom in Films, Time Magazine, Dec 8 1967,  Accessed 25 April 2009, http://www.time.com/time/magazine/article/0,9171,844256-7,00.html The desperation felt by studios during this period of economic downturn, and after the losses from expensive movie flops,  led to innovation and risk taking through allowing greater control by younger directors and producers.Tomas Schatz Ibid pp.14-16  Therefore, in an attempt to capture that audience which found a connection to the “art films” of Europe, the Studios hired a host of young filmmakers (many of whom were mentored by Roger Corman) and allowed them to make their films with relatively little studio control. This, together with the breakdown of the Production Code in 1966 and the new ratings system in 1968 (reflecting growing market segmentation) set the scene for New Hollywood.Schatz ibid
Characteristics of the New Hollywood filmsThis new generation of Hollywood filmmaker was film school-educated, counterculture-bred, and, most importantly from the point of view of the studios, young, and therefore able to reach the youth audience they were losing. This group of young filmmakers — actors, writers and directors — dubbed the New Hollywood by the press, briefly changed the business from the producer-driven Hollywood system of the past, and injected movies with a jolt of freshness, energy, sexuality, and a passion for the artistic value of film itself. Todd Berliner has written about the period's unusual narrative practices. The 1970s, Berliner says, marks Hollywood’s most significant formal transformation since the conversion to sound film and is the defining period separating the storytelling modes of the studio era and contemporary Hollywood. Seventies films deviate from classical narrative norms more than Hollywood films from other eras. Their narrative and stylistic devices threaten to derail an otherwise straightforward narration. Berliner argues that five principles govern the narrative strategies characteristic of Hollywood films of the 1970s:

1. Seventies films show a perverse tendency to integrate, in narratively incidental ways, story information and stylistic devices counterproductive to the films’ overt and essential narrative purposes.
2. Hollywood filmmakers of the 1970s often situate their filmmaking practices in between those of classical Hollywood and those of European and Asian art cinema. 
3. Seventies films prompt spectator responses more uncertain and discomforting than those of more typical Hollywood cinema.
4. Seventies narratives place an uncommon emphasis on irresolution, particularly at the moment of climax or in epilogues, when more conventional Hollywood movies busy themselves tying up loose ends.
5. Seventies cinema hinders narrative linearity and momentum and scuttles its potential to generate suspense and excitement.Todd Berliner,  Hollywood Incoherent: Narration in Seventies Cinema, University of Texas Press, 2010, pp. 51-52

Technically, the greatest change the New Hollywood filmmakers brought to the art form was an emphasis on realism. This was possible when the Motion Picture Association of America film rating system was introduced and location shooting was becoming more viable. Because of breakthroughs in film technology, the New Hollywood filmmakers could shoot 35mm camera film in exteriors with relative ease. Since location shooting was cheaper (no sets need be built) New Hollywood filmmakers rapidly developed the taste for location shooting, which had the effect of heightening the realism and immersion of their films, especially when compared to the artificiality of previous musicals and spectacles.  The use of editing to artistic effect was also an important factor in New Hollywood cinema, e.g. Easy Rider’s use of editing to foreshadow the climax of the movie, as well as subtler uses, such as editing to reflect the feeling of frustration in Bonnie and Clyde and the subjectivity of the protagonist in The Graduate.Paul Monaco,  The Sixties, 1960-69, History of American Cinema, University of California Press, London, 2001, p.183  Aside from realism, New Hollywood films often featured anti-establishment political themes, use of rock music, and sexual freedom deemed "counter-cultural" by the studios. Furthermore, many figures of the period openly admit to using drugs such as LSD and marijuana. The popularity of these films with young people shows the importance of these thematic elements and artistic values with a more cinematically knowledgeable audience.Schatz pp.12-22 The youth movement of the 1960s turned anti-heroes like Bonnie and Clyde and Cool Hand Luke into pop culture heroes, and Life magazine called the characters in Easy Rider "part of the fundamental myth central to the counterculture of the late 1960s."Monaco, pp.182-188) Easy Rider also had an impact on the way studios looked to reach the youth market.Monaco Ibid The success of Midnight Cowboy in spite of  its X rating was evidence for the interest in controversial themes at the time and also showed the weakness of the rating system and segmentation of the audience.Belton, Op Cit, p.288
Bonnie and ClydePerhaps the most significant film for the New Hollywood generation was Bonnie and Clyde in 1967. Produced by and starring Warren Beatty, its mix of graphic violence, sex and humor as well as its theme of glamorous disaffected youth was a hit with audiences, and received Academy Awards for Best Supporting Actress and Best Cinematography.  Its portrayal of violence and ambiguity in regard to moral values, and ‘shock’ ending, divided critics. Following a negative review, Time magazine received letters from fans of the movie, and according to journalist Peter Biskind, the impact of critic Pauline Kael in her positive review of the film (October 1967 New Yorker) led other reviewers to follow her lead and re-evaluate the film (notably Newsweek and Time).Peter Biskind, Easy Riders, Raging Bulls,  Bloomsbury, London, 1998, pp.40-47 Kael drew attention to the innocence of the characters in film and the artist merit of the contrast with the violence in the film: “In a sense, it is the absence of sadism — it is the violence without sadism — that throws the audience off balance at Bonnie and Clyde. The brutality that comes out of this innocence is far more shocking than the calculated brutalities of mean killers”. Kael also noted the reaction of audiences to the violent climax of the movie, and the potential to empathise with the gang of criminals in terms of their naiveté and innocence reflecting a change in expectations of American cinema.Pauline Kael, "Bonnie and Clyde" in, Pauline Kael, For Keeps (Plume, New York, 1994) pp. 141-57. Originally published in The New Yorker, October 21, 1967 The cover story in Time Magazine in December 1967, celebrated the movie and innovation in American New wave cinema. This influential article by Stefan Kanfer claimed that Bonnie and Clyde represented a "New Cinema" through its blurred genre lines, and disregard for honoured aspects of plot and motivation, and that “In both conception and execution, Bonnie and Clyde is a watershed picture, the kind that signals a new style, a new trend.” Biskind states that this review and turnaround by some critics allowed the film to be rereleased thus proving its commercial success and reflecting the move to New Hollywood.Biskind Ibid The impact of this film is important in understanding the rest of the American New Wave, as well as the conditions that were necessary for it.  These initial successes paved the way for the studio to relinquish almost complete control to these innovative young filmmakers. In the mid-1970s, idiosyncratic, startling original films such as Paper Moon, Dog Day Afternoon, Chinatown and Taxi Driver among others (see below), enjoyed enormous critical and commercial success. These successes by the members of New Hollywood led each of them in turn to make more and more extravagant demands, both on the studio and eventually on the audience.
The end of the New Hollywood eraIn retrospect, Jaws (1975) and Star Wars (1977) marked the beginning of the end for the New Hollywood era. With their unprecedented box-office successes, Steven Spielberg's and George Lucas's films jumpstarted Hollywood's blockbuster mentality, giving studios a new paradigm of how to make money in the changing commercial landscape. The focus on high-concept premises, with greater concentration on tie-in merchandise (such as toys), spin-offs into other media (such as soundtracks), and the use of sequels (which had been made more respectable by Coppola's The Godfather Part II), all showed the studios how to make money in the new environment.On realizing how much money could potentially be made in films, major corporations started buying up the Hollywood studios. The corporate mentality these companies brought to the filmmaking business would slowly squeeze out the more idiosyncratic of these young filmmakers, while ensconcing the more malleable and commercially successful of them.The New Hollywood's ultimate demise came after a string of box office failures that many critics viewed as self-indulgent and excessive. Directors had enjoyed unprecedented creative control and budgets during the New Hollywood era, but such expensive flops as At Long Last Love, New York, New York, and Sorcerer caused the studios to increase their control over production.New Hollywood excess culminated in two unmitigated financial disasters: Michael Cimino's Heaven's Gate (1980) and Francis Ford Coppola's One from the Heart (1982). After astronomical cost overruns stemming from Cimino's demands, Heaven's Gate only earned $3.5 million in box-office sales after costing $44 million to produce.  The loss was so great that it forced United Artists into bankruptcy, resulting in its sale to MGM. Coppola, having flourished after the near financial disaster of Apocalypse Now, plowed all of the enormous success of that film into American Zoetrope, effectively becoming his own studio head. As such, he bet it all on One from the Heart, which only took in $636,000--barely a fraction of the $26 million it cost to make.  The film lasted only a week in theaters, bankrupting Coppola and his fledgling studio. (Following the box-office disaster, Hollywood wags started referring to the picture as "One Through the Heart".)These two costly examples, as well as the above-mentioned box-office failures, coupled with the new commercial paradigm of Jaws and Star Wars gave studios a clear and renewed sense of where the market was going: high-concept, mass-audience, wide-release films. Therefore, the costly and risky strategy of surrendering control to the director ended, and with that, the New Hollywood era.
Interpretations on defining New HollywoodFor Peter Biskind, the new wave marked a significant shift towards independently produced and innovative works by a new wave of directors, that significantly changed after Jaws, Star Wars and the realization by studios of the importance of blockbusters, advertising and control over production.Biskind, Op cit, p.288Writing in 1968, critic Pauline Kael argued that the importance of The Graduate was in its social significance in relation to a new young audience, and the role of mass media, rather than any artistic aspects. Kael argued that college students identifying with the graduate were not too different from audiences identifying with characters in dramas of previous decade.Pauline Kael, “Trash, Art, and the Movies” in Going Steady, Film Writings 1968-69, Marion Boyers, New York, 1994, pp.125-7John Belton points to the changing demographic to even younger, more conservative audiences in the mid 70s (50% aged 12–20) and the move to less politically subversive themes in mainstream cinema.Belton, Ibid, pp.292-296Thomas Schatz sees the mid to late 1970s as the decline of the art cinema movement as a significant industry force with its peak in 1974-75 with Nashville and Chinatown.Schatz, Ibid, p.20Geoff King sees the period as an interim movement in American cinema where a conjunction of forces lead to a measure of freedom in filmmaking.King, Ibid, p.48Todd Berliner says that seventies cinema resists the efficiency and harmony that normally characterize classical Hollywood cinema and tests the limits of Hollywood's classical model.Berliner, Ibid.
List of important figures in the New Hollywood eraMany of the filmmakers listed below did multiple chores on various film productions through their careers. They are here listed by the category they are most readily recognized as.
Writers and directorsThe issue of whether or not a specific director belongs to the "New Hollywood" generation is a difficult one to address. Many of those listed below made either their only films or their most successful films (Bogdanovich or Hal Ashby) in this period. Others, such as Martin Scorsese or John Boorman, have continued to make acclaimed and successful films. Aside from this, however, "membership" of the New Generation is a blurred line. Initially, thus, many of these filmmakers' earlier productions (famously Kubrick's Spartacus (1960) and Lumet's 12 Angry Men (1957) did not play a part in informing the New Generation zeitgeist as, say, Nashville (1975) or Midnight Cowboy (1969) did.{{columns}}
Cinematographers, editors, and production designers{{col-begin}}
{{col-2}}

Dede Allen
Bill Butler
Caleb Deschanel
Verna Fields
Conrad Hall
László Kovács
Marcia Lucas
Walter Murch
{{col-2}}

Polly Platt
Toby Carr Rafelson
Thelma Schoonmaker
Paul Sylbert
Richard Sylbert
Dean Tavoularis
Haskell Wexler
Gordon Willis
Vilmos Zsigmond
{{col-end}}
Producers and executives{{col-begin}}
{{col-2}}

Steven Bach
Peter Bart
David Begelman
Stephen Blauner
Charlie Bluhdorn
David Brown
John Calley
Robert Chartoff
Roger Corman: Though emphatically and self-consciously not a member of the New Hollywood generation, he started the careers of many of them.
Barry Diller
Robert Evans
Gary Kurtz
Stanley Jaffe
Arthur Krim
Alan Ladd, Jr.
Julia Phillips
Michael Phillips
Fred Roos
Bert Schneider
Harold Schneider
Sidney Sheinberg
Jules Stein
Ned Tanen
Lew Wasserman
Fred Weintraub
Irwin Winkler
Saul Zaentz
Richard D. Zanuck
{{col-2}}
{{col-end}}
Actors{{columns}}
Others

Pauline Kael, movie critic
Sue Mengers, agent


List of notable New Hollywood filmsThe following is a chronological list of those films from the New Hollywood period that are generally considered to be seminal or notable.{{columns}}
See also

Cult classic


References{{Reflist}}
BibliographyBiskind, Peter (1998). Easy Riders, Raging Bulls: How the Sex-Drugs-And Rock 'N Roll Generation Saved Hollywood (Simon and Schuster) ISBN 978-0684857084.Biskind, Peter (1990). The Godfather Companion: Everything You Ever Wanted To Know About All Three Godfather Films (HarperPerennial)Belton, John, American Cinema/American Culture, McGraw/Hill, New York, 1993Berliner, Todd, Hollywood Incoherent: Narration in Seventies Cinema, Austin: University of Texas Press, 2010.Cook, David A, “Auteur Cinema and the film generation in 70s Hollywood”, in The New American Cinema by Jon Lewis (ed), Duke University Press, New York, 1998, pp.&nbsp;1–37James, David E,  Allegories of Cinema, American film in the Sixties, Princeton University Press, New York, 1989, pp.&nbsp;1–42Kael, Pauline “Bonnie and Clyde” in, Pauline Kael, For Keeps (Plume, New York, 1994) pp.&nbsp;141–57.Kael, Pauline, “Trash, Art, and the Movies” in Going Steady, Film Writings 1968-69, Marion Boyers, New York, 1994, pp87–129Kanfer, Stefan, The Shock of Freedom in Films, Time Magazine, Dec 8 1967,  Accessed 25 April 2009, http://www.time.com/time/magazine/article/0,9171,844256-7,00.htmlKing, Geoff,  New Hollywood Cinema: An Introduction, I.B Tauris, London, 2002. pp.&nbsp;1–49Monaco, Paul, The Sixties, 1960-69, History of American Cinema, University of California Press, London, 2001, pp.&nbsp;182–188Schatz, Thomas “The New Hollywood”, in Film Theory goes to the Movies, by Jim Collins, Hilary Radner and Ava Preacher Collins(eds.) Routledge,  New York, 1993 pp.&nbsp;8–37
External links

{{IMDb title}}






{{Link GA}}ca:New Hollywood
de:New Hollywood
es:Nuevo Hollywood
fa:هالیوود نو
fr:Nouvel Hollywood
it:Nuova Hollywood
nl:New Hollywood
ja:アメリカン・ニューシネマ
ru:Новый Голливуд
vi:New Hollywood
zh:美國新好萊塢電影