title{{Infobox software}}
{{Infobox software}}
Microsoft Office Word is a word processor designed by Microsoft. It was first released in 1983 under the name Multi-Tool Word for Xenix systems.{{Cite book}}{{cite web}}{{cite web}} Subsequent versions were later written for several other platforms including IBM PCs running DOS (1983), the Apple Macintosh (1984), the AT&T Unix PC (1985), Atari ST (1986), SCO UNIX, OS/2, and Microsoft Windows (1989). It is a component of the Microsoft Office software system; it is also sold as a standalone product and included in Microsoft Works Suite. The current versions are Microsoft Office Word 2010 for Windows and Microsoft Office Word 2011 for Mac.
History{{Main}}
Origins and growth: 1981 to 1995In 1981, Microsoft hired Charles Simonyi, the primary developer of Bravo, the first GUI word processor, which was developed at Xerox PARC.{{cite news}} Simonyi started work on a word processor called Multi-Tool Word and soon hired Richard Brodie, a former Xerox intern, who became the primary software engineer.{{Cite book}}{{cite web}}
Microsoft announced Multi-Tool Word for Xenix and MS-DOS in 1983.{{cite news}} Its name was soon simplified to Microsoft Word. Free demonstration copies of the application were bundled with the November 1983 issue of PC World, making it the first program to be distributed on-disk with a magazine.{{Cite news}} Unlike most MS-DOS programs at the time, Microsoft Word was designed to be used with a mouse, and it was able to display some formatting, such as bold, italic, and underlined text, although it could not render fonts. It was not initially popular, since its user interface was different from the leading word processor at the time, WordStar.{{cite book}}  However, Microsoft steadily improved the product, releasing versions 2.0 through 5.0 over the next six years.

In 1985, Microsoft ported Word to the Macintosh. This was made easier by the fact that Word for DOS has been designed for use with high-resolution displays and laser printers, even though none were yet available to the general public.{{cite news}} Following the precedents of LisaWrite and MacWrite, Word for Mac added true WYSIWYG features. After its release, Word for Mac's sales were higher than its MS-DOS counterpart for at least four years.The second release of Word for Macintosh, shipped in 1987, was named Word 3.0 to synchronize its version number with Word for DOS; this was Microsoft's first attempt to synchronize version numbers across platforms. Word 3.0 included numerous internal enhancements and new features, including the first implementation of the Rich Text Format (RTF) specification, but was plagued with bugs. Within a few months Word 3.0 was superseded by a more stable Word 3.01, which was mailed free to all registered users of 3.0. After MacWrite, Word for Mac never had any serious rivals on the Mac. Word 5.1 for the Macintosh, released in 1992, was a very popular word processor owing to its elegance, relative ease of use and feature set. Many users say its the best version of Word for Mac ever created.{{cite web}}In 1986, an agreement between Atari and Microsoft brought Word to the Atari ST{{cite web}} under the name Microsoft Write. The Atari ST version was a port of Word 1.05 for the Apple Macintosh{{cite web}}{{cite web}} and was never updated.The first version of Word for Windows was released in 1989. With the release of Windows 3.0 the following year, sales began to pick up and Microsoft soon became the market leader for word processors for IBM PC-compatible computers. In 1991, Microsoft capitalized on Word for Windows' increasing popularity by releasing a version of Word for DOS, version 5.5, that replaced its unique user interface with an interface similar to a Windows application.{{cite news}}{{cite news}} When Microsoft became aware of the Year 2000 problem, it made Microsoft Word 5.5 for DOS available for download free. As of February 2012, it is still available for download from Microsoft's web site.{{cite web}}In 1991, Microsoft embarked on a project code-named Pyramid to completely rewrite Microsoft Word from the ground up. Both the Windows and Mac versions would start from the same code base. It was abandoned when it was determined that it would take the development team too long to rewrite and then catch up with all the new capabilities that could have been added in the same time without a rewrite. Instead, the next versions of Word for Windows and Mac, dubbed version 6.0, both started from the code base of Word for Windows 2.0.With the release of Word 6.0 in 1993, Microsoft again attempted to synchronize the version numbers and coordinate product naming across platforms, this time across DOS, Macintosh, and Windows (this was the last version of Word for DOS). It introduced AutoCorrect, which automatically fixed certain typing errors, and AutoFormat, which could reformat many parts of a document at once. While the Windows version received favorable reviews (e.g.,{{cite news}}), the Macintosh version was widely derided. Many accused it of being slow, clumsy and memory intensive, and its user interface differed significantly from Word 5.1. In response to user requests, Microsoft was forced to offer Word 5 again, after it had been discontinued.{{cite web}} Subsequent versions of Word for Macintosh are no longer ported versions of Word for Windows.

Microsoft Word for Windows since 1995Word 95 for Windows was the first 32-bit version of the product, released with Office 95 around the same time as Windows 95. It was a straightforward port of Word 6.0 and it introduced few new features, one of them being red-squiggle underlined spell-checking.{{cite web}} Starting with Word 95, releases of Word were named after the year of its release, instead of its version number.{{cite web}} Word 2010 allows more customization of the Ribbon,{{cite news}} adds a Backstage view for file management,{{cite news}} has improved document navigation, allows creation and embedding of screenshots,{{cite news}} and integrates with Word Web App.{{cite web}}
Microsoft Word for Mac since 1995{{see also}}In 1997, Microsoft formed the Macintosh Business Unit as an independent group within Microsoft focused on writing software for the Mac. Its first version of Word, Word 98, was released with Office 98 Macintosh Edition. Document compatibility reached parity with Word 97, and it included features from Word 97 for Windows, including spell and grammar checking with squiggles.{{cite news}} Users could choose the menus and keyboard shortcuts to be similar to either Word 97 for Windows or Word 5 for Mac.Word 2001, released in 2000, added a few new features, including the Office Clipboard, which allowed users to copy and paste multiple items.{{cite web}} It was the last version to run on classic Mac OS and, on Mac OS X, it could only run within the Classic Environment. Word X, released in 2001, was the first version to run natively on, and required, Mac OS X, and introduced non-contiguous text selection.{{cite news}}Word 2004 was released in May 2004. It included a new Notebook Layout view for taking notes either by typing or by voice.{{cite news}} Other features, such as tracking changes, were made more similar with Office for Windows.{{cite web}}
Word 2008, released on January 15, 2008, included a Ribbon-like feature, called the Elements Gallery, that can be used to select page layouts and insert custom diagrams and images. It also included a new view focused on publishing layout, integrated bibliography management,{{cite news}} and native support for the new Office Open XML format. It was the first version to run natively on Intel-based Macs.{{cite news}}Word 2011, released in October 2010, replaced the Elements Gallery in favor of a Ribbon user interface that is much more similar to Office for Windows,{{cite news}} and includes a full-screen mode that allows users to focus on reading and writing documents, and support for Office Web Apps.{{cite news}}
File formats
File extensionMicrosoft Word's native file formats are denoted either by a .doc or .docx file extension.Although the ".doc" extension has been used in many different versions of Word, it actually encompasses four distinct file formats:

Word for DOS
Word for Windows 1 and 2; Word 4 and 5 for Mac
Word 6 and Word 95 for Windows; Word 6 for Mac
Word 97, 2000, 2002, 2003, 2007 and 2010 for Windows; Word 98, 2001, X, and 2004 for Mac

The newer ".docx" extension signifies the Office Open XML international standard for Office documents and is used by Word 2007 and 2010 for Windows, Word 2008 and 2011 for the Macintosh, as well as by a growing number of applications from other vendors, including OpenOffice.org Writer, an open source word processing program.{{cite web}}Microsoft does not guarantee the correct display of the document on different workstations, even if the two workstations use the same version of Microsoft Word, primarily due to page layout depending on the current printer.{{cite web}} This means it is possible the document the recipient sees might not be exactly the same as the document the sender sees.
Binary formats (Word 97–2003){{more footnotes}}
During the late 1990s and early 2000s, the default Word document format (.DOC) became a de facto standard of document file formats for Microsoft Office users. Though usually just referred to as "Word Document Format", this term refers primarily to the range of formats used by default in Word version 97-2003.Italic text--178.134.127.245 (talk) 08:33, 11 February 2012 (UTC)
b
Word document files by using the Word 97-2003 Binary File Format implement OLE (Object Linking and Embedding) structured storage to manage the structure of their file format. OLE behaves rather like a conventional hard drive file system and is made up of several key components. Each Word document is composed of so-called "big blocks" which are almost always (but do not have to be) 512-byte chunks; hence a Word document's file size will in most cases be a multiple of 512."Storages" are analogues of the directory on a disk drive, and point to other storages or "streams" which are similar to files on a disk. The text in a Word document is always contained in the "WordDocument" stream. The first big block in a Word document, known as the "header" block, provides important information as to the location of the major data structures in the document. "Property storages" provide metadata about the storages and streams in a doc file, such as where it begins and its name and so forth. The "File information block" contains information about where the text in a Word document starts, ends, what version of Word created the document and other attributes.Microsoft has published specifications for the Word 97-2003 Binary File Format.{{cite web}} However, these specifications were criticised for not documenting all of the features used by Word binary file format.{{cite web}}Word 2007 and 2010 continue to support the DOC file format, although it is no longer the default.
XML Document (Word 2003){{Main}}
The XML  format introduced in Word 2003{{cite web}} was a simple, XML-based format called WordprocessingML.
Attempts at cross-version compatibilityOpening a Word Document file in a version of Word other than the one with which it was created can cause incorrect display of the document. The document formats of the various versions change in subtle and not so subtle ways (such as changing the font, or the handling of more complex tasks like footnotes), creating a "lock in" phenomenon to the base Wastemans(proprietary) standard. Formatting created in newer versions does not always survive when viewed in older versions of the program, nearly always because that capability does not exist in the previous version. Rich Text Format (RTF), an early effort to create a format for interchanging formatted text between applications, is an optional format for Word that retains most formatting and all content of the original document.Later, after HTML appeared, Word supported an HTML derivative as an additional full-fidelity roundtrip format similar to RTF, with the additional capability that the file could be viewed in a web browser. Though an .html extension is applied to the file produced, that file will fail the W3C Validator spectacularly. If the file is intended to be uploaded as a Web page, use of a tool such as HTML Tidy is a necessary step so that visitors' downloads aren't bloated with the unnecessary Microsoft-specific markup.
Third party formatsPlugins permitting the Windows versions of Word to read and write formats it does not natively support, such as international standard OpenDocument format (ODF) (ISO/IEC 26300:2006), are available. Up until the release of Service Pack 2 (SP2) for Office 2007, Word did not natively support reading or writing ODF documents without a plugin, namely the SUN ODF Plugin or the OpenXML/ODF Translator. With SP2 installed, ODF format 1.1 documents can be read and saved like any other supported format in addition to those already available in Word 2007.{{cite book}}Microsoft Expands List of Formats Supported in Microsoft Office, May 21, 2008 http://www.microsoft.com/Presspass/press/2008/may08/05-21ExpandedFormatsPR.mspxNext Office 2007 service pack will include ODF, PDF support options,
May 21, 2008 Next Office 2007 service pack will include ODF, PDF support options | Betanews{{cite web}}{{cite web}} The implementation faces substantial criticism, and the ODF Alliance and others have claimed that the third party plugins provide better support.{{cite web}} Microsoft later declared that the ODF support has some limitations.{{cite web}}In October 2005, one year before the Microsoft Office 2007 suite was released, Microsoft declared that there was insufficient demand from Microsoft customers for the international standard OpenDocument format support, and that therefore it would not be included in Microsoft Office 2007. This statement was repeated in the following months.{{cite web}}{{cite web}}23 March 2006, Gates: Office 2007 will enable a new class of application Mass. holding tight to OpenDocument - ZDNet{{cite web}} As an answer, on October 20, 2005 an online petition was created to demand ODF support from Microsoft.{{cite web}} The petition was signed by approximately 12000 people.{{cite web}}In May 2006, the ODF plugin for Microsoft Office was released by the OpenDocument Foundation.{{cite web}} Microsoft declared that it had no relationship with the developers of the plugin.{{cite web}}In July 2006, Microsoft announced the creation of the Open XML Translator project&nbsp;– tools to build a technical bridge between the Microsoft Office Open XML Formats and the OpenDocument Format (ODF). This work was started in response to government requests for interoperability with ODF. The goal of project was not to add ODF support to Microsoft Office, but only to create a plugin and an external toolset.{{cite web}}Open XML Translator project announced (ODF support for Office),
July 05, 2006 Open XML Translator project announced (ODF support for Office) - Brian Jones & Zeyad Rajabi: Office Solutions - Site Home - MSDN Blogs In February 2007, this project released a first version of the ODF plugin for Microsoft Word.{{cite web}}In February 2007, Sun released an initial version of its ODF plugin for Microsoft Office.{{cite web}} Version 1.0 was released in July 2007.{{cite web}}Microsoft Word 2007 (Service Pack 1) supports (for output only) PDF and XPS formats, but only after manual installation of the Microsoft 'Save as PDF or XPS' add-on.{{cite web}}Microsoft to remove PDF support from Office 2007 in wake of Adobe dispute, Friday, June 02, 2006 Microsoft to remove PDF support from Office 2007 in wake of Adobe dispute | TG Daily On later releases, this was offered by default.
Image formatsWord can import and display images in common bitmap formats such as JPG and GIF. It can also be used to create and display simple line-art. No version of Microsoft Word has support for the common SVG vector image format.
Features and flaws{{Ref improve section}}
Among its features, Word includes a built-in spell checker, a thesaurus, a dictionary, and utilities for manipulating and editing text. The following are some aspects of its feature set.
WordArtWordArt enables drawing text in a Microsoft Word document such as a title, watermark, or other text, with graphical effects such as skewing, shadowing, rotating, stretching in a variety of shapes and colors and even including three-dimensional effects, starting at version 2007, and prevalent in Office 2010.  Users can apply formatting effects such as shadow, bevel, glow, and reflection to their document text as easily as applying bold or underline. Users can also spell-check text that uses visual effects, and add text effects to paragraph styles.
MacrosA Macro is a rule of pattern that specifies how a certain input sequence(often a sequence of characters) should be mapped to an output sequence according to defined process. Frequently used or repetitive sequences of keystrokes and mouse movements can be automated.
Like other Microsoft Office documents, Word files can include advanced macros and even embedded programs. The language was originally WordBasic, but changed to Visual Basic for Applications as of Word 97.This extensive functionality can also be used to run and propagate viruses in documents. The tendency for people to exchange Word documents via email, USB flash drives, and floppy disks made this an especially attractive vector in 1999. A prominent example was the Melissa virus, but countless others have existed in the wild. These macro viruses were the only known cross-platform threats between Windows and Macintosh computers and they were the only infection vectors to affect any Mac OS X system up until the advent of video codec trojans in 2007. Microsoft released patches for Word X and Word 2004 that effectively eliminated the macro problem on the Mac by 2006.Word's macro security setting, which regulates when macros may execute, can be adjusted by the user, but in the most recent versions of Word, is set to HIGH by default, generally reducing the risk from macro-based viruses, which have become uncommon.
Layout issuesBefore Word 2010 (Word 14) for Windows, the program was unable to handle ligatures defined in TrueType fontsWhat's new in Word 2010. Retrieved 1 July 2010. those ligature glyphs with Unicode codepoints may be inserted manually, but are not recognized by Word for what they are, breaking spell checking, while custom ligatures present in the font are not accessible at all. Since Word 2010, the program now has advanced typesetting features which can be enabled:Improving the look of papers written in Microsoft Word, Retrieved 30 May 2010. OpenType ligatures,How to Enable OpenType Ligatures in Word 2010, Oreszek Blog, 17 May 2009. kerning, and hyphenation. Other layout deficiencies of Word include the inability to set crop marks or thin spaces. Various third-party workaround utilities have been developed.Such as WordSetter (shareware) Similarly, combining diacritics are handled poorly: Word 2003 has "improved support", but many diacritics are still misplaced, even if a precomposed glyph is present in the font.Additionally, as of Word 2002, Word does automatic font substitution when it finds a character in a document that does not exist in the font specified. It is impossible to deactivate this, making it very difficult to spot when a glyph used is missing from the font in use. If "Mirror margins" or "Different odd and even" are enabled, Word will not allow the user to freshly begin page numbering an even page after a section break (and vice versa). Instead it inserts a mandatory blank page which cannot be removed.{{cite web}}In Word 2004 for Macintosh, support of complex scripts was inferior even to Word 97{{Citation needed}}, and Word 2004 does not support Apple Advanced Typography features like ligatures or glyph variants.{{cite web}}
Bullets and numberingWord has extensive list of bullets and numbering features used for tables, lists, pages, chapters, headers, footnotes, and tables of content. Bullets and numbering can be applied directly or using a button or by applying a style or through use of a template.
Some problems with numbering have been found in Word 97-2003. An example is Word's system for restarting numbering.{{cite web}} The Bullets and Numbering system has been significantly overhauled for Office 2007, which is intended to reduce the severity of these problems. For example, Office 2007 cannot align tabs for multi-leveled numbered lists {{Citation needed}}. Often, items in a list will be inexplicably separated from their list number by one to three tabs, rendering outlines unreadable{{Citation needed}}. These problems cannot be resolved even by expert users{{Citation needed}}. Even basic dragging and dropping of words is usually impossible{{Citation needed}}. Bullet and numbering problems in Word include: bullet characters are often changed and altered, indentation is changed within the same list, bullet point or number sequence can belong to an entirely different nest within the same sequence{{Citation needed}}.
Creating tablesUsers can also create tables in MS Word. Depending on the version, Word can perform simple calculations. Formulae are supported as well.
AutoSummarizeAutoSummarize highlights passages or phrases that it considers valuable. The amount of text to be retained can be specified by the user as a percentage of the current amount of text.According to Ron Fein of the Word 97 team, AutoSummarize cuts wordy copy to the bone by counting words and ranking sentences. First, AutoSummarize identifies the most common words in the document (barring "a" and "the" and the like) and assigns a "score" to each word—the more frequently a word is used, the higher the score. Then, it "averages" each sentence by adding the scores of its words and dividing the sum by the number of words in the sentence—the higher the average, the higher the rank of the sentence. "It's like the ratio of wheat to chaff," explains Fein.{{cite web}}AutoSummarize was removed from Microsoft Word for Mac 2011, although it was present in Word for Mac 2008. AutoSummarize was removed from the Office 2010 release version (14) as well.Changes in Word 2010
Versions{{Over detailed}}


Versions for Microsoft Windows include the following:

Year Released Name Version Comments 
1989 Word for Windows 1.0 
1990 Word for Windows 1.1 1.1 Code-named Bill the Cat 
1990 Word for Windows 1.1a 1.1a For Windows 3.1 
1991 Word for Windows 2.0 2.0 Code-named Spaceman Spiff 
1993 Word for Windows 6.0 6.0 Code-named T3 (renumbered 6 to bring Windows version numbering in line with that of DOS version, Macintosh version and also WordPerfect, the main competing word processor at the time; also a 32-bit version for Windows NT only) 
1995 Word 95 7.0 Included in Office 95 
1997 Word 97 8.0 Included in Office 97 
1998 Word 98 8.5 Only sold as part of Office 97 Powered By Word 98, which was only available in Japan and Korea. 
1999 Word 2000 9.0 Included in Office 2000 
2001 Word 2002 10.0 Included in Office XP 
2003 Office Word 2003 11.0 Included in Office 2003 
2006 Office Word 2007 12.0 Included in Office 2007; released to businesses on November 30, 2006, released worldwide to consumers on January 30, 2007 
2010 Word 2010 14.0 Included in Office 2010 

Note: Version number 13 was skipped due to superstition.For the sake of superstition the next version of Office won't be called '13', Office Watch News.

Versions for the Macintosh (Mac OS and Mac OS X) include the following:

Year Released Name Comments 
1985 Word 1  
1987 Word 3  
1989 Word 4 Part of Office 1.0 and 1.5 
1991 Word 5jkj 

Part of Office 3.0
Requires System 6.0.2, 512 KB of RAM (1 MB for 5.1, 2 MB to use spell check and thesaurus), 6.5 MB available hard drive space
 
1992 Word 5.1 

Part of Office 3.0
Last version to support 68000-based Macs
 
1993 Word 6 

Part of Office 4.2
Shares code and user interface with Word for Windows 6
Requires System 7.0, 4 MB of RAM (8 MB recommended), at least 10 MB available hard drive space, 68020 CPU
 
1998 Word 98 

Part of Office 98 Macintosh Edition
Requires PowerPC-based Mac
 
2000 Word 2001 

Part of Microsoft Office 2001
Last version compatible with Classic (OS 9 or earlier) Mac OS
 
2001 Word v. X 

Part of Office v. X
First version for Mac OS X only
 
2004 Word 2004 Part of Office 2004 
2008 Word 2008 Part of Office 2008 
2010 Word 2011 Part of Office 2011 
Versions for MS-DOS

Year Released Name Comments 
1983 Word 1  
1985 Word 2  
1986 Word 3  
1987 Word 4  
1989 Word 5  
1991 Word 5.1  
1991 Word 5.5 First DOS version to use a Windows-like user interface 
1993 Word 6.0  
Versions for the Atari ST:

Year Released Name Comments 
1988 Microsoft Write Conversion of Microsoft Word 1.05 for Mac 
Versions for OS/2 include the following:

Year Released Name Comments 
1992 Microsoft Word for OS/2 version 1.1B  

See also

List of word processors
Comparison of word processors
Microsoft Word Viewer
{{wikiversity}}
References{{Reflist}}
Further reading

Tsang, Cheryl. Microsoft: First Generation. New York: John Wiley &#38; Sons, Inc. ISBN 978-0-471-33206-0.
Liebowitz, Stan J. &#38; Margolis, Stephen E. WINNERS, LOSERS & MICROSOFT: Competition and Antitrust in High Technology Oakland: Independent Institute. ISBN 978-0-945999-80-5.


External links

Microsoft Word home page
Office 2010 product guide
The Word Object Model
Ms Word Files Generation using .net framework
Changing the Normal.dot file in Microsoft templates
Microsoft Word 1.0 for Macintosh screenshots
The Word MVP Site

{{Word processors}}{{Microsoft Office}}






af:Microsoft Word
ar:مايكروسوفت وورد
az:Microsoft Word
bg:Microsoft Word
bs:Microsoft Word
ca:Microsoft Word
cs:Microsoft Word
da:Microsoft Word
de:Microsoft Word
et:Microsoft Word
es:Microsoft Word
eo:Microsoft Word
eu:Microsoft Word
fa:مایکروسافت ورد
fr:Microsoft Word
ga:Microsoft Office Word
ko:마이크로소프트 워드
hy:Microsoft Office Word
hi:माइक्रोसॉफ्ट वर्ड
hr:Microsoft Word
id:Microsoft Word
it:Microsoft Word
he:Microsoft Word
kk:Microsoft Office Word
la:Microsoft Word
lt:Microsoft Word
hu:Microsoft Word
mk:Microsoft Word
mr:मायक्रोसॉफ्ट वर्ड
ms:Microsoft Word
nl:Microsoft Office Word
ja:Microsoft Word
no:Microsoft Office Word
pl:Microsoft Word
pt:Microsoft Word
ro:Microsoft Word
ru:Microsoft Word
sq:Microsoft Word
simple:Microsoft Word
sk:Microsoft Word
sl:Microsoft Word
sr:Мајкрософт ворд
fi:Microsoft Word
sv:Microsoft Word
ta:மைக்ரோசாப்ட் வேர்டு
th:ไมโครซอฟท์ เวิร์ด
tr:Microsoft Word
uk:Microsoft Word
vep:Microsoft Office Word
vi:Microsoft Word
yi:Microsoft Word
zh:Microsoft Word