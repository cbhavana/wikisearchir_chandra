title{{Infobox royalty}}
Ferdinand III, Grand Duke of Tuscany (6 May 1769 &ndash; 18 June 1824) was Grand Duke of Tuscany from 1790 to 1801 and, after a period of disenfranchisement, again from 1814 to 1824. He was also the Prince-elector and Grand Duke of Salzburg (1803–1806) and Grand Duke of Würzburg (1806–1814).
Biography
Ferdinand was born in Florence, Tuscany, into the House of Habsburg-Lorraine. He was the son of Leopold, then Grand-Duke of Tuscany, and his wife Infanta Maria Luisa of Spain. When his father was elected Emperor of the Holy Roman Empire, Ferdinand succeeded him as Grand Duke of Tuscany, officially taking the office on 22 July 1790.{{Cite book}}{{Cite book}}In 1792 during the French Revolution, Ferdinand became the first monarch to formally recognize the new French First Republic, and he attempted to work peacefully with it. As the French Revolutionary Wars commenced, however, the English and Russian monarchs persuaded him to join their side in the War of the First Coalition. Ferdinand provided his allies with passive support but no enthusiasm, and after he witnessed a year of resounding victories by the French, he became the first member of the coalition to give up. In a proclamation dated 1 March 1795, he abandoned the alliance and declared Tuscany's neutrality in the war.{{Cite book}}His normalization of relations with France helped stabilize his rule for several years but by 1799 he was compelled to flee to Vienna for protection when republicans established a new government in Florence. He was forced to renounce his throne in 1801 by the Treaty of Aranjuez:Napoleon brushed him aside to make way for the Kingdom of Etruria, created as compensation for the Bourbon Dukes of Parma, being dispossessed by the Peace of Lunéville in that same year.Ferdinand was compensated by being given the Dukedom and Electorate of Salzburg, the secularized lands of the Archbishop of Salzburg, as Duke of Salzburg. He was also made a Prince-elector of the Holy Roman Empire, both on 26 December 1802, a role which expired with the Empire's dissolution in 1806.On 25 December 1805, Ferdinand had to give up Salzburg as well, which by the Treaty of Pressburg was annexed by his older brother, Emperor Francis II. Ferdinand was then made Duke of Würzburg, a new state created for him from the old Bishopric of Würzburg, remaining an Elector. With the dissolution of the Empire in 1806 he took the new title of Grand Duke of Würzburg.On 30 May 1814, after Napoleon's fall, Ferdinand was restored as Grand Duke of Tuscany. However, in 1815, the Duchy of Lucca was carved out of Tuscany, again as temporary compensation for the Bourbons of Parma. (Lucca would be reintegrated into Tuscany in 1847.)Ferdinand died in 1824 in Florence and was succeeded by his son Leopold.
Family and childrenIn Naples on 15 August 1790 by proxy and in Vienna on 19 September 1790 in person, Ferdinand married firstly his double first cousin, the Princess (later Grand Duchess) Luisa of Naples and Sicily (1773–1802), daughter of Ferdinand I of the Two Sicilies and Marie Caroline of Austria.Their children were:

Archduchess Carolina Ferdinanda Teresa of Austria, born 2 August 1793;
Archduke Francesco Leopoldo of Austria, born 15 December 1794;
Leopold II, Grand Duke of Tuscany born 1797;
Archduchess Maria Luisa Giuseppa Cristina Rosa of Austria, born 30 August 1799;
Archduchess Maria Theresa of Austria, born 21 March 1801;

Their first two children, Carolina and Francesco, died at very young ages (eight and five respectively) but the later three prospered under their father's care. Grand Duchess Luisa died when they were all quite young, on 19 September 1802, together with a stillborn son who was unnamed. Two decades later, in Florence on 6 May 1821, Ferdinand married again, this time to the much younger Princess Maria Ferdinanda of Saxony (1796–1865). She was the daughter of Maximilian, Prince of Saxony, and his wife Caroline of Bourbon-Parma; she was also his first cousin once removed, as well as the first cousin once removed of the dead Luisa. Though Ferdinand was likely hoping to produce another male heir, there were no children born of this second marriage.
Titles and styles

6 May 1769 - 22 July 1790 His Imperial and Royal Highness Archduke Ferdinand, Prince Imperial of Austria, Prince Royal of Hungary and Bohemia
22 July 1790 - 3 August 1801 His Imperial and Royal Highness The Grand Duke of Tuscany
26 December 1802 - 25 December 1805 His Imperial and Royal Highness The Prince-Elector and Grand Duke of Salzburg
25 December 1805 - 27 April 1814 His Imperial and Royal Highness The Grand Duke of Würzburg


Ancestry{{Ahnentafel top}}
{{Ahnentafel-compact5}}
{{Ahnentafel bottom}}
References{{Reflist}}
External links

House of Habsburg, Tuscan Branch, family tree by Ferdinand Schevill in A Political History of Modern Europe (1909)


{{S-start}}
{{S-hou}}
{{S-reg}}
{{Succession box}}
{{Succession box}}
{{Succession box}}
{{Succession box}}
{{S-end}}
{{Austrian archdukes}}
{{tuscan princes}}
{{Grand Dukes of Tuscany}}{{Use dmy dates}}{{Persondata}}
{{DEFAULTSORT:Ferdinand 03 Of Tuscany, Grand Duke}}












ar:فرديناندو الثالث دوق توسكانا الأكبر
ca:Ferran III de Toscana
cs:Ferdinand III. Toskánský
de:Ferdinand III. (Toskana)
el:Φερδινάνδος Γ', μέγας δούκας της Τοσκάνης
es:Fernando III de Toscana
fr:Ferdinand III de Toscane
it:Ferdinando III di Toscana
la:Ferdinandus III Tusciae
hu:III. Ferdinánd toszkánai nagyherceg
nl:Ferdinand III van Toscane
ja:フェルディナンド3世 (トスカーナ大公)
pl:Ferdynand III Toskański
pt:Fernando III, grão-duque da Toscana
ro:Ferdinand al III-lea, Mare Duce de Toscana
ru:Фердинанд III (великий герцог Тосканский)
sv:Ferdinand III av Toscana
vi:Ferdinando III, Đại Công tước xứ Toscana