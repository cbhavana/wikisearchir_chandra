title{{see also}}{{Infobox Ethnic group}}The Maya people constitute a diverse  range of the Native American people of southern Mexico and northern Central America. The overarching term "Maya" is a collective designation to include the peoples of the region who share some degree of cultural and linguistic heritage; however, the term embraces many distinct populations, societies, and ethnic groups, who each have their own particular traditions, cultures, and historical identity.There are an estimated 7 million Maya living in this area at the start of the 21st century. Ethnic Maya of Guatemala, southern Mexico and the Yucatan Peninsula, Belize, El Salvador, and western Honduras have managed to maintain substantial remnants of their ancient cultural heritage. Some are quite integrated into the majority hispanicized Mestizo cultures of the nations in which they reside, while others continue a more traditional culturally distinct life, often speaking one of the Maya languages as a primary language.The largest populations of contemporary Maya inhabit Guatemala, Belize, and the western portions of Honduras and El Salvador, as well as large segments of population within the Mexican states of Yucatán, Campeche, Quintana Roo, Tabasco, and Chiapas.
Yucatán Peninsula
{{Maya civilization}}
The largest group of modern Maya can be found on Mexico's  Yucatán Peninsula. They commonly identify themselves simply as "Maya" with no further ethnic subdivision (unlike in the Highlands of Western Guatemala).  They speak the language which anthropologists term "Yucatec Maya", but is identified by speakers and Yucatecos simply as "Maya".  Among Maya speakers Spanish is commonly spoken as a second or first language. There is a significant amount of confusion as to the correct terminology to use—Maya or Mayan—and the meaning of these words with reference to contemporary or precolumbian peoples, to Mayan peoples in different parts of Mexico, Guatemala, Belize,  and to languages or peoples.  Linguists refer to Maya (language) as Yucatec or Yucatec Maya to disambiguate any confusion with other Mayan languages.  This norm has often been misinterpreted to mean that the people are also called Yucatec Maya, but that term only refers to the language and the correct name for the people is simply Maya (not Mayans).  Maya is one language in the Mayan language family.  Thus, to refer to Maya as Mayans would be similar to referring to Americans as Germanics because they speak a language belonging to the Germanic language family.{{cite web}}  Further, confusion of the term Maya/Mayan as ethnic label occurs because Maya women who use traditional dress autoidentify by the ethnic term mestiza and not Maya.{{cite journal}} As well, persons use a strategy of ethnic identitification that Juan Castillo Cocom refers to as "ethnoexodus" -- meaning that ethnic self identification as Maya is quite variable, situational, and articulated not to processes of producing group identity but of escaping from discriminatory processes of sociocultural marginalization (see http://i09.cgpublisher.com/proposals/330/index_html).{{cite journal}}  The Yucatán's indigenous population was first exposed to Europeans after a party of Spanish shipwreck survivors came ashore in 1511. One of the sailors, Gonzalo Guerrero, is reported to have started a family and taken up a position of counsel among a local polity near present-day Chetumal. Later Spanish expeditions to the region were led by Córdoba in 1517, Grijalva in 1518 and Cortés in 1519. From 1528 to 1540, several attempts by Francisco Montejo to conquer the Yucatán failed. His son Francisco de Montejo the Younger fared almost as badly when he first took over: while desperately holding out at Chichen Itza, he lost 150 men in a single day.Clendinnen, Inga; Ambivalent Conquests: Maya and Spaniard in Yucatán, 1517-1570. (pg 34) ISBN 0521379814 European diseases, massive recruitment of native warriors from Campeche and Champoton, and internal hatred between the Xiu Maya and the lords of Cocom eventually turned the tide for Montejo the Younger and consequently resulted in the fall of Chichen Itza by 1570. In 1542, the western Yucatán peninsula also surrendered to him.Historically, the population in the eastern half of the peninsula was less affected by and less integrated with Hispanic culture than that of the western half. Today in the Yucatán Peninsula (Mexican States of Campeche, Yucatán and Quintana Roo) between 750,000 and 1,200,000 people speak Mayan. However three times more than that do not speak their native language but are from Maya origins and hold ancient Maya last names, such as Ak, Can, Chan, Be, Cantun, Canche, Chi, Chuc, Coyoc, Dzib, Dzul, Ehuan, Hoil, Hau, May, Tamay, Ucan, Pool, Zapo, etc..Matthew Restall, in his book The Maya Conquistador,Matthew Restall. Maya Conquistador. Boston, Mass.: Beacon. 1998. Pp. xvi, 254. mentions a series of letters sent to the King of Spain in the 16th and 17th Centuries. The noble Maya Families at that time signed documents to the Spanish Royal Family; surnames mentioned in those letters are Pech, Camal, Xiu, Ucan, Canul, Cocom, and Tun, among others. A large 19th century revolt by the native Maya people of Yucatán (Mexico), known as the Caste War of Yucatán, was one of the most successful modern Native American revolts;The Caste War of Yucatan: Revised Edition, By Nelson Reed, Published by Stanford University Press, 2002 ISBN 0804740011, 9780804740012, 448 pages results included the temporary existence of the Maya state of Chan Santa Cruz, recognized as an independent nation by the British Empire.Francisco Luna-Kan was elected governor of the state of Yucatán from 1976 to 1982. Luna-Kan was born in Mérida, Yucatán, and he was a Doctor of medicine, then a Professor of Medicine before his political offices, his first being overseer of the state's rural medical system. He was the first Governor of in the modern Yucatan Peninsula, from a full Maya background. Currently there are dozens of politicians including Deputies, Majors and Senators from a full or mixed Maya heritage from the Yucatan Peninsula. According to the National Institute of Geography and Informatics (Mexico’s INEGI) in Yucatan State there were 1.2 million of Mayan speakers in 2009, representing 59.5% of the inhabitants.http://www.el-universal.com.mx/articulos/52903.html Due to this, the cultural section of the government of Yucatan began to give on-line classes for grammar and proper pronunciation of Maya.http://www.indemaya.gob.mx/Maya People from Yucatan Peninsula living in the United States of America have been organizing Maya language lessons and Maya cooking classes since 2003 in California and other states: clubs of Yucatec Mayahttp://www.yucatecos.org/home.html are registered in Dallas and Irving, Texas; Salt Lake City in Utah; Las Vegas, Nevada; and California, with groups in San Francisco, San Rafael, Chino, Pasadena, Santa Ana, Garden Grove, Inglewood, Los Angeles, Thousand Oaks, Oxnard, San Fernando Valley and Whittier.
Chiapas
Chiapas was for many years one of the regions of Mexico that were least touched by the reforms of the Mexican Revolution. The Zapatista Army of National Liberation, which launched a rebellion against the Mexican state in Chiapas in January 1994, declared itself to be an indigenous movement and drew its strongest and earliest support from Chiapan Maya, a number of whom still support it today. (see also the EZLN and the Chiapas conflict)Maya groups in Chiapas include the Tzotzil and Tzeltal, in the highlands of the state, the Tojolabalis concentrated in the lowlands around Las Margaritas, and the Ch'ol in the jungle. (see map)The most traditional of Maya groups are the Lacandon, a small population avoiding contact with outsiders until the late 20th century by living in small groups in the Lacandon Jungle. These Lacandon Maya came from the Campeche/Petén area (north-east of Chiapas) and moved into the Lacandon rain-forest at the end of the 18th century, 1000 years after the ancient (Pre-Columbian) Maya civilization had disappeared (around 850 A.D).In the course of the 20th century, and increasingly in the 1950s and 1960s, other people (mainly The Maya and subsistence peasants from the highlands), also entered into the Lacandon region; initially encouraged by the government. This immigration led to land-related conflicts and an increasing pressure on the rainforest. To halt the migration the government decided in 1971 to declare a large part of the forest (614,000 hectares, or 6140&nbsp;km2) a protected area: the Montes Azules Biosphere Reserve. They appointed only one small population group (the 66 Lacandon families) as tenants (thus creating the Lacandon Community), thereby displacing 2000 Tzeltal and Ch'ol families from 26 communities, and leaving non-Lacandon communities dependent on the government for granting their rights to land. In the decades that followed the government carried out numerous programs to keep the problems in the region under control, using land distribution as a political tool; as a way of ensuring loyalty from different campesino groups. This strategy of divide and rule led to great disaffection and tensions among population groups in the region.
(see also the Chiapas conflict and the Lacandon Jungle).
BelizeThe Maya population in Belize is concentrated in the Cayo, Toledo and Orange Walk districts, but they are scattered throughout the country. They are divided into the Yucatec, Kekchi, and Mopan.
TabascoThe Mexican state of Tabasco is home to the Chontal Maya.
Guatemala
In Guatemala, the largest and most traditional Maya populations are in the western highlands. The departments of Baja Verapaz, Quiché, Totonicapan, Huehuetenango, Quetzaltenango, and San Marcos are mostly Maya.http://www.inforpressca.com/municipal/mapas_web/guatemala.phpIn Guatemala, the Spanish colonial pattern of keeping the native population legally separate and subservient continued well into the 20th century. This resulted in many traditional customs being retained, as the only other option than traditional Maya life open to most Maya was entering the Hispanic culture at the very bottom rung.Considerable identification with local and linguistic affinities, often corresponding to pre-Columbian nation states, continues, and many people wear traditional clothing that displays their specific local identity. Clothing of women tends to be more traditional than that of the men.The Maya people of the Guatemala highlands include the Achi, Akatek, Chuj, Ixil, Jakaltek, Kaqchikel, K'iche', Mam, Poqomam, Poqomchi', Q'anjob'al, Q'eqchi', Tz'utujil and Uspantek.The southeastern region of Guatemala (bordering with Honduras) includes groups such as the Ch'orti'. The northern lowland Petén region includes the Itza, whose language is near extinction but whose agro-forestry practices, including use of dietary and medicinal plants may still tell us much about pre-colonial management of the Maya lowlands.Atran, Scott; Lois, Ximena; Ucan Ek', Edilberto(2004) Plants of the Peten Itza Maya, Memoirs of the Museum of Anthropology, University of Michigan, 38
Maya Heritage
The Maya people are known for their brightly colored yarn-based textiles, which are woven into capes, shirts, blouses, huipiles and dresses. Each village has its own distinctive pattern, making it possible to distinguish a person's home town on sight. Women's clothing consists of a shirt and a long skirt.
Roman Catholicism combined with the indigenous Maya religion are the unique syncretic religion which prevailed throughout the country and still does in the rural regions. Beginning from negligible roots prior to 1960, however, Protestant Pentecostalism has grown to become the predominant religion of Guatemala City and other urban centers and down to mid-sized towns. The unique religion is reflected in the local saint, Maximón, who is associated with the subterranean force of masculine fertility and prostitution. Always depicted in black, he wears a black hat and sits on a chair, often with a cigar placed in his mouth and a gun in his hand, with offerings of tobacco, alcohol, and Coca-cola at his feet. The locals know him as San Simon of Guatemala.
The Popol Vuh is the most significant work of Guatemalan literature in the Quiché language, and one of the most important of Pre-Columbian American literature. It is a compendium of Mayan stories and legends, aimed to preserve Maya traditions. The first known version of this text dates from the 16th century and is written in Quiché transcribed in Latin characters. It was translated into Spanish by the Dominican priest Francisco Ximénez in the beginning of the 18th century. Due to its combination of historical, mythical, and religious elements, it has been called the Maya Bible. It is a vital document for understanding the culture of pre-Columbian America. The Rabinal Achí is a dramatic work consisting of dance and text that is preserved as it was originally represented. It is thought to date from the 15th century and narrates the mythical and dynastic origins of the Kek'chi' people, and their relationships with neighboring people. The Rabinal Achí is performed during the Rabinal festival of January 25, the day of Saint Paul. It was declared a masterpiece of oral tradition of humanity by UNESCO in 2005. The 16th century saw the first native-born Guatemalan writers that wrote in Spanish.
List of leaders and notable Maya people{{See also}}

Armando Manzanero Canché
Jacinto Canek
Rigoberta Menchú
Felipe Carrillo Puerto
Francisco Luna Kan
Comandante Ramona
Hilario Chi Canul
Crescencio Poot
Ah Ahaual
Marcial Mes
Gaspar Antonio Chi
Juan Jose Pacho
Alexandro Cirilo Perez Oxlaj


Quotes

&#34;We are not myths of the past, ruins in the jungle or zoos. We are people and we want to be respected, not to be victims of intolerance and racism.&#34; — Rigoberta Menchú, 1992.Quote taken from an interview with her by a representative of a Central American human rights organization (Riis-Hansen 1992). Menchú gave this interview shortly before she was awarded the Nobel Peace Prize


See also

The Forgotten District, a documentary on Maya ecotourism in southern Belize, Central America


Notes{{Reflist}}
References{{refbegin}}

{{cite journal}}
{{cite book}}
Mooney, James, {{CathEncy}}
Restall, Matthew (1997). The Maya World. Yucatecan Culture and Society, 1550-1850. Stanford: Stanford University Press. ISBN 9780804736589.
{{cite web}}
{{cite book}}
{{refend}}
Further reading{{refbegin}}

{{cite web}}
{{cite book}}
{{cite book}}
{{cite book}}
{{cite book}}
{{cite book}}
{{cite book}}
{{refend}}
External links{{Commons category}}

Courtly Art of the Ancient Maya at the National Gallery of Art
Learn more about Maya hieroglyphs and Maya numbering from the National Gallery of Art
News, Articles, Videos, and Information on the Indigenous Maya
Yax Te' Books homepage, publishing house in Philadelphia for Mayan literature.
Cholsamaj Foundation homepage publishing house in Guatemala City for Mayan literature (note: many pages on the site not yet translated into English).
Mystery of the Maya -  Canadian Museum of Civilization
A traditional Mayan horse race for the Todos Santos Holiday

{{DEFAULTSORT:Maya Peoples}}





ar:شعب المايا
be:Мая
bg:Маи
bs:Maje
ca:Maia
cs:Mayové
cy:Maya
nv:Mááya dinéʼiʼ
dsb:Maya
et:Maiad
es:Pueblo maya
fr:Mayas
fy:Maya's
hr:Maya Indijanci
id:Bangsa Maya
jv:Suku Maya
kk:Майя
la:Maia
lt:Majai (tauta)
mk:Маи
nl:Maya's
ja:マヤ人
pl:Majowie
pt:Povos maias
qu:Maya
ru:Майя (народ)
sq:Majat (popull)
simple:Maya people
sk:Mayovia
sr:Маје
fi:Mayakansat
sv:Mayaindianer
uk:Майя (народ)
zh:玛雅人