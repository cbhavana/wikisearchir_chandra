title
{{Table Numeral Systems}}
Maya Numerals (otherwise known as mayan numerals) were a vigesimal (base-twenty) numeral system used by the Pre-Columbian Maya civilization.The numerals are made up of three symbols; zero (shell shape), one (a dot) and five (a bar).  For example, nineteen (19) is written as four dots in a horizontal row above three horizontal lines stacked upon each other.
Numbers above 19

400s    
20s    
1s    
 33 429 5125 
Numbers after 19 were written vertically in powers of twenty. For example, thirty-three would be written as one dot above three dots, which are in turn atop two lines. The first dot represents "one twenty" or "1×20", which is added to three dots and two bars, or thirteen. Therefore, (1×20) + 13 = 33. Upon reaching 20^2 or 400, another row is started. The number 429 would be written as one dot above one dot above four dots and a bar, or (1×20^2) + (1×20^1) + 9 = 429. The powers of twenty are numerals, just as the Hindu-Arabic numeral system uses powers of tens.{{cite web}}Other than the bar and dot notation, Maya numerals can be illustrated by face type glyphs or pictures. The face glyph for a number represents the deity associated with the number. These face number glyphs were rarely used, and are mostly seen only on some of the most elaborate monumental carving.
Addition and subtractionAdding and subtracting numbers below 20 using Maya numerals is very simple.http://www.museumofman.org/html/lessonplan_maya_math2.pdf
Addition is performed by combining the numeric symbols at each level:
If five or more dots result from the combination, five dots are removed and replaced by a bar. If four or more bars result, four bars are removed and a dot is added to the next higher column.Similarly with subtraction, remove the elements of the subtrahend symbol from the minuend symbol:
If there are not enough dots in a minuend position, a bar is replaced by five dots. If there are not enough bars, a dot is removed from the next higher minuend symbol in the column and four bars are added to the minuend symbol being worked on.
ZeroThe Maya/Mesoamerican Long Count calendar required the use of zero as a place-holder within its vigesimal positional numeral system. A shell glyph --  -- was used as a zero symbol for these Long Count dates, the earliest of which (on Stela 2 at Chiapa de Corzo, Chiapas) has a date of 36 BC.No long count date actually using the number 0 has been found before the 3rd century AD, but since the long count system would make no sense without some placeholder, and since Mesoamerican glyphs do not typically leave empty spaces, these earlier dates are taken as indirect evidence that the concept of 0 already existed at the time. However, since the eight earliest Long Count dates appear outside the Maya homeland,Diehl (2004, p.186). it is assumed that the use of zero predated the Maya, and was possibly the invention of the Olmec. Indeed, many of the earliest Long Count dates were found within the Olmec heartland. However, the Olmec civilization had come to an end by the 4th century BC, several centuries before the earliest known Long Count dates—which suggests that zero was not an Olmec discovery.
In the calendar
In the "Long Count" portion of the Maya calendar, a variation on the strictly vigesimal numbering is used. The Long Count changes in the third place value; it is not 20×20 = 400, as would otherwise be expected, but 18×20, so that one dot over two zeros signifies 360.  This is supposed to be because 360 is roughly the number of days in a year. (Some hypothesize that this was an early approximation to the number of days in the solar year, although the Maya had a quite accurate calculation of 365.2422 days for the solar year at least since the early Classic era.){{Citation needed}}  Subsequent place values return to base-twenty.In fact, every known example of large numbers uses this 'modified vigesimal' system, with the third position representing multiples of 18×20. It is reasonable to assume, but not proven by any evidence, that the normal system in use was a pure base-20 system.
Notes{{reflist}}
References{{refbegin}}

{{cite book}}
{{cite journal}} {{es icon}}
{{cite book}}
{{cite book}}

{{refend}}
External links{{Commons category}}

Maya Mathematics online converter from decimal numeration to Maya numeral notation.
Anthropomorphic Maya numbers online story of number representations.

{{DEFAULTSORT:Maya Numerals}}


{{Link GA}}
az:Mayya rəqəmləri
bo:མ་ཡའི་ཨང་ཀི།
bg:Цифри на маите
ca:Numeració maia
cs:Mayská dvacítková soustava
de:Zahlendarstellung der Maya
es:Numeración maya
fr:Numération maya
ko:마야 숫자
it:Sistema di numerazione maya
he:ספרות מאיה
lt:Majų skaitmenys
hu:Maja számrendszer
ms:Angka Maya
nl:Mayacijfers
no:Maya-matematikk
nds:Maya-Tallen
pl:Cyfry Majów
pt:Numeração maia
ru:Цифры майя
sh:Majanski brojevi
fi:Mayojen numerot
sv:Mayakulturens matematik
ta:மாயர் எண் முறைமை
th:เลขมายา
uk:Система числення майя
zh:玛雅数字