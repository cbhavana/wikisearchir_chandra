title{{Merge}}
Ancient Maya women had an important role in society: beyond just propagating culture through the bearing and raising of children, Maya women involved themselves in economic, governmental and farming activities.  Yet the lives of women in ancient Mesoamerica were not well documented: “of the three elite founding area tombs discovered to date within the Copan Acropolis," writes one scholar, "two contain the remains of women, and yet there is not a single reference to a woman in either known contemporary texts or later retrospective accounts of Early Classis events and personages at Copan.”Bell, E. E.  “Engendering a Dynasty: A Royal Woman in the Margarita Tomb, Copan.”  In Ancient Maya Women, edited by Traci Ardren. Walnut Creek: Altamira Press, 2002 The status of women in pre-Columbian Maya society can be inferred only from their elaborate burial sites.
Matrilineal societies
Matrilineal Maya societies include Tonina, a city which became matrilineal after the death of the powerful leader Lady K’awil.  She is proof that women were involved in politics in ancient Mesoamerica. She assumed the mantle of power after the failure of two male leaders.Ayala Falcon, M. “Lady K’awil, Goddess O, and Maya Warfare.”  In Ancient Maya Women, edited by Traci Ardren. Walnut Creek: Altamira Press, 2002  Lady K'awil's reign is documented by murals which depict her seated on a throne with captives at her feet!
Food
Aside from political involvement, women also participated in the Maya food economy. Deer played an essential part of the Mesoamerican diet; most of the meat consumed by the ancient Maya was venison.  Society depended on deer meat, and women played a large role in making sure there was an abundant supply of deer.  Evidence shows that deer sometimes lived inside Maya households.  “there is also ample evidence that deer may have been nurtured in the household, much as children were.”  {{Citation needed}}  While it was the man’s responsibility to hunt and kill deer, it was necessary for the women to make sure there would be enough deer to kill.  Women raised deer due to the dwindling area of graze-worthy savanna.	
Art
The importance of the moon goddess is seen through her depiction in the codices and in ancient murals.  However, these were not the only forms of art for the ancient Maya.  Textiles were an important aspect of ancient Mayan life, and while it is not known whether all women produced textiles, those textiles that were produced were created by women.  The objects that women used in the spinning and weaving processes were different, depending on the social class of the women.  Noble women had the good fortune to use dye in their textiles.  Also, the products that were used in the spinning were different; the noble women used higher quality fibers.  {{Citation needed}} Craft and fiber evidence from the city of Ceren, which was buried by volcanic ash in 600 C.E.,  indicates that women's textile work was no longer considered a mundane task.  The tapestries being woven at the time of the city’s destruction were works of art, no longer simply constructed for a specific household purpose.  The fact that these works of art were being created suggests that there was a market for them.  Women thus held power in their ability to work thread and to create something that retained a value because someone outside of their own home desired it {{Citation needed}}. This means that for women to know what was desired by the public they must have had contact with the outside world. Women were not merely relegated to their own homes. {{Citation needed}}
Gender roles
Men and women performed separate but equal tasks: “males produce[d] food by agricultural labor, but females process[ed] the products of the field to make them edible.”Josserand, J. K.  “Women in Classic Maya Hieroglyphic Texts.”  Walnut Creek: Altamira Press, 2002  In addition to raising deer when necessary, the duties of women were embedded in the culture’s rich tradition, including its religion.  Women held important everyday roles in this aspect of life. While young boys were being taught hunting skills, “the girl was trained in the household, and she was taught how to keep the domestic religious shrines.”Sigal, P.  From Moon Goddesses to Virgins: The Colonization of Yucatecan Maya Sexual Desire.  Austin: University of Texas Press, 2000  Women were associated with the ritual practice of religion, as well as the actual beliefs themselves.  The Moon Goddess is one of the most prominent gods in the Maya pantheon.  Through her relations with the other gods, she was able to produce the Maya population.  The local rulers would then claim descent from the Moon Goddess.  Gender in ancient Maya art is ambiguous; it is difficult to ascertain the gender of some figures simply because one couldn’t survive without the other.  In some images of heir recognition, this duality is explicit: there is a male figure on one side of the newly-anointed, and a female figure on the other side.
Child bearing
Although women were important in aspects other than those associated with the ability to bear children, the latter remained a very important part of their lives.  The mythology and power associated with the ability to create was one which men tried to emulate.  Men would participate in the act of bloodletting their own genitals to create something new from their blood.Gustafson, L. S.  “Mother/Father Kings.”  In Ancient Maya Gender Identity and Relations, edited by Lowell S. Gustafson and Amelia M. Trevelyan.  Westport: Bergin & Garvey, 2002  Instead of giving birth to life they would give birth to new eras through the symbolic gesture of menstruation.  This act was highly ritualized; the objects used to pierce the skin were “stingray spines, obsidian blades, or other sharp instruments.” The blood was allowed to drip on cloth, which was then burned.
See also

Women in Maya society
Women rulers in Maya society
Gender in Mesoamerican cultures
Goddess I
Maya moon goddess
Ixchel


Notes{{reflist}}
References{{refbegin}}

{{cite book}}



{{cite book}}

{{refend}}

