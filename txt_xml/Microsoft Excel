title{{Infobox software}}
{{Infobox software}}Microsoft Excel is a commercial spreadsheet application written and distributed by Microsoft for Microsoft Windows and Mac OS X. It features calculation, graphing tools, pivot tables, and a macro programming language called Visual Basic for Applications. It has been a very widely applied spreadsheet for these platforms, especially since version 5 in 1993, and it has almost completely replaced Lotus 1-2-3 as the industry standard for spreadsheets. Excel forms part of Microsoft Office. The current versions are 2010 for Microsoft Windows and 2011 for Mac OS X.
Basic operation{{Main}}
Microsoft Excel has the basic features of all spreadsheets,{{cite book}} using a grid of cells arranged in numbered rows and letter-named columns to organize data manipulations like arithmetic operations. It has a battery of supplied functions to answer statistical, engineering and financial needs. In addition, it can display data as line graphs, histograms and charts, and with a very limited three-dimensional graphical display. It allows sectioning of data to view its dependencies on various factors from different perspectives (using pivot tables and the scenario manager{{cite book}}). And it has a programming aspect, Visual Basic for Applications, allowing the user to employ a wide variety of numerical methods, for example, for solving differential equations of mathematical physics,{{cite book}}{{cite book}} and then reporting the results back to the spreadsheet. Finally, it has a variety of interactive features allowing user interfaces that can completely hide the spreadsheet from the user, so the spreadsheet presents itself as a so-called application, or decision support system (DSS), via a custom-designed user interface, for example, a stock analyzer,{{cite book}} or in general, as a  design tool that asks the user questions and provides answers and reports.{{cite book}} Excellent examples are developed that show just how applications can be designed.{{cite book}}{{cite book}} In a more elaborate realization, an Excel application can automatically poll external databases and measuring instruments using an update schedule,Some form of data acquisition hardware is required. See, for example, {{cite book}} analyze the results, make a Word report or Power Point slide show, and e-mail these presentations on a regular basis to a list of participants.


Microsoft allows for a number of optional command-line switches to control the manner in which Excel starts.{{cite web}}
VBA programming{{Main}}
The Windows version of Excel supports programming through Microsoft's Visual Basic for Applications (VBA), which is a dialect of Visual Basic. Programming with VBA allows spreadsheet manipulation that is awkward or impossible with standard spreadsheet techniques. Programmers may write code directly using the Visual Basic Editor (VBE), which includes a window for writing code, debugging code, and code module organization environment. The user can implement numerical methods as well as automating tasks such as formatting or data organization in VBAFor example, by converting to Visual Basic the recipes in {{cite book}} Code conversion to Basic from Fortran probably is easier than from C++, so the 2nd edition (isbn=0521437210) may be easier to use, or the Basic code implementation of the first edition: {{cite book}}. and guide the calculation using any desired intermediate results reported back to the spreadsheet.VBA was removed from Mac Excel 2008, as the developers did not believe that a timely release would allow porting the VBA engine natively to Mac OS X. VBA was restored in the next version, Mac Excel 2011.A common and easy way to generate VBA code is by using the Macro Recorder.However an increasing proportion of Excel functionality is not captured by the Macro Recorder leading to largely useless macros.

{{cite book}}
The Macro Recorder records actions of the user and generates VBA code in the form of a macro. These actions can then be repeated automatically by running the macro. The macros can also be linked to different trigger types like keyboard shortcuts, a command button or a graphic. The actions in the macro can be executed from these trigger types or from the generic toolbar options. The VBA code of the macro can also be edited in the VBE. Certain features such as loop functions and screen prompts by their own properties, and some graphical display items, cannot be recorded, but must be entered into the VBA module directly by the programmer. Advanced users can employ user prompts to create an interactive program, or react to events such as sheets being loaded or changed.VBA code interacts with the spreadsheet through the Excel Object Model,{{cite book}} a vocabulary identifying spreadsheet objects, and a set of supplied functions or methods that enable reading and writing to the spreadsheet and interaction with its users (for example, through custom toolbars or command bars and message boxes). User-created VBA subroutines execute these actions and operate like macros generated using the macro recorder, but are more flexible and efficient.
ChartsExcel supports charts, graphs or histograms generated from specified groups of cells. The generated graphic component can either be embedded within the current sheet, or added as a separate object.These displays are dynamically updated if the content of cells change. For example, suppose that the important design requirements are displayed visually; then, in response to a user's change in trial values for parameters, the curves describing the design change shape, and their points of intersection shift, assisting the selection of the best design.
Using other Windows applications{{Expand section}}
Windows applications such as Microsoft Access and Microsoft Word, as well as Excel as can communicate with each other and use each others' capabilities.
The most common are 
Dynamic Data Exchange: although strongly deprecated by Microsoft, this is a common method to send data between applications running on Windows, with official MS publications referring to it as "the protocol from hell".http://drdobbs.com/article/print?articleId=184409151&siteSectionName= As the name suggests, it allows applications to supply data to others for calculation and display. It is very common in financial markets, being used to connect to important financial data services such as Bloomberg and Reuters.OLE Object Linking and Embedding: allows a Windows application to control another to enable it to format or calculate data. This may take on  the form of "embedding" where an application uses another to handle as task that it is more suited to, for example a Powerpoint presentation may be embedded in an Excel spreadsheet or vice versa.{{cite book}}

{{cite book}}{{cite book}}{{cite book}}{{cite book}}
Using external data{{Expand section}}
Excel users can access external data sources via Microsoft Office features such as (for example) .odc connections built with the Office Data Connection file format. Excel files themselves may be updated using a Microsoft supplied ODBC driver.Excel can accept data in real time through several programming interfaces, which allow it to communicate with many data sources such as Bloomberg and Reuters.

DDE : &#34;Dynamic Data Exchange&#34; uses the message passing mechanism in Windows to allow data to flow between Excel and other applications. Although it is easy for users to create such links, programming such links reliably is so difficult that Microsoft, the creators of the system, officially refer to it as &#34;the protocol from hell&#34;.JM Newcomer "An alternative to the protocol from hell" In spite of its many issues DDE remains the most common way for data to reach traders in financial markets.



Network DDE Extended the protocol to allow spreadsheets on different computers to exchange data. Given the view above, it is not surprising that in Vista, Microsoft no longer supports the facility.Microsoft discontinues support for network DDE



Real Time Data : RTD although in many ways technically superior to DDE, has been slow to gain acceptance, since it requires non-trivial programming skills, and when first released was neither adequately documented nor supported by the major data vendors.How to set up RTD in Excel
{{cite book}}
Alternatively, Microsoft Query provides ODBC-based browsing within Microsoft Excel.{{cite book}}{{cite web}}Use Microsoft Query to retrieve external data
QuirksOther errors specific to Excel include misleading statistics functions, mod function errors, date limitations and the Excel 2007 error.Fix of display error for a number from 65535.99999999995 to 65536 {{see}}
Statistical functionsThe accuracy and convenience of statistical tools in Excel has been criticized,{{cite journal}}{{cite journal}}{{cite journal}}{{cite web}}{{cite web}} as mishandling missing data, as returning incorrect values due to inept handling of round-off and large numbers, as only selectively updating calculations on a spreadsheet when some cell values are changed, and as having a limited set of statistical tools. Microsoft has announced some of these issues are addressed in Excel 2010.Function improvements in Excel 2010 Comments are provided from readers that may illuminate some remaining problems.
GraphingIn the Microsoft Excel 2007, menus related to graphs and graph formatting have been changed.  Some common activities in using graphs are less transparent than previously. For example, to add a curve to a graph, one can right click on the graph and choose "select data" from the drop-down menu, or use the "chart tools/design" tab. However, when there are other drop-down menus open, this menu doesn't appear and the "select data" option is grayed out (unavailable) from the toolbar. That facet of the menu system must be "discovered" by the user.
Excel MOD function errorExcel has issues with modulo operations. In the case of excessively large results, Excel will return the error warning #NUM! instead of an answer.{{cite web}}{{cite web}}
Date problemsExcel includes January 0, 1900 and February 29, 1900, incorrectly treating 1900 as a leap year.{{cite web}}{{cite web}} The bug originated from Lotus 1-2-3, and was purposely implemented in Excel for the purpose of backward compatibility.{{cite web}} This legacy has later been carried over into Office Open XML file format.{{cite web}}
FilenamesMicrosoft Excel will not open two documents with the same name and instead will display the following error:

A document with the name &#39;%s&#39; is already open. You cannot open two documents with the same name, even if the documents are in different folders. To open the second document, either close the document that is currently open, or rename one of the documents.The Hindu Business Line : Trouble with macros
The reason is for calculation ambiguity with linked cells. If there is a cell ='[Book1.xlsx]Sheet1'!$G$33, and there are two books named "Book1" open, there is no way to tell which one the user means.Microsoft Excel - Why Can't I Open Two Files With the Same Name?
Numeric precision{{main}}
Despite the use of 15-figure precision, Excel can display many more figures (up to thirty) upon user request. But the displayed figures are not those actually used in its computations, and so, for example, the difference of two numbers may differ from the difference of their displayed values. Although such departures are usually beyond the 15th decimal, exceptions do occur, especially for very large or very small numbers. Serious errors can occur if decisions are made based upon automated comparisons of numbers (for example, using the Excel If function), as equality of two numbers can be unpredictable.In the figure the fraction 1/9000 is displayed in Excel. Although this number has a decimal representation that is an infinite string of ones, Excel displays only the leading 15 figures. In the second line, the number one is added to the fraction, and again Excel displays only 15 figures. In the third line, one is subtracted from the sum using Excel. Because the sum in the second line has only eleven 1's after the decimal, the difference when 1 is subtracted from this displayed value is three 0's followed by a string of eleven 1's. However, the difference reported by Excel in the third line is three 0's followed by a string of thirteen 1's and two extra erroneous digits. Thus, the numbers Excel calculates with to obtain the third line are not the numbers that it displays in the first two lines. Moreover, the error in Excel's answer is not just round-off error.Excel works with a modified 1985 version of the IEEE 754 specification.Microsoft's overview is found at: {{cite web}} Excel's implementation involves an amalgam of truncations and conversions between binary and decimal representations, leading to accuracy that sometimes is better than one would expect from simple fifteen digit precision, and sometimes much worse.  See the main article for details.Besides accuracy in user computations, the question of accuracy in Excel-provided functions may be raised. Particularly in the arena of statistical functions, Excel has been criticized for sacrificing accuracy for speed of calculation.{{cite book}}{{cite book}}As many calculations in Excel are executed using VBA, an additional issue is the accuracy of VBA, which varies with variable type and user-requested precision.{{cite book}}
History
Excel 2.0Microsoft originally marketed a spreadsheet program called Multiplan in 1982. Multiplan became very popular on CP/M systems, but on MS-DOS systems it lost popularity to Lotus 1-2-3. Microsoft released the first version of Excel for the Mac in 30 September 1985, and the first Windows version (numbered 2.05 to line up with the Mac and bundled with a run-time Windows environment) in November 1987.{{cite book}} Lotus was slow to bring 1-2-3 to Windows and by 1988 Excel had started to outsell 1-2-3 and helped Microsoft achieve the position of leading PC software developer. This accomplishment, dethroning the king of the software world, solidified Microsoft as a valid competitor and showed its future of developing GUI software. Microsoft pushed its advantage with regular new releases, every two years or so.Early in 1993 Excel became the target of a trademark lawsuit by another company already selling a software package named "Excel" in the finance industry. As the result of the dispute Microsoft had to refer to the program as "Microsoft Excel" in all of its formal press releases and legal documents. However, over time this practice has been ignored, and Microsoft cleared up the issue permanently when they purchased the trademark of the other program.{{Citation needed}} Microsoft also encouraged the use of the letters XL as shorthand for the program; while this is no longer common, the program's icon on Windows still consists of a stylized combination of the two letters, and the file extension of the default Excel format is .xls.Excel offers many user interface tweaks over the earliest electronic spreadsheets; however, the essence remains the same as in the original spreadsheet software, VisiCalc: the program displays cells organized in rows and columns, and each cell may contain data or a formula, with relative or absolute references to other cells.Excel became the first spreadsheet to allow the user to define the appearance of spreadsheets (fonts, character attributes and cell appearance). It also introduced intelligent cell recomputation, where only cells dependent on the cell being modified are updated (previous spreadsheet programs recomputed everything all the time or waited for a specific user command). Excel has extensive graphing capabilities, and enables users to perform mail merge.
Excel 5.0With version 5.0 (1993), Excel has included Visual Basic for Applications (VBA), a programming language based on Visual Basic which adds the ability to automate tasks in Excel and to provide user-defined functions (UDF) for use in worksheets. VBA is a powerful addition to the application and includes a fully featured integrated development environment (IDE). Macro recording can produce VBA code replicating user actions, thus allowing simple automation of regular tasks. VBA allows the creation of forms and in-worksheet controls to communicate with the user. The language supports use (but not creation) of ActiveX (COM) DLL's; later versions add support for class modules allowing the use of basic object-oriented programming techniques.The automation functionality provided by VBA made Excel a target for macro viruses. This caused serious problems until antivirus products began to detect these viruses. Microsoft belatedly took steps to prevent the misuse by adding the ability to disable macros completely, to enable macros when opening a workbook or to trust all macros signed using a trusted certificate.Versions 5.0 to 9.0 of Excel contain various Easter eggs, including a Hall of Tortured Souls, although since version 10 Microsoft has taken measures to eliminate such undocumented features from their products.{{cite web}}
Excel 2000{{See also}}
For many users, one of the most obvious changes introduced with Excel 2000 (and the rest of the Office 2000 suite) involved a clipboard that could hold multiple objects at once. In another noticeable change the Office Assistant, whose frequent unsolicited appearance in Excel 97 had annoyed many users, became less intrusive. 

Excel 2007The most obvious change is a completely revamped user interface called the Ribbon menu system, which means a user must abandon most habits acquired from previous versions. Some practical advantages of the new system are a decreased number of mouse-clicks needed to reach a given functionality (e.g. removing the gridlines now takes 2 mouse-clicks instead of 5), greatly improved management of named variables through the Name Manager, and much improved flexibility in formatting graphs, which now allow (x, y) coordinate labeling and lines of arbitrary weight. The number of rows is now 1,048,576 and columns is 16,384. Several improvements to pivot tables were introduced. Office Open XML file formats were introduced, including .xlsm for a workbook with macros and .xlsx for a workbook without macros.{{cite book}}This version makes more extensive use of multiple cores for the calculation of spreadsheets, however VBA macros are not handled in parallel and XLL addins only are executed in parallel if they are thread-safe and indicate this at the time of registration.
Excel 2010The changes in Excel 2010 are listed on the microsoft website.Office.microsoft.com
The bottom right corner of an Excel 2010 Spreadsheet is XFD1048576
Versions
Microsoft Windows

1987 Excel 2.0 for Windows
1990 Excel 3.0
1992 Excel 4.0
1993 Excel 5.0 (Office 4.2 &#38; 4.3, also a 32-bit version for Windows NT only on the x86, PowerPC, Alpha, and MIPS architectures)
1995 Excel for Windows 95 (version 7.0) included in Office 95
1997 Excel 97 (version 8.0) included in Office 97 (for x86 and Alpha). This version of Excel includes a flight simulator as an Easter Egg.
1999 Excel 2000 (version 9.0) included in Office 2000
2001 Excel 2002 (version 10) included in Office XP
2003 Office Excel 2003 (version 11) included in Office 2003
2007 Office Excel 2007 (version 12) included in Office 2007
2010 Excel 2010 (version 14) included in Office 2010
Note: No MS-DOS version of Excel 1.0 for Windows ever existed: the Windows version originated at the time the Mac version was up to 2.0.
Note: There is no Excel 6.0, because the Windows 95 version was launched with Word 7. All the Office 95 & Office 4.X products have OLE 2 capacity&nbsp;— moving data automatically from various programs&nbsp;— and Excel 7 would show that it was contemporary with Word 7.


Apple Macintosh

1985 Excel 1.0
1988 Excel 1.5
1989 Excel 2.2
1990 Excel 3.0
1992 Excel 4.0
1993 Excel 5.0 (part of Office 4.X—Motorola 68000 version and first PowerPC version)
1998 Excel 8.0 (part of Office 98)
2000 Excel 9.0 (part of Office 2001)
2001 Excel 10.0 (part of Office v. X)
2004 Excel 11.0 (part of Office 2004)
2008 Excel 12.0 (part of Office 2008)
2011 Excel 14.0 (part of Office 2011)


OS/2

1989 Excel 2.2
1990 Excel 2.3
1991 Excel 3.0


Number of rowsVersions of Excel up to 7.0 had a limitation in the size of their data sets of 16K (2^14=16384) rows. Versions 8.0 through 11.0 could handle 64K (2^16=65536) rows and 256 columns (2^8 as label 'IV'). Version 12.0 can handle 1M (2^20=1048576) rows, and 16384 (2^14 as label 'XFD') columns.{{cite web}}
File formats{{Infobox file format}}Microsoft Excel up until 2007 version used a proprietary binary file format called Binary Interchange File Format (BIFF) as its primary format.{{cite web}} Excel 2007 uses Office Open XML as its primary file format, an XML-based format that followed after a previous XML-based format called "XML Spreadsheet" ("XMLSS"), first introduced in Excel 2002.{{cite web}}Although supporting and encouraging the use of new XML-based formats as replacements, Excel 2007 remained backwards-compatible with the traditional, binary formats. In addition, most versions of Microsoft Excel can read CSV, DBF, SYLK, DIF, and other legacy formats. Support for some older file formats were removed in Excel 2007.{{cite web}} The file formats were mainly from DOS based programs.
BinaryOpenOffice.org has created documentation of the Excel format.{{cite web}} Since then Microsoft made the Excel binary format specification available to freely download.{{cite web}}
XML Spreadsheet{{main}}
The XML Spreadsheet format introduced in Excel 2002 is a simple, XML based format missing some more advanced features like storage of VBA macros. Though the intended file extension for this format is .xml, the program also correctly handles XML files with .xls extension. This feature is widely used by third-party applications (e.g. MySQL Query Browser) to offer "export to Excel" capabilities without implementing binary file format. The following example will be correctly opened by Excel if saved either as Book1.xml or Book1.xls:
<?xml version="1.0"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <Worksheet ss:Name="Sheet1">
  <Table ss:ExpandedColumnCount="2" ss:ExpandedRowCount="2" x:FullColumns="1"
   x:FullRows="1">
   <Row>
    <Cell><Data ss:Type="String">Name</Data></Cell>
    <Cell><Data ss:Type="String">Example</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Value</Data></Cell>
    <Cell><Data ss:Type="Number">123</Data></Cell>
   </Row>
  </Table>
 </Worksheet>
</Workbook>


Standard file-extensions{{Expand section}}

Format Extension Description 
Spreadsheet .xls Main spreadsheet format which holds data in worksheets, charts, and macros 
Add-in (VBA) .xla Adds custom functionality; written in VBA 
Toolbar .xlb The file extension where Microsoft Excel custom toolbar settings are stored. 
Chart .xlc A chart created with data from a Microsoft Excel spreadsheet that only saves the chart. To save the chart and spreadsheet save as .XLS. XLC is not supported in Excel 2007 or in any newer versions of Excel. 
Dialog .xld Used in older versions of Excel. 
Archive .xlk A backup of an Excel Spreadsheet 
Add-in (DLL) .xll Adds custom functionality; written in C++/C, Visual Basic, Fortran, etc. and compiled in to a special dynamic-link library 
Macro .xlm A macro is created by the user or pre-installed with Excel. 
Template .xlt A pre-formatted spreadsheet created by the user or by Microsoft Excel. 
Module .xlv A module is written in VBA (Visual Basic for Applications) for Microsoft Excel  
Library .DLL Code written in VBA may access functions in a DLL, typically this is used to access the Windows API 
Workspace .xlw Arrangement of the windows of multiple Workbooks 

Office Open XML{{Main}}Microsoft Excel 2007, along with the other products in the Microsoft Office 2007 suite, introduced new file-formats. The first of these (.xlsx) is defined in the Office Open XML (OOXML) specification.The new Excel 2007 formats are:

New Excel 2007 formats
Format Extension Description 
Excel Workbook .xlsx The default Excel 2007 workbook format. In reality a ZIP compressed archive with a directory structure of XML text documents. Functions as the primary replacement for the former binary .xls format, although it does not support Excel macros for security reasons. 
Excel Macro-enabled Workbook .xlsm As Excel Workbook, but with macro support. 
Excel Binary Workbook .xlsb As Excel Macro-enabled Workbook, but storing information in binary form rather than XML documents for opening and saving documents more quickly and efficiently. Intended especially for very large documents with tens of thousands of rows, and/or several hundreds of columns. 
Excel Macro-enabled Template .xltm A template document that forms a basis for actual workbooks, with macro support. The replacement for the old .xlt format. 
Excel Add-in .xlam Excel add-in to add extra functionality and tools. Inherent macro support because of the file purpose. 
Excel 2010 continues with these new formats.
Export and migration of spreadsheetsProgrammers have produced APIs to open Excel spreadsheets in a variety of applications and environments other than Microsoft Excel. These include opening Excel documents on the web using either ActiveX controls, or plugins like the Adobe Flash Player. The Apache POI opensource project provides Java libraries for reading and writing Excel spreadsheet files. ExcelPackage is another open-source project that provides server-side generation of Microsoft Excel 2007 spreadsheets. PHPExcel is a PHP library that converts Excel5, Excel 2003, and Excel 2007 formats into objects for reading and writing within a web application.
See also

Comparison of risk analysis Microsoft Excel add-ins
Comparison of spreadsheets
Excel Viewer
List of spreadsheets
Spreadmart
Visual Basic for Applications


References{{Reflist}}
General references

{{cite book}}
{{cite book}}


External links{{Commons category}}
{{wikibooks}}

Microsoft Excel official site
Office 2010 product guide
Interactive translation from Excel 2003 menus to 2007

{{Spreadsheets}}
{{Microsoft Office}}



ar:مايكروسوفت إكسل
az:Microsoft Excel
bn:মাইক্রোসফট এক্সেল
bg:Microsoft Excel
bs:Microsoft Excel
ca:Microsoft Excel
cs:Microsoft Excel
da:Microsoft Excel
de:Microsoft Excel
et:Microsoft Excel
es:Microsoft Excel
fa:مایکروسافت اکسل
fr:Microsoft Excel
gl:Microsoft Excel
ko:마이크로소프트 엑셀
hi:माइक्रोसॉफ़्ट ऍक्सल
hr:Microsoft Excel
id:Microsoft Excel
it:Microsoft Excel
he:Microsoft Excel
ka:Excel
kk:Microsoft Office Еxcel
lb:.xls
lt:Microsoft Excel
mk:Microsoft Excel
mr:मायक्रोसॉफ्ट एक्सेल
arz:مايكروسوفت اكسل
nl:Microsoft Office Excel
ja:Microsoft Excel
no:Microsoft Office Excel
pl:Microsoft Excel
pt:Microsoft Excel
ro:Microsoft Excel
ru:Microsoft Excel
sq:Excel
si:Microsoft Excel
simple:Microsoft Excel
sk:Microsoft Excel
sl:Microsoft Excel
sr:Majkrosoft eksel
sh:Microsoft Excel
fi:Microsoft Excel
sv:Microsoft Excel
ta:மைக்ரோசாப்ட் எக்செல்
th:ไมโครซอฟท์ เอกซ์เซล
tr:Microsoft Excel
uk:Microsoft Excel
vep:Microsoft Office Excel
vi:Microsoft Excel
yi:עקסעל
zh:Microsoft Excel