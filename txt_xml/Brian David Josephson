title{{Infobox scientist}}
Brian David Josephson, FRS{{cite web}} (born 4 January 1940; Cardiff, Wales{{cite web}}) is a Welsh physicist. He became a Nobel Prize laureate in 1973{{cite web}} for the prediction of the eponymous Josephson effect.As of late 2007, he was a retired professor at the University of Cambridge, where he is the head of the Mind–Matter Unification Project in the Theory of Condensed Matter (TCM) research group. He is also a fellow of Trinity College, Cambridge.{{cite web}}
EducationBrian Josephson attended Cardiff High School and then Cambridge University, where he gained a BA in 1960. Whilst an undergraduate, he became notorious as a brilliant and self-assured student. A former lecturer remembers the importance of being particularly precise if addressing a class that included Josephson; if a mistake was made, Josephson would not be afraid to politely point it out after the lecture. As an undergraduate he published a paper in which he calculated a thermal correction to the Mössbauer effect that reconciled previously different measurements of gravitational red shifts reported by teams in the US and UK. After completing his undergraduate degree he continued to study at Cambridge, and in 1964 was awarded his PhD in physics. In the 1970s he learned Transcendental Meditation.{{Cite book}}
Academic careerJosephson became a fellow of Trinity College, Cambridge in 1962 before moving to the United States to take a position as Research Assistant Professor at the University of Illinois. He returned to Cambridge University in 1967 as an Assistant Director of Research at the Cavendish Laboratory and then a professor of physics in 1974, a position he retained until his retirement in 2007.Since 1983 Josephson has been appointed a Visiting Professor at various institutions including the Wayne State University in 1983, the Indian Institute of Science in 1984 and the University of Missouri-Rolla in 1987.Josephson has been a member of the Theory of Condensed Matter (TCM) Group, a theoretical physics group at the Cavendish Laboratory, for much of his research career.{{cite web}} While working at TCM group he was jointly awarded the Nobel Prize in Physics in 1973 while still only a Reader in Physics. He shared the award with Japanese physicist Leo Esaki and American physicist Ivar Giaever, who each received 1/4 of the prize, with 1/2 going to Josephson.{{cite web}} Unusually, along with Josephson, neither Esaki nor Giaever held professorships at the time of the award. It is rare that academics ranked below professors win the prestigious prize.{{cite web}} In addition and also unusually, each of the three performed the relevant research prior to being awarded his PhD.Josephson also directs the Mind–Matter Unification Project in the TCM Group.http://www.tcm.phy.cam.ac.uk/research.html He currently sits on the Advisory and Editorial Board of NeuroQuantology: An Interdisciplinary Journal of Neuroscience and Quantum Physics.{{cite web}}
Research
Josephson effectJosephson is best known for his pioneering theoretical work on superconductivity, earning him a 1/2 share of the 1973 Nobel Prize for Physics.{{cite web}} Specifically, it was awarded for "his theoretical predictions of the properties of a supercurrent through a tunnel barrier, in particular those phenomena which are generally known as the Josephson effects", which led to the invention of the Josephson junction. These junctions are key components in devices used to make highly sensitive measurements in magnetic fields. Further use for his discoveries was realised by researchers at IBM who, by 1980, had assembled an experimental computer switch structure, which would permit switching speeds from 10 to 100 times faster than those possible with conventional silicon-based chips, increasing data processing capabilities by a vast amount.
Mind–Matter Unification ProjectJosephson directed the Mind–Matter Unification Project, which he describes as: "a project concerned primarily with the attempt to understand, from the viewpoint of the theoretical physicist, what may loosely be characterised as intelligent processes in nature, associated with brain function or with some other natural process".{{cite web}} More generally, the research involves how the brain works, investigating topics such as language and consciousness, and pondering the fundamental connections between music and the mind. It is based on the belief that quantum mechanics is not the ultimate theory of nature. He maintains that "Quantum theory is not a complete picture of nature even though it is correct in its own domain".{{cite web}} He believes ideas such as complementarity in physics may also apply to biology.Despite his retirement Josephson continues to be active within the Mind-Matter Unification project. Among his aims is to find mechanisms behind phenomena such as the possibility that organisms can learn to bias the statistics of supposedly random physical processes through having a better understanding of its patterns than non-living matter.
ParapsychologyJosephson is one of the more well-known scientists who say that parapsychological phenomena may be real, and is also interested in the possibility that Eastern mysticism may have relevance to scientific understanding. He has said that one of his guiding principles has been nullius in verba (take nobody's word, the motto of the Royal Society), saying that "if scientists as a whole denounce an idea, this should not necessarily be taken as proof that the said idea is absurd; rather, one should examine carefully the alleged grounds for such opinions and judge how well these stand up to detailed scrutiny."In 2001 Josephson's views on the paranormal were under the spotlight when he wrote about them in a booklet to accompany six special stamps to honour the 100th anniversary of the Nobel prize. The Royal mail had sent Josephson a request to write a small article about their award and the implication of research in their field they could use in conjunction with the special Nobel Centenary stamp issue. He wrote the following: "Physicists attempt to reduce the complexity of nature to a single unifying theory, of which the most successful and universal, the quantum theory, has been associated with several Nobel prizes, for example those to Dirac and Heisenberg. Max Planck's original attempts a hundred years ago to explain the precise amount of energy radiated by hot bodies began a process of capturing in mathematical form a mysterious, elusive world containing 'spooky interactions at a distance', real enough however to lead to inventions such as the laser and transistor.Quantum theory is now being fruitfully combined with theories of information and computation. These developments may lead to an explanation of processes still not understood within conventional science such as telepathy, an area where Britain is at the forefront of research."
He came under criticism from several fellow physicists including David Deutsch, a quantum physicist at Oxford University who stated: "It is utter rubbish. Telepathy simply does not exist. The Royal Mail has let itself be hoodwinked into supporting ideas that are complete nonsense". However, Josephson maintains "There is a lot of evidence to support the existence of telepathy, for example, but papers on the subject are being rejected – quite unfairly".{{cite news}}In 2005, Josephson said that "parapsychology should now have become a conventional field of research, and yet parapsychology's claims are still not generally accepted". He compared this situation to that of Alfred Wegener's hypothesis of continental drift, where he believed there was initially great resistance to acceptance despite the strength of the evidence.Michael A. Thalbourne and Lance Storm (2005). Parapsychology in the twenty-first century: essays on the future of psychical research McFarland, pp. 1–2. Only after Wegener's death did further evidence lead to a gradual change of opinion and ultimate acceptance of his ideas. Josephson said that many scientists are not yet swayed by the evidence for parapsychology and the paranormal. Josephson contends that some scientists feel uncomfortable about ideas such as telepathy and that their emotions sometimes get in the way.
Awards and medals
Awards

New Scientist 1969
Research Corporation 1969
Fritz London Memorial Prize, 1970
Nobel Prize for Physics 1973


Medals

Guthrie Medal (Institute of Physics) 1972
van der Pol 1972
Elliott Cresson Medal (Franklin Institute) 1972
Hughes (Royal Society) 1972
Holweck Prize (Institute of Physics and French Institute of Physics) 1972
Faraday (Institution of Electrical Engineers) 1982
Sir George Thomson (Institute of Measurement and Control) 1984
Medal of the town of Marseilles 2004


Selected publications

Josephson, B.D., 1964: &#34;Coupled Superconductors&#34;, Review of Modern Physics, 36 [1P1].
Josephson, B.D., 1965: &#34;Supercurrents through Barriers&#34;, Advances in Physics, 14 [56].
Josephson, B.D., 1992: &#34;Telepathy Works&#34;, New Scientist, 135 [1833], 50-50.
Josephson, B.D., 1992: &#34;Defining Consciousness&#34;, Nature, 358 [6388], 618-618.
Josephson, B.D., 1993: &#34;All in the Memes&#34;, New Statesman & Society, 6 [276], 28-29.
Josephson, B.D., 1994: &#34;Awkward Eclipse&#34;, New Scientist, 144 [1956], 51-51.
Josephson, B.D., 1995: &#34;Light Barrier&#34;, New Scientist, 146 [1975], 55-55.
Josephson, B.D., 1997: &#34;Skeptics Cornered&#34;, Physics World, 10 [9], 20-20.
Josephson, B.D., 1999: &#34;What is truth?&#34;, Physics World, 12 [2], 15-15.
Josephson, B.D., 2000: &#34;Positive bias to paranormal claims&#34;, Physics World, 13 [10], 20-20.
Josephson, B.D., 2006: &#34;Take nobody&#39;s word for it&#34;, New Scientist, 192 [2581], 56-57.


See also

List of physicists
List of Jewish Nobel laureates
Quantum pseudo-telepathy
Scientific phenomena named after people


References{{reflist}}
External links

Brian Josephson's home page.
Josephson, Brian D. Biography by Britannica.
&#34;How Josephson Discovered His Effect.&#34; by Philip Anderson, Physics Today, November 1970. Anderson&#39;s account (he taught the graduate course in solid-state/many-body theory in which Josephson was a student) of Josephson&#39;s discovery.
The Discovery of Tunnelling Supercurrents Nobel lecture.
&#34;The Nobel Laureate Versus the Graduate Student&#34; by Donald G. McDonald, Physics Today, July 2001. An account of the historic debate between Josephson and John Bardeen at Queen Mary College in London, September 1962.

{{Nobel Prize in Physics Laureates 1951-1975}}
{{Scientists whose names are used in physical constants}}
{{Parapsychology}}{{Use dmy dates}}{{Persondata}}
{{DEFAULTSORT:Josephson, Brian David}}











ar:بريان جوزيفسن
bn:ব্রায়ান ডেভিড জোসেফসন
be:Браян Дэйвід Джозефсан
bg:Брайън Джоузефсън
ca:Brian David Josephson
cs:Brian David Josephson
cy:Brian David Josephson
de:Brian D. Josephson
es:Brian David Josephson
eu:Brian David Josephson
fa:بریان دیوید جوزفسون
fr:Brian David Josephson
gl:Brian David Josephson
ko:브라이언 데이비드 조지프슨
io:Brian David Josephson
id:Brian David Josephson
it:Brian Josephson
sw:Brian Josephson
ht:Brian David Josephson
hu:Brian David Josephson
mr:ब्रायन डेव्हिड जोसेफसन
nl:Brian Josephson
ja:ブライアン・ジョゼフソン
no:Brian David Josephson
oc:Brian David Josephson
pnb:بریان جوزفسن
pl:Brian David Josephson
pt:Brian David Josephson
ro:Brian David Josephson
ru:Джозефсон, Брайан Дэвид
sa:ब्रायन जोसेफसन
scn:Brian David Josephson
simple:Brian David Josephson
sk:Brian David Josephson
sl:Brian David Josephson
fi:Brian David Josephson
sv:Brian D. Josephson
uk:Браян Девід Джозефсон
yo:Brian David Josephson
zh:布赖恩·戴维·约瑟夫森