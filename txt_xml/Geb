title{{About}}{{cleanup-reorganize}}{{Infobox Egyptian deity}}
{{Hiero}}
Geb was the Egyptian god of the Earth and a member of the Ennead of Heliopolis. It was believed in ancient Egypt that Geb's laughter were earthquakes and that he allowed crops to grow. The name was pronounced as such from the Greek period onward, (formerly erroneously read as Sebcf. E.A.Wallis Budge, The Gods of the Egyptians. Studies in Egyptian Mythology (London, 1904; republ.Dover Publications, New York, 1969) or as Keb). The original Egyptian was "Gebeb"/"Kebeb", meaning probably: 'weak one', perhaps:'lame one'. It was spelled with either initial -g- (all periods), or with -k-point (gj). The latter initial root consonant occurs once in the Middle Kingdom Coffin Texts, more often in 21st Dynasty mythological papyri as well as in a text from the Ptolemaic tomb of Petosiris at Tuna el-Gebel or was written with initial hard -k-, as e.g. in a 30th Dynasty papyrus text in the Brooklyn Museum dealing with descriptions of and remedies against snakes and their bites.
Role and development
The oldest representation in a fragmentary relief of the god, was as an anthropomorphic bearded being accompanied by his name, and dating from king Djoser's reign, 3rd Dynasty, and was found in Heliopolis. In later times he could also be depicted as a ram, a bull or a crocodile (the latter in a vignet of the Book of the Dead-papyrus of the lady Heryweben in the Egyptian Museum, Cairo, depicted in G.Posener e.a., Dictionnaire de la civilisation égyptienne = Lexikon der ägyptischen Kultur (Wiesbaden, 1960),lemma 'Crocodile'/'Krokodil'). Frequently described mythologically as 'father' of snakes (one of the names for snake was s3-t3 'son of the earth' and in a Coffin Texts-spell Geb was described as 'father' of the primeval snake Nehebkau, while his mother was in that case the goddess Neith) and therefore depicted sometimes (partly) as such. In mythology Geb also often occurs as a primeval divine king of Egypt from whom his 'son' Osiris and his 'grand-son' Horus inherited the land after many contendings with the disruptive god Seth, brother and killer of Osiris. Geb could also be regarded as personified fertile earth and barren desert, the latter containing the dead or setting them free from their tombs, metaphorically described as ' Geb opening his jaws', or imprisoning those there not worthy to go to the fertile North-Eastern heavenly Field of Reeds. In the latter case, one of his otherworldly attributes was an ominous jackal-headed stave (called wsr.t) rising from the ground unto which enemies could be bound and punished. 	 In the Heliopolitan Ennead (a group of nine gods created in the beginning by the one god Atum or Ra), Geb is the husband of Nut, the sky or visible daytime and nightly firmament, the son of the earlier primordial elements Tefnut ('orphaness', later also conceived of as moisture [e.g.: 'tef']) and Shu ('emptiness' or perhaps 'raiser'[namely of the firmament as air]), and the 'father' to the four lesser gods of the system - Osiris, Seth, Isis and Nephthys. In this context, Geb was believed to have originally been engaged in eternal sex with Nut, and had to be separated from her by Shu, god of the air.Meskell, Lynn Archaeologies of social life: age, sex, class et cetera in ancient Egypt Wiley Blackwell (20 Oct 1999) ISBN 978-0631212997 p.103 Consequently, in mythological depictions, Geb was shown as a 'man' reclining, sometimes with his phallus still pointed towards the sky goddess Nut.	As time progressed, the deity became more associated with the habitable land of Egypt and also as one of its early godly rulers. As a chthonic deity he (like Osiris and Min) became naturally associated with the underworld and with vegetation -barley being said to grow upon his ribs- and was depicted with plants and other green patches on his body. His association with vegetation, and sometimes with the underworld, and also with royalty  brought Geb the occasional interpretation that he was the husband of Renenutet, primarily a minor goddess of the harvest and also mythological caretaker (the meaning of her name is 'nursing snake') of the young king in the shape of a cobra, who herself could also be regarded as the mother of Nehebkau, a primeval snake god associated with the underworld, who, however, was on the same occasions said to be his son by her. He is also equated by classical authors as the Greek Titan Cronus.
Goose
{{cleanup}}Some Egyptologists, (specifically Jan Bergman, Terence Duquesne or Richard H. Wilkinson) have stated that Geb was associated with a mythological divine creator goose who had laid a cosmic egg from which the sun and/or the world had sprung. This theory is assumed to be incorrect and to be a result of confusing the divine name "Geb" with that of a Whitefronted Goose (Anser albifrons), also called originally gb(b): 'lame one, stumbler'.C.Wolterman, "On the Names of Birds and Hieroglyphic Sign-List G 22, G 35 and H 3" in: "Jaarbericht van het Vooraziatisch-Egyptisch genootschap Ex Oriente Lux" no.32 (1991-1992)(Leiden, 1993), p.122, note 8 This bird-sign is used only as a phonogram in order to spell the name of the god (H.te Velde, in: Lexikon der Aegyptologie II, lemma: Geb). An alternative ancient name for this goose species was trp meaning similarly 'walk like a drunk', 'stumbler.' The Whitefronted Goose is never found as a cultic symbol or holy bird of Geb. The mythological creator 'goose' referred to above, was called Ngg wr 'Great Honker' and always depicted as a Nilegoose/Foxgoose (Alopochen aegyptiacus) who ornitologically belongs to a separate genus and whose Egyptian name was smn, Coptic smon. A coloured vignet irrefutably depicts a Nile Goose with an opened beak (Ngg wr!) in a context of solar creation on a mythological papyrus dating from the 21st Dynasty. Similar images of this divine bird are to be found on temple walls (Karnak, Deir el-Bahari), showing a scene of the king standing on a papyrus raft and ritually plucking papyrus for the Theban god Amun-Re-Kamutef. The latter Theban creator god could be embodied in a Nilegoose, but never in a Whitefronted Goose. In Underworld Books a diacritic goose-sign (most probably denoting then an Anser albifrons) was sometimes depicted on top of the head of an standing anonymous male anthropomorphic deity, pointing to Geb's identity. Geb himself was never depicted as a Nile Goose, as later was Amun, called on some New Kingdom stelae explicitly:'Amun, the beautiful smn-goose (Nile Goose). The only clear pictorial confusion between the hieroglyphs of a Whitefronted Goose (in the normal hieroglyphic spelling of the name Geb, often followed by the additional -b-sign) and a Nile Goose in the spelling of the name Geb occurs in the rock cut tomb of the provincial governor Sarenput II (12th Dynasty, Middle Kingdom) on the Qubba el-Hawa desert-ridge (opposite Aswan), namely on the left (southern) wall near the open doorway, in the first line of the brightly painted funerary offering formula. This confusion is to be compared with the frequent hacking out by Ekhnaton's agents of the sign of the Pintail Duck (meaning 'son') in the royal title 'Son of Re', especially in Theban temples, where they confused the duck sign with that of a Nilegoose regarded as a form of the then forbidden god Amon.text: drs. Carles Wolterman, Amstelveen, Holland
External Links

Geb at Egyptian Gods


Notes
{{reflist}}
{{Ancient Egyptian religion footer}}


als:Geb
bn:গেব
bo:ཀའེ་པུའུ།
bs:Geb
br:Geb
bg:Геб
ca:Llista de personatges de la mitologia egípcia
cs:Geb
da:Geb
de:Geb (Ägyptische Mythologie)
et:Geb
el:Γκεμπ
es:Geb
eu:Geb
fa:گب
fr:Geb
ko:게브
hr:Geb
it:Geb
he:גב (אל)
ka:გები
lt:Gebas
hu:Geb
mk:Геб
arz:جب
nl:Geb (mythologie)
ja:ゲブ
no:Geb
oc:Geb
pl:Geb
pt:Geb
ro:Geb
ru:Геб
scn:Geb
simple:Geb
sk:Geb
sl:Geb
sr:Геб
sh:Geb
fi:Geb
sv:Geb
th:เกบ
tr:Geb
uk:Ґеб
vi:Geb
zh:盖布