titleA user guide or user's guide, also commonly known as a manual, is a technical communication document intended to give assistance to people using a particular system.{{cite web}} It is usually written by a technical writer, although user guides are written by programmers, product or project managers, or other technical staff, particularly in smaller companies.User guides are most commonly associated with electronic goods, computer hardware and software.Most user guides contain both a written guide and the associated images.  In the case of computer applications, it is usual to include screenshots of the human-machine interface(s), and hardware manuals often include clear, simplified diagrams.  The language used is matched to the intended audience, with jargon kept to a minimum or explained thoroughly.
Contents of a user manualThe sections of a user manual often include:

A cover page
A title page and copyright page
A preface, containing details of related documents and information on how to navigate the user guide
A contents page
A guide on how to use at least the main functions of the system
A troubleshooting section detailing possible errors or problems that may occur, along with how to fix them
A FAQ (Frequently Asked Questions)
Where to find further help, and contact details
A glossary and, for larger documents, an index


Computer software manuals and guidesUser manuals and user guides for most non-trivial software applications are book-like documents with contents similar to the above list. The Starta User Manual{{cite web}} is a good example of this type of document. Some documents have a more fluid structure with many internal links. The Google Earth User Guide{{cite web}} is an example of this format. The term guide is often applied to a document that addresses a specific aspect of a software product. Some usages are Installation Guide, Getting Started Guide, and various How to guides. An example is the Picasa Getting Started Guide.{{cite web}}In some business software applications, where groups of users have access to only a sub-set of the application's full functionality, a user guide may be prepared for each group. An example of this approach is the Autodesk Topobase 2010 Help{{cite web}} document, which contains separate Administrator Guides, User Guides, and a Developer's Guide. These guides are a valuable tool for On-the-job training.
References{{reflist}}
See also

Release notes
Moe book
Technical writer
Manual page (Unix)
Instruction manual (gaming)
Reference card



ar:دليل المستخدم
da:Brugsanvisning
de:Gebrauchsanleitung
es:Guía del usuario
fr:Mode d'emploi
id:Manual pengguna
he:מדריך למשתמש
nl:Handleiding
ja:マニュアル
no:Bruksanvisning
ru:Руководство пользователя
sv:Manual