package edu.ics.uci.irw12.wiki.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import edu.ics.uci.irw12.wiki.xml.Parser;
import edu.ics.uci.irw12.wiki.xml.impl.ParseXMLFiles;
import edu.ics.uci.irw12.wiki.xml.impl.VtdXMLParser;
import edu.ics.uci.irw12.wiki.benchmark.NaivePhaseBenchmark;
import edu.ics.uci.irw12.wiki.indexer.Index;
import edu.ics.uci.irw12.wiki.indexer.impl.NaivePhaseIndexer;
import edu.ics.uci.irw12.wiki.search.SearchIndex;
import org.apache.log4j.*;

public class WikiProject {

	Index indexer;
	Parser PXF;
	Logger logger;
	SearchIndex SI;
	Properties prop;
	NaivePhaseBenchmark bm;
	
	public WikiProject() {
		// Put init stuff here
	}
	public Index getIndexer() {
		return indexer;
	}
	public Logger getLogger() {
		return logger;
	}
	public Parser getPXF() {
		return PXF;
	}
	public SearchIndex getSI() {
		return SI;
	}
	public void initialize() {
		try {
			//init props
			prop = new Properties();
			try {
		        //load a properties file
		 		prop.load(new FileInputStream("config.properties"));
		 	} catch (IOException ex) {
		 		logger.fatal("Exception loading properties file");
		 		ex.printStackTrace();
		    }
			
			//INTRODUCING NEW BENCHMARK PROCEDURE HERE
			
			//END OF BENCHMARK PROCEDURE
			
			long start,end;
			PropertyConfigurator.configure("log4j.properties");
			logger = Logger.getLogger("edu.ics.uci.irw12.wiki.WikiProject.class");
			
			if (!prop.isEmpty() && prop.containsKey("parse") && prop.getProperty("parse").equals("yes")) {
				if (prop.containsKey("parser") && prop.getProperty("parser").equals("jdom")) {
					PXF = new ParseXMLFiles();
				} else {
					PXF = new VtdXMLParser();
				}
				logger.info("Parse object created");
				start=System.currentTimeMillis();
				PXF.parseXML(logger);
				end=System.currentTimeMillis();
				logger.info("Time needed to parse XMLs is - "+(end-start)+" milliseconds");
			}
			
			if (!prop.isEmpty() && prop.containsKey("index") && prop.getProperty("index").equals("yes")) {
				indexer = new NaivePhaseIndexer();
				logger.info("NaivePhaseIndexer created");
				start=System.currentTimeMillis();
				indexer.createIndex(logger);
				end=System.currentTimeMillis();
				logger.info("Time needed to index is - "+(end-start)+" milliseconds");
			}
			
			if (!prop.isEmpty() && prop.containsKey("search") && prop.getProperty("search").equals("yes")) {
				SI = new SearchIndex();
				logger.info("Search object created");
				search();
			}
		} catch (Exception e) {
			/*Make exceptions clear - Ambitious to have an exception framework*/
			logger.error(e.getMessage());
		}
	}
	
	public void search() {
		try {
			String options[]=new String[3];
			if (!prop.isEmpty()) {
				//Page size
				if (prop.containsKey("hitsPerPage")) {
					options[0] = "-paging";
					options[1] = prop.getProperty("hitsPerPage");
				} else {
					options[0] = "-paging";
					options[1] = "10";
				}
				
				//scores
				if (prop.containsKey("raw")) {
					options[2] = "-raw";
				}
			}
			SI.searchIndex(options, logger);
		} catch(Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	

	public static void main(String[] args) {
		WikiProject wp = new WikiProject();
		
		try {
			System.out.println("Initializing system");
			wp.initialize();
			System.out.println("System has been initialized");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
