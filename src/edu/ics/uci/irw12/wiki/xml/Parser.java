package edu.ics.uci.irw12.wiki.xml;

import org.apache.log4j.Logger;

public interface Parser {
	
	void parseXML(Logger logger) throws Exception;
	
}
