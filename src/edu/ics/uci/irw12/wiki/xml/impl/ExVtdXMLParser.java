package edu.ics.uci.irw12.wiki.xml.impl;

import com.ximpleware.XMLModifier.ByteSegment;
import com.ximpleware.extended.*;
import java.io.FileOutputStream;
import com.ximpleware.extended.XMLMemMappedBuffer;


public class ExVtdXMLParser {

	public static void first_read() throws Exception {
		VTDGenHuge vg = new VTDGenHuge();
		System.out.println("Inside");
		if (vg.parseFile("/home/anirudh/Lucene/sandbox/vtd/ximpleware_extended_2.10/code_example/alabama.xml",true, VTDGenHuge.MEM_MAPPED)) {
			VTDNavHuge vnh = vg.getNav();
			AutoPilotHuge aph = new AutoPilotHuge(vnh);
			AutoPilotHuge aph1 = new AutoPilotHuge(vnh);
			AutoPilotHuge aph2 = new AutoPilotHuge(vnh);
			//aph.selectXPath("/mediawiki/page/text()");
			aph1.selectXPath("title");
			aph2.selectXPath("revision/text");
			aph.bind(vnh);
			aph1.bind(vnh);
			aph2.bind(vnh);
			//aph.selectElementNS("//*","/mediawiki/page");
			//aph.selectElementNS("//*", "page");
			aph.selectElement("*");
			System.out.println("Started while");
			int i = -1, j = 0,k=-1;
			String t,tt;
			byte bb[] = null;
			boolean b = true;
			while ((b=aph.iterate())) {
				t=aph1.evalXPathToString();
				System.out.println("Title is "+t);
				tt=aph2.evalXPathToString();
				System.out.println("Text is "+tt);
				try {
				//(new FileOutputStream("/home/anirudh/Lucene/sandbox/vtd/ximpleware_extended_2.10/code_example/out"+ j + ".xml")).write(b, (int) l[0],	(int) (l[1]));
				} catch (Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
			aph2=aph1=aph=null;
			vnh=null;
			vg=null;
			System.gc();
			System.out.println(b+"Ended while");
		}
	}
	/* second read is the shorter version of loading the XML file */
	public static void second_read() throws Exception {
		VTDGenHuge vg = new VTDGenHuge();
		System.out.println("Inside");
		if (vg.parseFile("/home/anirudh/Lucene/sandbox/vtd/ximpleware_extended_2.10/code_example/alabama.xml",true, VTDGenHuge.MEM_MAPPED)) {
			VTDNavHuge vnh = vg.getNav();
			AutoPilotHuge aph = new AutoPilotHuge(vnh);
			AutoPilotHuge aph1 = new AutoPilotHuge(vnh);
			AutoPilotHuge aph2 = new AutoPilotHuge(vnh);
			aph1.selectXPath("title");
			aph2.selectXPath("revision/text");
			aph.bind(vnh);
			aph1.bind(vnh);
			aph2.bind(vnh);
			System.out.println("Started while");
			int i = -1, j = 0,k=-1;
			String t,tt;
			byte byteBuffer[] = null;
			long longArray[] = null;
			XMLMemMappedBuffer xMMB = new XMLMemMappedBuffer();
			FileOutputStream fos = null;
			aph.selectElement("page");
			while ((aph.iterate())) {
				try{
					fos=new FileOutputStream("/home/anirudh/Lucene/sandbox/vtd/ximpleware_extended_2.10/code_example/parsed/output"+j);
					k=aph1.evalXPath();
					if(k!=-1) {
						longArray = vnh.getElementFragment();
						xMMB.writeToFileOutputStream(fos, longArray[0], longArray[1]);
					}
					
					k=aph2.evalXPath();
					if(k!=-1) {
						longArray = vnh.getElementFragment();
						xMMB.writeToFileOutputStream(fos, longArray[0], longArray[1]);
					}
					fos.close();
					j++;
	/*				t=aph1.evalXPathToString();
					System.out.println("Title is "+t);
					tt=aph2.evalXPathToString();
					System.out.println("Text is "+tt);
	*/		
					//(new FileOutputStream("/home/anirudh/Lucene/sandbox/vtd/ximpleware_extended_2.10/code_example/out"+ j + ".xml")).write(b, (int) l[0],	(int) (l[1]));
				} catch (Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
			aph2=aph1=aph=null;
			vnh=null;
			vg=null;
			System.gc();
			System.out.println("Ended while");
		}
	}

	public static void main(String[] s) throws Exception {
		//first_read();
		System.out.println("Started");
		second_read();
	}

}
