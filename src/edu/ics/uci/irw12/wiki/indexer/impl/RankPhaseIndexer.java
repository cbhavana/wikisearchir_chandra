//Code borrowed from Apache Lucene DemoIndexer

package edu.ics.uci.irw12.wiki.indexer.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.index.FieldInfo.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.*;

import edu.ics.uci.irw12.wiki.indexer.Index;

// CHECK IF ALL OPTIONS LIKE UPDATE, CREATE ETC ARE NEEDED. 
// IF YES, PROVIDE METHODS TO PASS PARAMETERS FOR IT


public class RankPhaseIndexer implements Index{

	public RankPhaseIndexer() {
	}

	/** Index all text files under a directory. */
	public void createIndex(Logger logger) throws Exception {
		
		Properties prop = new Properties();
		try {
	        //load a properties file
	 		prop.load(new FileInputStream("config.properties"));
	 	} catch (IOException ex) {
	 		logger.fatal("Exception loading properties file");
	 		ex.printStackTrace();
	    }
		
		//Error checks
		if (prop.isEmpty()) {
			logger.fatal("Properties file is empty or not defined");
			return;
		} else if (!prop.containsKey("index.output.dir") || !prop.containsKey("parse.output.dir")){
			logger.fatal("parser.output.dir is not defined");
			return;
		}
		
		String indexPath = prop.getProperty("index.output.dir");
		String docsPath = prop.getProperty("parse.output.dir");
		boolean create = true;

		final File docDir = new File(docsPath);
		if (!docDir.exists() || !docDir.canRead()) {
			System.out
					.println("Document directory '"
							+ docDir.getAbsolutePath()
							+ "' does not exist or is not readable, please check the path");
			System.exit(1);
		}

		Date start = new Date();
		try {
			logger.info("Indexing to directory '" + indexPath + "'...");

			Directory dir = FSDirectory.open(new File(indexPath));
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_35);
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_35, analyzer);

			if (create) {
				// Create a new index in the directory, removing any
				// previously indexed documents:
				iwc.setOpenMode(OpenMode.CREATE);
			} else {
				// Add new documents to an existing index:
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}
			
			//increased iwc RAM Buffer size
			iwc.setRAMBufferSizeMB(256.0);
			
			IndexWriter writer = new IndexWriter(dir, iwc);
			indexDocs(writer, docDir, logger);

			// NOTE: if you want to maximize search performance,
			// you can optionally call forceMerge here. This can be
			// a terribly costly operation, so generally it's only
			// worth it when your index is relatively static (ie
			// you're done adding documents to it):
			//
			// writer.forceMerge(1);

			writer.close();

			Date end = new Date();
			// System.out.println(end.getTime() - start.getTime()+
			// " total milliseconds");
			logger.info(end.getTime() - start.getTime()
					+ " total milliseconds for indexing");

		} catch (IOException e) {
			System.out.println(" caught a " + e.getClass()
					+ "\n with message: " + e.getMessage());
			logger.error(e.getMessage());
		}
	}

	static void indexDocs(IndexWriter writer, File file, Logger logger)
			throws IOException {
		// do not try to index files that cannot be read
		if (file.canRead()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				// an IO error could occur
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						indexDocs(writer, new File(file, files[i]), logger);
					}
				}
			} else {

				FileInputStream fis;
				try {
					fis = new FileInputStream(file);
				} catch (FileNotFoundException fnfe) {
					// at least on windows, some temporary files raise this
					// exception with an "access denied" message
					// checking if the file can be read doesn't help
					return;
				}

				try {

					// make a new, empty document
					Document doc = new Document();					
					
					//ANIRUDH COMMENT: IT WAS PATH BEFORE 'TITLE' NOW CHANGED IT TO TITLE
					
					
					String title_plain = file.getName().trim();
					if((title_plain.indexOf("User:")!=-1) || (title_plain.indexOf("File:")!=-1) || (title_plain.indexOf("User:")!=-1)) {
						System.out.println("Inside under boosting "+file.getName());
						Field title = new Field("title", title_plain, Field.Store.YES, Field.Index.NOT_ANALYZED);
						//title.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
						title.setBoost(0.5f);
						doc.add(title);
						Field title_rank = new Field("titlerank", title_plain, Field.Store.YES, Field.Index.ANALYZED);
						//title_rank.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
						title_rank.setBoost(0.0f);
						doc.add(title_rank);
						doc.setBoost(0.2f);
						
					} else {
						//System.out.println(file.getName());
						Field title = new Field("title", title_plain, Field.Store.YES, Field.Index.NOT_ANALYZED);
						//title.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
						title.setBoost(1.0f);
						doc.add(title);
						Field title_rank = new Field("titlerank", title_plain, Field.Store.YES, Field.Index.ANALYZED);
						//title_rank.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
						title_rank.setBoost(1.0f);
						doc.add(title_rank);
					}
					
					//NOt needed for now
					
					//System.out.println(file.getName());
					Field pathField = new Field("path", file.getAbsolutePath(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
					//pathField.setIndexOptions(IndexOptions.DOCS_ONLY);
					doc.add(pathField);
					
					NumericField modifiedField = new NumericField("modified");
					modifiedField.setLongValue(file.lastModified());
					//System.out.println(file.lastModified());
					doc.add(modifiedField);

					// Add the contents of the file to a field named "contents".
					// Specify a Reader,
					// so that the text of the file is tokenized and indexed,
					// but not stored.
					// Note that FileReader expects the file to be in UTF-8
					// encoding.
					// If that's not the case searching for special characters
					// will fail.
					StringBuffer sb = new StringBuffer();
					String sb_rl = new String();
					BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
					while((sb_rl=br.readLine())!=null){
						sb.append(sb_rl);
					}
					Field contents = new Field("contents", sb.toString(), Field.Store.NO, Field.Index.ANALYZED);
					//contents.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
					contents.setBoost(0.87f);
					doc.add(contents);

					if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
						// New index, so we just add the document (no old
						// document can be there):

						// System.out.println("adding " + file);
						//logger.info("adding " + file);
						writer.addDocument(doc);
					} else {
						// Existing index (an old copy of this document may have
						// been indexed) so
						// we use updateDocument instead to replace the old one
						// matching the exact
						// path, if present:
						
						//System.out.println("updating " + file);
						logger.info("updating " + file);
						writer.updateDocument(new Term("path", file.getPath()),doc);
					}

				} finally {
					fis.close();
				}
			}
		}
	}
}